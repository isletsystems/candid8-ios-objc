//
//  main.m
//  candid8
//
//  Created by Aldrin Lenny on 08/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ASCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ASCAppDelegate class]));
    }
}
