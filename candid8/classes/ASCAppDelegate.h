//
//  ASCAppDelegate.h
//  candid8
//
//  Created by Aldrin Lenny on 08/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <UIKit/UIKit.h>
#define TheAppDelegate ((ASCAppDelegate *)[[UIApplication sharedApplication] delegate])
@interface ASCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
- (UIViewController*)topViewController;
@end
