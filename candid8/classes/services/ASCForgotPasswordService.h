//
//  ASCForgotPasswordService.h
//  candid8
//
//  Created by Aldrin Lenny on 14/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCBaseService.h"

@interface ASCForgotPasswordService : ASCBaseService

+ (ASCForgotPasswordService *)sharedInstance;

- (void)resetPasswordWithEmailId:(NSString *)anEmailId andResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

@end
