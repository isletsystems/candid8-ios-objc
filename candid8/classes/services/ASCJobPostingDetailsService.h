//
//  ASCJobPostingDetailsService.h
//  candid8
//
//  Created by Aldrin Lenny on 14/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCBaseService.h"
#import "ASCJobPosting.h"
#import "ASCJobPostingQuestion.h"

@interface ASCJobPostingDetailsService : ASCBaseService

@property (nonatomic, strong) NSString *selectedJobId;
@property (nonatomic, strong) ASCJobPosting *selectedJob;
@property (nonatomic, strong) ASCJobPostingQuestion *selectedJobQuestion;
@property (nonatomic, readwrite) NSInteger selectedQuestionIndex;

+ (ASCJobPostingDetailsService *)sharedInstance;

- (void)fetchJobDetailWithId:(NSString *)aJobPostingId ofCandidateId:(NSString *)aCandidateId andResponseBlock:(ASCServiceResponseBlock)aResponseBlock;
- (void)fetchJobDetailWithId:(NSString *)aJobPostingId andResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

@end
