//
//  ASCJobListingService.h
//  candid8
//
//  Created by Aldrin Lenny on 27/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCBaseService.h"

@interface ASCJobListingService : ASCBaseService

+ (instancetype)sharedInstance;

- (void)fetchJobsOfCandidate:(NSString *)aCandidateId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

@end
