//
//  ASCJobViewedService.h
//  candid8
//
//  Created by Aldrin Lenny on 27/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCBaseService.h"

@interface ASCJobViewedService : ASCBaseService

+ (instancetype)sharedInstance;

- (void)markViewedJob:(NSString *)aJobId ofCandidate:(NSString *)aCandidateId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

@end
