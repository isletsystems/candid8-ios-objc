//
//  ASCCandidateResumeDeleteService.h
//  candid8
//
//  Created by islet on 11/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "ASCBaseService.h"

@interface ASCCandidateResumeDeleteService : ASCBaseService
+ (instancetype)sharedInstance;
- (void)deleteResumeWith:(NSString *)aResumeId  withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;
@end
