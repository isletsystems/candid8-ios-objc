//
//  KIBaseService.m
//  candid8
//
//  Created by Aldrin Lenny on 20/06/13.
//
//

#import "ASCBaseService.h"

#import <AFNetworking/AFNetworking.h>

//#define kASCServicePrefix @"http://wa.thecandid8.com/ws"
#define kASCServicePrefix @"http://api.thecandid8.com/v2"
#define kASCServicePrefix_DEBUG @"http://wa.thecandid8.com/ws"

@implementation ASCBaseService

- (NSString *)serviceUrl
{
    return nil;
}

- (void)fetchWithUrl:(NSString *)aUrlString andProcesseWithReponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    [self fetchWithUrl:aUrlString params:nil andProcesseWithReponseBlock:aResponseBlock];
}

- (void)fetchWithUrl:(NSString *)aUrlString params:(NSDictionary *)aParamsDict andProcesseWithReponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    DLog(@"url sent: %@", aUrlString);
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:[self getDynamicBaseUrlPrefix]]];
    manager.responseSerializer = [AFJSONResponseSerializer new];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:aUrlString parameters:aParamsDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"%@", responseObject);
        self.errorResponseDictionary = nil;
        id parsedData = [self parseRseponse:responseObject];
        if (parsedData) {
            aResponseBlock(TRUE, parsedData);
        } else {
            aResponseBlock(FALSE, @[]);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"Error: %@", error);
        if (![self isSessionTimedOut:operation] && aResponseBlock)
        {
            aResponseBlock(FALSE, error);
        }
    }];
}

- (void)deleteWithUrl:(NSString *)aUrlString params:(NSDictionary *)aParamsDict andProcesseWithReponseBlock:(ASCServiceResponseBlock)aResponseBlock{
    DLog(@"url sent: %@", aUrlString);
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:[self getDynamicBaseUrlPrefix]]];
    manager.responseSerializer = [AFJSONResponseSerializer new];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager DELETE:aUrlString parameters:aParamsDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (!responseObject) {
            responseObject = operation.response;
        }
        _errorResponseDictionary = nil;
        id parsedData = [self parseRseponse:responseObject];
        if (parsedData) {
            aResponseBlock(TRUE, parsedData);
        } else {
            aResponseBlock(FALSE, @[]);
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"Error: %@", error);
        if (![self isSessionTimedOut:operation] && aResponseBlock)
        {
            aResponseBlock(FALSE, error);
        }
    }];
}

- (void)putWithUrl:(NSString *)aUrlString  andProcesseWithReponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    [self putWithUrl:aUrlString params:[self parameDicForPutService] andProcesseWithReponseBlock:aResponseBlock];
}

- (void)putWithUrl:(NSString *)aUrlString params:(NSDictionary *)aParamsDict andProcesseWithReponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    DLog(@"PUT url sent: %@, params: %@", aUrlString, aParamsDict);
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:[self getDynamicBaseUrlPrefix]]];
    manager.responseSerializer = [AFJSONResponseSerializer new];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager PUT:aUrlString parameters:aParamsDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"%@", responseObject);
        aResponseBlock(TRUE, responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"Error: %@", error);
        if (![self isSessionTimedOut:operation] && aResponseBlock)
        {
            aResponseBlock(FALSE, error);
        }
    }];
}

- (id)parseRseponse:(id)aResponseObject
{
    @throw [[NSException alloc] initWithName:@"Place Holder Parse Response" reason:@"must be implemented by subclass" userInfo:nil];
            
    return nil;
}

- (NSDictionary *)parameDicForPutService
{
    @throw [[NSException alloc] initWithName:[NSString stringWithFormat:@"Place Holder Parse Response "] reason:[NSString stringWithFormat:@"%s must be implemented by subclass(%@)",__PRETTY_FUNCTION__,NSStringFromClass([self class])] userInfo:nil];
    
    return nil;
}

- (BOOL)isSessionTimedOut:(AFHTTPRequestOperation *)operation
{
    if (operation.response.statusCode == 401)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCLogoutNotification" object:nil];
        return TRUE;
    }
    return FALSE;
}

- (NSString *)getDynamicBaseUrlPrefix
{
#ifdef DEBUG1
    return kASCServicePrefix_DEBUG;
#else
    return kASCServicePrefix;
#endif
}


@end
