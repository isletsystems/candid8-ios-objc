//
//  ASCBaseService.h
//  candid8
//
//  Created by Aldrin Lenny on 20/06/13.
//
//
#import "ASCServiceResponse.h"

#import <AFNetworking/AFNetworking.h>

@interface ASCBaseService : NSObject

@property(nonatomic,strong)NSDictionary * errorResponseDictionary;
- (NSString *)serviceUrl;
- (void)fetchWithUrl:(NSString *)aUrlString andProcesseWithReponseBlock:(ASCServiceResponseBlock)aResponseBlock;
- (void)fetchWithUrl:(NSString *)aUrlString params:(NSDictionary *)aParamsDict andProcesseWithReponseBlock:(ASCServiceResponseBlock)aResponseBlock;
- (id)parseRseponse:(id)aResponseObject;
- (NSDictionary *)parameDicForPutService;

- (NSString *)getDynamicBaseUrlPrefix;
- (void)deleteWithUrl:(NSString *)aUrlString params:(NSDictionary *)aParamsDict andProcesseWithReponseBlock:(ASCServiceResponseBlock)aResponseBlock;
- (void)putWithUrl:(NSString *)aUrlString  andProcesseWithReponseBlock:(ASCServiceResponseBlock)aResponseBlock;
- (void)putWithUrl:(NSString *)aUrlString params:(NSDictionary *)aParamsDict andProcesseWithReponseBlock:(ASCServiceResponseBlock)aResponseBlock;


- (BOOL)isSessionTimedOut:(AFHTTPRequestOperation *)operation;

@end
