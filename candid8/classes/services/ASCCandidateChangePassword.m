//
//  ASCCandidateChangePassword.m
//  candid8
//
//  Created by islet on 14/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "ASCCandidateChangePassword.h"
#import "ASCCandidateProfileService.h"
#import "ASCLoginService.h"
#import "ASCResume.h"
#import <Objection/Objection.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>
@implementation ASCCandidateChangePassword
objection_register_singleton(ASCCandidateChangePassword)
+ (instancetype)sharedInstance{
    return [[JSObjection defaultInjector] getObject:[ASCCandidateChangePassword class]];
}
- (void)updatePasswordWithOldPassword:(NSString *)oldPassword withNewPassword:(NSString *)newPassword withResponseBlock:(ASCServiceResponseBlock)aResponseBlock{
    self.oPassword = oldPassword;
    self.nPassword = newPassword;
    
      NSString *urlString = [NSString stringWithFormat:@"%@/users/%@?access_token=%@",[self getDynamicBaseUrlPrefix], [ASCLoginService sharedInstance].loggedInCandidate.userId,[ASCLoginService sharedInstance].tokenInfo.accessToken];
     [self putWithUrl:urlString  andProcesseWithReponseBlock:aResponseBlock];
}
- (NSDictionary *)parameDicForPutService{
    
    NSMutableDictionary * retDic = [@{} mutableCopy];
    NSMutableDictionary * tmpDic = [@{} mutableCopy];
    tmpDic[@"oldPassword"]       = self.oPassword?self.oPassword:[NSNull null];
    tmpDic[@"newPassword"]       = self.nPassword?self.nPassword:[NSNull null];
    
    
    retDic[@"user"]      = tmpDic;
    return retDic;
}

-(id)parseRseponse:(id)aResponseObject {
    
    //convert JSON to objects
    NSError *error = nil;
    
    
    if (!error) {
        
    }
    return aResponseObject;
}
@end
