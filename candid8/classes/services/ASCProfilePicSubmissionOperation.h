//
//  ASCProfilePicSubmissionOperation.h
//  candid8
//
//  Created by islet on 24/09/15.
//  Copyright © 2015 AppSails. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASCServiceResponse.h"


@interface ASCProfilePicSubmissionOperation : NSOperation

- (instancetype)initWithImage:(UIImage *)anImage named:(NSString *)name withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

@end
