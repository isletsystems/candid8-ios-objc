//
//  ASCLoginService.h
//  candid8
//
//  Created by Aldrin Lenny on 13/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCBaseService.h"
#import "ASCCandindate.h"
#import "candid8-Swift.h"

@interface ASCLoginService : ASCBaseService

@property (nonatomic, strong) ASCCandindate *loggedInCandidate;
@property (nonatomic, strong) ASCTokenInfo *tokenInfo;

+ (instancetype)sharedInstance;

- (void)executeAfterGettingTocken:(ASCServiceAfterRefreshResponseBlock)aResponseBlock;
- (void)loginWithUserName:(NSString *)aUserName password:(NSString *)aPassword andResponseBlock:(ASCServiceResponseBlock)aResponseBlock;



@end
