//
//  ASCResponseSubmissionOperation.h
//  candid8
//
//  Created by Aldrin Lenny on 09/05/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASCJobPosting.h"
#import "ASCJobPostingQuestion.h"
#import "ASCServiceResponse.h"

@class ASCJobSubmissionService;

@interface ASCResponseSubmissionOperation : NSOperation

-(id)initWithJob:(ASCJobPosting *)aJob andQuestion:(ASCJobPostingQuestion *)aQuestion withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

@end
