//
//  ASCCandidateSaveProfile.m
//  candid8
//
//  Created by islet on 11/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "ASCCandidateSaveProfile.h"
#import "ASCCandidateProfileService.h"
#import "ASCLoginService.h"
#import "ASCResume.h"

#import <Objection/Objection.h>
@implementation ASCCandidateSaveProfile
objection_register_singleton(ASCCandidateSaveProfile)

+ (instancetype)sharedInstance  {
    return [[JSObjection defaultInjector] getObject:[ASCCandidateSaveProfile class]];
}

- (void)pushDetailsOfCandidate:(ASCCandindate *)aCandidate withResponseBlock:(ASCServiceResponseBlock)aResponseBlock{
    NSString *urlString = [NSString stringWithFormat:@"%@/candidates/%@",[self getDynamicBaseUrlPrefix], aCandidate.cid];
    [self putWithUrl:urlString andProcesseWithReponseBlock:aResponseBlock];
}

-(id)parseRseponse:(id)aResponseObject {
    
    //convert JSON to objects
    NSError *error = nil;
    if (!error) {
        if ([aResponseObject isKindOfClass:[NSHTTPURLResponse class]]) {
            NSHTTPURLResponse * res = aResponseObject;
            if (res.statusCode == 200) {
                return @{@"Sucess":@YES};
            }
        }
        
    }
    return nil;
}
- (NSDictionary *)parameDicForPutService{
    
    NSMutableDictionary * retDic = [@{} mutableCopy];
    ASCCandindate *c8 = [ASCLoginService sharedInstance].loggedInCandidate;
    retDic[@"first_name"] = c8.firstName?c8.firstName:[NSNull null];
    retDic[@"last_name"] = c8.lastName?c8.lastName:[NSNull null];
    retDic[@"phone"] = c8.phone?c8.phone:[NSNull null];
    retDic[@"photo_file_name"] = c8.photo?c8.photo:[NSNull null];
    retDic[@"user_id"] = c8.userId?c8.userId:[NSNull null];;
    retDic[@"access_token"] =[ASCLoginService sharedInstance].tokenInfo.accessToken;
    
    return retDic;
}

@end
