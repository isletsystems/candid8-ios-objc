//
//  ASCJobFacade.m
//  candid8
//
//  Created by Aldrin Lenny on 27/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobFacade.h"

#import "ASCJobPostingDetailsService.h"
#import "ASCJobDeleteService.h"
#import "ASCJobViewedService.h"
#import "ASCJobListingService.h"
#import "ASCJobApplyByNumberService.h"
#import "ASCJobSearchByNumberService.h"

#import <Objection/Objection.h>

#import "ASCLoginService.h"
#import "candid8-Swift.h"

@implementation ASCJobFacade

objection_register_singleton(ASCJobFacade)

+ (instancetype)sharedInstance
{
    return [[JSObjection defaultInjector] getObject:[ASCJobFacade class]];
}

-(void)initialize {
}

- (void)searchForJobWithNumber:(NSString *)aJobNumber withResponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    [[ASCJobSearchByNumberService sharedInstance] searchJobWithRefNumber:aJobNumber withResponseBlock:aResponseBlock];
}


- (void)applyForJobId:(NSString *)aJobId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    [[ASCJobApplyByNumberService sharedInstance] applyForJobId:aJobId withResponseBlock:aResponseBlock];
}

- (void)fetchJobDetailWithId:(NSString *)aJobPostingId ofCandidateId:(NSString *)aCandidateId andResponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    [[ASCJobPostingDetailsService sharedInstance] fetchJobDetailWithId:aJobPostingId ofCandidateId:aCandidateId andResponseBlock:aResponseBlock];
}

- (void)fetchJobsOfCandidate:(NSString *)aCandidateId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    [[ASCJobListingService sharedInstance] fetchJobsOfCandidate:aCandidateId withResponseBlock:aResponseBlock];
}

- (void)markViewedJob:(NSString *)aJobId ofCandidate:(NSString *)aCandidateId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    [[ASCJobViewedService sharedInstance] markViewedJob:aJobId ofCandidate:aCandidateId withResponseBlock:aResponseBlock];
}

- (void)deleteJob:(ASCJobPosting *)aCandidateJob withResponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    [[ASCJobDeleteService sharedInstance] deleteJob:aCandidateJob withResponseBlock:aResponseBlock];
}

- (void)submitWrittenResponse:(NSString *)text job:(ASCJobPosting *)job question:(ASCJobPostingQuestion *)quest completion:(ASCServiceResponseBlock)aResponseBlock
{
    [[ASCWrittenResponseSubmissionService shared] submitWrittenResponse:text
                                                                  token:[ASCLoginService sharedInstance].tokenInfo.accessToken
                                                                  job:job
                                                               question:quest
                                                                    completion:aResponseBlock];
}

- (void)submitVideoResponseOfQuestion:(ASCJobPostingQuestion *)quest job:(ASCJobPosting *)job completion:(ASCServiceResponseBlock)aResponseBlock
{
    [[ASCVideoResponseSubmissionService shared] submitVideoResponse:[ASCLoginService sharedInstance].tokenInfo.accessToken job:job
                                                           question:quest completion:aResponseBlock];
}
@end
