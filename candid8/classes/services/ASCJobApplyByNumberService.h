//
//  ASCJobApplyByNumberService.h
//  candid8
//
//  Created by Aldrin Lenny on 29/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCPostActionService.h"
#import "ASCBaseService.h"

@interface ASCJobApplyByNumberService : ASCPostActionService

+ (instancetype)sharedInstance;

- (void)applyForJobId:(NSString *)aJobId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

@end
