//
//  ASCCandidateSaveProfile.h
//  candid8
//
//  Created by islet on 11/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "ASCBaseService.h"
#import "ASCCandindate.h"

@interface ASCCandidateSaveProfile : ASCBaseService
+ (instancetype)sharedInstance;
- (void)pushDetailsOfCandidate:(ASCCandindate *)aCandidate withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;
@end
