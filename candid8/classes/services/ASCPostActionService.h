//
//  ASCPostActionService.h
//  candid8
//
//  Created by Aldrin Lenny on 18/10/12.
//
//

#import "ASCPostActionService.h"
#import "ASCBaseService.h"
#import <AFNetworking/AFNetworking.h>

@interface ASCPostActionService : ASCBaseService

+ (id)sharedHTTPClient;

- (void) post:(NSDictionary*)objects
      success:(void (^)(AFHTTPRequestOperation *request, NSDictionary *objects))success
      failure:(void (^)(AFHTTPRequestOperation *request, NSError *error))failure;



- (void) action:(NSString *)aURLAction post:(NSDictionary*)objects
      success:(void (^)(AFHTTPRequestOperation *request, NSDictionary *objects))success
      failure:(void (^)(AFHTTPRequestOperation *request, NSError *error))failure;


- (void) action:(NSString *)aURLAction post:(NSDictionary*)parameters
      onManager:(AFHTTPRequestOperationManager *)manager
        success:(void (^)(AFHTTPRequestOperation *request, NSDictionary *objects))success
        failure:(void (^)(AFHTTPRequestOperation *request, NSError *error))failure;

@end
