//
//  ASCJobFacade.h
//  candid8
//
//  Created by Aldrin Lenny on 27/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASCServiceResponse.h"
#import "ASCJobPosting.h"


@interface ASCJobFacade : NSObject

+ (instancetype)sharedInstance;

- (void)searchForJobWithNumber:(NSString *)aJobNumber withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

- (void)applyForJobId:(NSString *)aJobId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

- (void)fetchJobsOfCandidate:(NSString *)aCandidateId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

- (void)fetchJobDetailWithId:(NSString *)aJobPostingId ofCandidateId:(NSString *)aCandidateId andResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

- (void)markViewedJob:(NSString *)aJobId ofCandidate:(NSString *)aCandidateId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

- (void)deleteJob:(ASCJobPosting *)aCandidateJob withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

- (void)submitWrittenResponse:(NSString *)text job:(ASCJobPosting *)job question:(ASCJobPostingQuestion *)quest completion:(ASCServiceResponseBlock)aResponseBlock;

- (void)submitVideoResponseOfQuestion:(ASCJobPostingQuestion *)quest job:(ASCJobPosting *)job completion:(ASCServiceResponseBlock)aResponseBlock;

@end
