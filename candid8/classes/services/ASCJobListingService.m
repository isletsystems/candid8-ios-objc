//
//  ASCJobListingService.m
//  candid8
//
//  Created by Aldrin Lenny on 27/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobListingService.h"

#import <Objection/Objection.h>
#import "ASCJobPosting.h"
#import "ASCLoginService.h"

@implementation ASCJobListingService
objection_register_singleton(ASCJobListingService)
+ (instancetype)sharedInstance
{
    return [[JSObjection defaultInjector] getObject:[ASCJobListingService class]];
}

- (void)fetchJobsOfCandidate:(NSString *)aCandidateId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@/candidateJobs",[self getDynamicBaseUrlPrefix]];
    NSDictionary *params = @{@"access_token" : [ASCLoginService sharedInstance].tokenInfo.accessToken};
    [self fetchWithUrl:urlString params:params andProcesseWithReponseBlock:aResponseBlock];
}

-(id)parseRseponse:(id)aResponseObject {
    
    //convert JSON to objects
    NSError *error = nil;
    
    NSArray *employers = aResponseObject[@"employeeList"];
    NSMutableDictionary *parsedEmployers = [@{} mutableCopy];
    for (NSDictionary *empDict in employers) {
        ASCJobProvider *emp = [[ASCJobProvider alloc] initWithDictionary:empDict error:&error];
        if (!error) {
            [parsedEmployers setObject:emp forKey:emp.eid];
        }
    }
    
    NSArray *answers = aResponseObject[@"answers"];
    NSMutableDictionary *parsedAnswers = [@{} mutableCopy];
    for (NSDictionary *ansDict in answers) {
        ASCJobPostingResponse *ans = [[ASCJobPostingResponse alloc] initWithDictionary:ansDict error:&error];
        if (!error) {
            [parsedAnswers setObject:ans forKey:ans.qid];
        }
    }
    
    
    NSArray *questions = aResponseObject[@"jobQuestions"];
    NSMutableDictionary *parsedQuestions = [@{} mutableCopy];
    for (NSDictionary *questDict in questions) {
        ASCJobPostingQuestion *quest = [[ASCJobPostingQuestion alloc] initWithDictionary:questDict error:&error];
        ASCJobPostingResponse *ans = parsedAnswers[quest.qid];
        if (ans) {
            quest.answer = ans;
        }
        if (!error) {
            [parsedQuestions setObject:quest forKey:quest.qid];
        }
    }
    
    NSArray *jobs = aResponseObject[@"candidateJobs"];
    NSMutableArray *parsedJobs = [@[] mutableCopy];
    for (NSDictionary *jobDict in jobs) {
        ASCJobPosting *job = [[ASCJobPosting alloc] initWithDictionary:jobDict error:&error];
        if (!error) {
            job.employer = [parsedEmployers objectForKey:job.employerId];
            NSArray *jobQuestionIds = jobDict[@"jobQuestions"];
            NSMutableArray<ASCJobPostingQuestion, Optional> *jobQuestions = [@[] mutableCopy];
            for (NSNumber *qid in jobQuestionIds) {
                ASCJobPostingQuestion *quest = parsedQuestions[qid.stringValue];
                if (quest) {
                    [jobQuestions addObject:quest];
                }
            }
            job.questions = jobQuestions;
            [parsedJobs addObject:job];
        }
    }
    return parsedJobs;
}

@end
