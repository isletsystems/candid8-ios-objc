//
//  KIServiceResponse.h
//  candid8
//
//  Created by Aldrin Lenny on 15/06/13.
//
//

#ifndef candid8_ASCServiceResponse_h
#define candid8_ASCServiceResponse_h

typedef void (^ASCServiceResponseBlock)(BOOL isSuccess, id resultData);
typedef void (^ASCServiceAfterRefreshResponseBlock)(NSString * accessToken);
#endif
