//
//  ASCCandidateProfileSubmissionService.m
//  candid8
//
//  Created by islet on 24/09/15.
//  Copyright © 2015 AppSails. All rights reserved.
//

#import "ASCCandidateProfileSubmissionService.h"
#import <Objection/Objection.h>
#import "ASCProfilePicSubmissionOperation.h"

@interface ASCCandidateProfileSubmissionService ()

@property (nonatomic, strong) NSOperationQueue *submissionQueue;

@end
@implementation ASCCandidateProfileSubmissionService
objection_register_singleton(ASCCandidateProfileSubmissionService)
+ (instancetype)sharedInstance
{
    return [[JSObjection defaultInjector] getObject:[ASCCandidateProfileSubmissionService class]];
}
- (void)pushImage:(UIImage *)image named:(NSString *)name withResponseBlock:(ASCServiceResponseBlock)aResponseBlock{
    if (!self.submissionQueue)
    {
        self.submissionQueue = [[NSOperationQueue alloc] init];
        self.submissionQueue.name = @"C8 Profile Image Queue";
        self.submissionQueue.maxConcurrentOperationCount = 1;
    }
    ASCProfilePicSubmissionOperation * anOperation = [[ASCProfilePicSubmissionOperation alloc] initWithImage:image named:name withResponseBlock:^(BOOL isSuccess, id resultData) {
        aResponseBlock(TRUE,@YES);
        
    }];
    [self.submissionQueue addOperation:anOperation];
    

}
@end
