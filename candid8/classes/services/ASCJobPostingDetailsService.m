//
//  ASCJobPostingDetailsService.m
//  candid8
//
//  Created by Aldrin Lenny on 14/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobPostingDetailsService.h"
#import "ASCJobPosting.h"
#import "ASCLoginService.h"

#import <Objection/Objection.h>


@implementation ASCJobPostingDetailsService


objection_register_singleton(ASCJobPostingDetailsService)

+ (ASCJobPostingDetailsService *)sharedInstance {
    return [[JSObjection defaultInjector] getObject:[ASCJobPostingDetailsService class]];
}

- (void)fetchJobDetailWithId:(NSString *)aJobPostingId ofCandidateId:(NSString *)aCandidateId andResponseBlock:(ASCServiceResponseBlock)aResponseBlock {
    
    NSString *urlString = [NSString stringWithFormat:@"%@/job_dts.json?jobid=%@&cid=%@",[self getDynamicBaseUrlPrefix], aJobPostingId, aCandidateId];
	
    [self fetchWithUrl:urlString andProcesseWithReponseBlock:aResponseBlock];
}

- (void)fetchJobDetailWithId:(NSString *)aJobPostingId andResponseBlock:(ASCServiceResponseBlock)aResponseBlock {
    
    NSString *urlString = [NSString stringWithFormat:@"%@/candidateJobs/%@",[self getDynamicBaseUrlPrefix], aJobPostingId];
    NSDictionary *params = @{@"access_token" : [ASCLoginService sharedInstance].tokenInfo.accessToken};
    [self fetchWithUrl:urlString params:params andProcesseWithReponseBlock:aResponseBlock];
}

-(id)parseRseponse:(id)aResponseObject {
    
    //convert JSON to objects
    NSError *error = nil;
    
    //extract questions
    NSArray *questions = aResponseObject[@"jobQuestions"];
    NSMutableArray<ASCJobPostingQuestion, Optional> *jobQuestions = [@[] mutableCopy];
    NSMutableDictionary *qidToQuestionDict = [@{} mutableCopy];
    for (NSDictionary *questDict in questions) {
        ASCJobPostingQuestion *quest = [[ASCJobPostingQuestion alloc] initWithDictionary:questDict error:&error];
        if (!error) {
            [jobQuestions addObject:quest];
            [qidToQuestionDict setObject:quest forKey:quest.qid];
        }
    }
 
    //extract answers
    NSArray *answers = aResponseObject[@"answers"];
    for (NSDictionary *ansDict in answers) {
        ASCJobPostingResponse *ans = [[ASCJobPostingResponse alloc] initWithDictionary:ansDict error:&error];
        if (!error) {
            ASCJobPostingQuestion *quest = [qidToQuestionDict objectForKey:ans];
            if (quest) {
                quest.answer = ans;
            }
        }
    }
    
    ASCJobPosting *job = self.selectedJob;
//    job.questions = nil;
    
    return job;
}


//-(id)parseRseponse:(id)aResponseObject {
//    
//    //convert JSON to objects
//    BOOL success = [aResponseObject[@"success"] boolValue];
//    if (!success) {
//        return nil;
//    }
//    NSError *error = nil;
//    ASCJobPosting *c8Job = [[ASCJobPosting alloc] initWithDictionary:aResponseObject[@"job"] error:&error];
//    return c8Job;
//}


@end
