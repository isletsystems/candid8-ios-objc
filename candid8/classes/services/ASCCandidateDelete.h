//
//  ASCCandidateDelete.h
//  candid8
//
//  Created by islet on 22/09/15.
//  Copyright © 2015 AppSails. All rights reserved.
//

#import "ASCBaseService.h"

@interface ASCCandidateDelete : ASCBaseService

+ (instancetype)sharedInstance;
- (void)deleteAccountForCandidate:(NSString *)aCandidateId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;
@end
