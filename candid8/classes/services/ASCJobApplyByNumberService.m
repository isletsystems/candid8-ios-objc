//
//  ASCJobApplyByNumberService.m
//  candid8
//
//  Created by Aldrin Lenny on 29/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobApplyByNumberService.h"

#import <Objection/Objection.h>
#import "ASCLoginService.h"

@implementation ASCJobApplyByNumberService
objection_register_singleton(ASCJobApplyByNumberService)
+ (instancetype)sharedInstance
{
    return [[JSObjection defaultInjector] getObject:[ASCJobApplyByNumberService class]];
}

- (void)applyForJobId:(NSString *)aJobId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@/candidateJobs",[self getDynamicBaseUrlPrefix]];
    /*
     {"candidateJob"=>{"mail_sent_status"=>false, "is_shortlist"=>nil, "comment"=>nil, "score"=>nil, "resume_file_name"=>nil, "resume_content_type"=>nil, "resume_file_size"=>nil, "resume_updated_at"=>nil, "publish"=>nil, "created_at"=>nil, "updated_at"=>nil, "job_applied"=>false, "tot_questions"=>nil, "answerd_questions"=>nil, "del_status"=>false, "job_viewd"=>false, "reject_mail_status"=>false, "ans_viewd"=>nil, "interview_date"=>nil, "note"=>nil, "meeting_type"=>nil, "time_zone_value"=>nil, "time_zone_text"=>nil, "shared"=>false, "job_applied_date"=>nil, "candidate_id"=>nil, "job_id"=>"293", "jobQuestions"=>nil, "answers"=>nil}, "candidate_job"=>{}}
     */
    
    NSMutableDictionary *dataDict = [@{@"candidateJob": @{@"job_id":aJobId},@"access_token" : [ASCLoginService sharedInstance].tokenInfo.accessToken} mutableCopy];
    [[ASCPostActionService sharedHTTPClient]  action:urlString post:dataDict
                                             success:^(AFHTTPRequestOperation *request, NSDictionary *objects) {
                                                 aResponseBlock(TRUE, objects);
                                             }
                                             failure:^(AFHTTPRequestOperation *request, NSError *error){
                                                 aResponseBlock(FALSE, error);
                                             }];
}

@end
