//
//  ASCJobSearchByNumberService.h
//  candid8
//
//  Created by Aldrin ML on 7/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "ASCBaseService.h"

@interface ASCJobSearchByNumberService : ASCBaseService

+ (ASCJobSearchByNumberService *)sharedInstance;

- (void)searchJobWithRefNumber:(NSString *)aRefNumber withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

@end
