//
//  ASCJobDeleteService.m
//  candid8
//
//  Created by Aldrin Lenny on 27/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobDeleteService.h"
#import <Objection/Objection.h>
#import "ASCLoginService.h"

@implementation ASCJobDeleteService
objection_register_singleton(ASCJobDeleteService)
+ (instancetype)sharedInstance
{
    return [[JSObjection defaultInjector] getObject:[ASCJobDeleteService class]];
}

- (void)deleteJob:(ASCJobPosting *)aJob withResponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@/candidateJobs/%@",[self getDynamicBaseUrlPrefix], aJob.jid];
    NSMutableDictionary *dataDict = [@{@"candidateJob" : @{@"job_id":aJob.jobId, @"del_status" : @"true"},
                                       @"access_token" :[ASCLoginService sharedInstance].tokenInfo.accessToken} mutableCopy];
    
    [self putWithUrl:urlString params:dataDict andProcesseWithReponseBlock:aResponseBlock];
}

@end
