//
//  ASCJobDeleteService.h
//  candid8
//
//  Created by Aldrin Lenny on 27/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCBaseService.h"
#import "ASCJobPosting.h"

@interface ASCJobDeleteService : ASCBaseService

+ (instancetype)sharedInstance;

- (void)deleteJob:(ASCJobPosting *)aJob withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

@end
