//
//  ASCProfilePicSubmissionOperation.m
//  candid8
//
//  Created by islet on 24/09/15.
//  Copyright © 2015 AppSails. All rights reserved.
//

#import "ASCProfilePicSubmissionOperation.h"
#import "ASCCandindate.h"
#import "ASCCandidateProfileImageSign.h"
#import "ASCS3Manager.h"
#import "ASCLoginService.h"

NSString *const C8S3_IMAGE_TYPE_JPG = @"image/jpeg";
NSString *const C8S3_IMAGE_TYPE_EXTENSION_JPG = @"jpg";

NSString *const C8S3_IMAGE_TYPE_PNG = @"image/png";
NSString *const C8S3_IMAGE_TYPE_EXTENSION_PNG = @"png";

@interface ASCProfilePicSubmissionOperation ()

@property (nonatomic, strong) ASCServiceResponseBlock responseBlock;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, readwrite) BOOL responseComplete;


@end

@implementation ASCProfilePicSubmissionOperation

- (instancetype)initWithImage:(UIImage *)anImage named:(NSString *)name withResponseBlock:(ASCServiceResponseBlock)aResponseBlock{
    if (self == [super init]) {
        self.responseBlock = aResponseBlock;
        self.image = anImage;
        self.imageName = name;
        self.responseComplete = false;
    }
    return self;
}
#pragma mark -
#pragma mark - Main operation

- (void)main
{
    @autoreleasepool {
        if (self.isCancelled)
            return;
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:self.imageName];
        
        // Convert UIImage object into NSData (a wrapper for a stream of bytes) formatted according to PNG spec
        NSData *imageData = UIImageJPEGRepresentation(self.image, 1.0);
        BOOL status = [imageData writeToFile:filePath atomically:YES];
        if (!status && self.responseBlock) {
            self.responseBlock(NO, @"Upload failed!");
            return;
        }
        DLog(@"file write status: %d, %@", status, filePath);
        
        NSString *s3ProfileItemKey =  [self getProfileKeyForCandidate];
        DLog(@"s3ProfileItemKey %@",s3ProfileItemKey);
        NSString *s3ProfileItemUrl = [NSString stringWithFormat:@"https://%@.%@/%@",C8S3_BUCKET,C8S3_ENDPOINT,s3ProfileItemKey];
        
        ASCServiceResponseBlock respBlk = self.responseBlock;
        
        ASCS3Manager *s3Uploader = [ASCS3Manager sharedInstance];
        [s3Uploader uploadFromLocalUrl:filePath withBucketItemName:s3ProfileItemKey andContentType:@"image/jpeg" withResponseBlock:^(BOOL status, double percentComplete) {
            if (status && percentComplete == 100 && respBlk && !self.responseComplete) {
                self.responseComplete = true;
                [ASCLoginService sharedInstance].loggedInCandidate.photo = s3ProfileItemKey;
                respBlk(TRUE, s3ProfileItemUrl);
            }
        }];
    }
}

- (NSString *)getProfileKeyForCandidate
{
    NSString *cid = [ASCLoginService sharedInstance].loggedInCandidate.cid;
    NSString *key = [NSString stringWithFormat:@"can/c%@/%.0f-c%@-profile.jpg",
                     cid,[[NSDate new] timeIntervalSince1970],  cid];
    return key;
}

@end
