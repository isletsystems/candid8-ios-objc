//
//  ASCCandidateProfileImageSign.m
//  candid8
//
//  Created by islet on 24/09/15.
//  Copyright © 2015 AppSails. All rights reserved.
//

#import "ASCCandidateProfileImageSign.h"
#import <Objection/Objection.h>
#import "ASCLoginService.h"


@implementation ASCCandidateProfileImageSign
objection_register_singleton(ASCCandidateProfileImageSign)
+ (instancetype)sharedInstance
{
    return [[JSObjection defaultInjector] getObject:[ASCCandidateProfileImageSign class]];
}

- (void)fetchImageSavingDetailForName:(NSString *)name type:(NSString *)type size:(NSNumber *)sizeinByte withResponseBlock:(ASCServiceResponseBlock)aResponseBlock{
    NSString *urlString = [NSString stringWithFormat:@"%@/candidates/sign",[self getDynamicBaseUrlPrefix]];
    NSDictionary *params = @{@"access_token" : [ASCLoginService sharedInstance].tokenInfo.accessToken,
                             @"name":name.length>0?name:@"",
                             @"type":type.length>0?type:@"",
                             @"size":sizeinByte?sizeinByte:@0};
    [self fetchWithUrl:urlString params:params andProcesseWithReponseBlock:aResponseBlock];

}
-(id)parseRseponse:(id)aResponseObject {
    
    //convert JSON to objects

    return aResponseObject;
}
@end
