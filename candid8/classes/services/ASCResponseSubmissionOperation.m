//
//  ASCResponseSubmissionOperation.m
//  candid8
//
//  Created by Aldrin Lenny on 09/05/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCResponseSubmissionOperation.h"
#import "ASCJobSubmissionService.h"
#import "ASCS3Manager.h"

#import "ASCJobPosting.h"
#import "ASCJobPostingQuestion.h"



NSString *const C8S3_VIDEO_TYPE = @"video/mp4";
NSString *const C8S3_IMAGE_TYPE = @"image/jpeg";
NSString *const C8S3_VIDEO_TYPE_EXTENSION = @"mp4";
NSString *const C8S3_IMAGE_TYPE_EXTENSION = @"jpg";

@interface ASCResponseSubmissionOperation ()

@property (nonatomic, strong) ASCJobPosting *jobPosting;
@property (nonatomic, strong) ASCJobPostingQuestion *jobQuestion;
@property (nonatomic, strong) ASCServiceResponseBlock responseBlock;

@end

@implementation ASCResponseSubmissionOperation

- (id)initWithJob:(ASCJobPosting *)aJob andQuestion:(ASCJobPostingQuestion *)aQuestion withResponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    if (self = [super init])
    {
        self.jobPosting = aJob;
        self.jobQuestion = aQuestion;
        self.responseBlock = aResponseBlock;
    }
    return self;
}

#pragma mark -
#pragma mark - Main operation
- (void)main
{
    @autoreleasepool
    {
        if (self.isCancelled)
            return;
        
        __block BOOL saveInProgress = FALSE;
        
        //if its a skipped job, just submit and return
        
        ASCJobPosting *job = self.jobPosting;
        ASCJobPostingQuestion *quest = self.jobQuestion;
        ASCServiceResponseBlock respBlk = self.responseBlock;
        
        ASCResponseMode respMode = ASCResponseModePending;
        
        //check if there are any locally answered data, take that as overriding the cloud one
        NSDictionary *respDict = [[ASCJobSubmissionService sharedInstance] getLocalResponseDictForQuestion:quest onJob:job];
        if (respDict)
        {
            //yes there is local data, lets pick that
            respMode = [respDict[C8_RESPMODE_KEY] integerValue];
        }
        
        if (respMode == ASCResponseModeSkip || respMode == ASCResponseModeWrite)
        {
            [[ASCJobSubmissionService sharedInstance] submitQuestion:quest onJob:job withResponseHandler:^(BOOL isSuccess, id resultData) {
                if (isSuccess) {
                    DLog(@"returned data: %@", resultData);
                    respBlk(TRUE, @"Upload opted out Job Question Answer OK!");
                } else {
                    
                    DLog(@"NOK %@",resultData);
                    respBlk(FALSE, @"Upload opted out Job Question Answer NOT OK!");
                }
            }];
            // all good return.
            return;
        }
        
        //else we need to do the video upload first
        
        NSString *s3VideoItemKey = [[ASCJobSubmissionService sharedInstance] getVideoKeyForQuestion:self.jobQuestion onJob:self.jobPosting];
        NSString *s3VideoLocalPath = [[ASCJobSubmissionService sharedInstance] localPathToMovieForQuestion:self.jobQuestion onJob:self.jobPosting];
        
        ASCS3Manager *s3Uploader = [ASCS3Manager sharedInstance];
        [s3Uploader uploadFromLocalUrl:s3VideoLocalPath withBucketItemName:s3VideoItemKey andContentType:C8S3_VIDEO_TYPE withResponseBlock:^(BOOL status, double uploadPercent) {
            if (status)
            {
                if (uploadPercent == 100 && !saveInProgress )
                {
                    saveInProgress = TRUE;
                    DLog(@"S3 vieo upload completed for jobid:%@",job.jid);
                    
                    //save still image also to S3
                    NSString *s3StillImageItemKey = [s3VideoItemKey stringByReplacingOccurrencesOfString:C8S3_VIDEO_TYPE_EXTENSION withString:C8S3_IMAGE_TYPE_EXTENSION];
                    NSString *s3StillImageLocalPath = [s3VideoLocalPath stringByReplacingOccurrencesOfString:C8S3_VIDEO_TYPE_EXTENSION withString:C8S3_IMAGE_TYPE_EXTENSION];
                    [s3Uploader uploadFromLocalUrl:s3StillImageLocalPath withBucketItemName:s3StillImageItemKey andContentType:C8S3_IMAGE_TYPE withResponseBlock:^(BOOL status, double uploadImagePercent) {
                        if (status && uploadImagePercent == 100)
                        {
                            DLog(@"S3 image upload success for jobid:%@",job.jid);
                            //delete local image
                            NSError *fileDeleteError;
                            [[NSFileManager defaultManager] removeItemAtPath:s3StillImageLocalPath error:&fileDeleteError];
                            DLog(@"c8 local image deleted after successfull s3 upload %@", fileDeleteError?fileDeleteError:@"");
                        }
                    }];
                    
                    [[ASCJobSubmissionService sharedInstance] submitQuestion:quest onJob:job withResponseHandler:^(BOOL isSuccess, id resultData) {
                        if (isSuccess) {
                            DLog(@"returned data: %@", resultData);
                            //delete local video
                            NSError *fileDeleteError;
                            [[NSFileManager defaultManager] removeItemAtPath: s3VideoLocalPath error:&fileDeleteError];
                            
                            respBlk(TRUE, @"Upload Job Question Answer OK!");
                        } else {
                            
                            DLog(@"NOK %@",resultData);
                            respBlk(FALSE, @"Upload Job Question Answer NOT OK!");
                        }
                    }];
                }
            } else {
                //show toast
                DLog(@"S3 upload failed");
                respBlk(FALSE, @"Upload Job Question Answer NOT OK!");
            }
        }];
    }
}

@end
