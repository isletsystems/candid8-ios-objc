//
//  ASCForgotPasswordService.m
//  candid8
//
//  Created by Aldrin Lenny on 14/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCForgotPasswordService.h"
#import "ASCPostActionService.h"

#import <Objection/Objection.h>

@implementation ASCForgotPasswordService

objection_register_singleton(ASCForgotPasswordService)

+ (ASCForgotPasswordService *)sharedInstance {
    return [[JSObjection defaultInjector] getObject:[ASCForgotPasswordService class]];
}

- (void)resetPasswordWithEmailId:(NSString *)anEmailId andResponseBlock:(ASCServiceResponseBlock)aResponseBlock {

    [[ASCPostActionService sharedHTTPClient] action:[NSString stringWithFormat:@"%@/emails",[self getDynamicBaseUrlPrefix]] post:@{@"email" : @{@"typ":@"reset-password", @"emailIds":anEmailId}} success:^(AFHTTPRequestOperation *request, NSDictionary *objects) {
        
        if (aResponseBlock) {
            aResponseBlock(TRUE,objects);
        }
    } failure:^(AFHTTPRequestOperation *request, NSError *error) {
        if (aResponseBlock) {
            aResponseBlock(FALSE,error);
        }
    }];
}

@end
