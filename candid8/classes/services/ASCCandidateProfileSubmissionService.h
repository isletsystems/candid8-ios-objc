//
//  ASCCandidateProfileSubmissionService.h
//  candid8
//
//  Created by islet on 24/09/15.
//  Copyright © 2015 AppSails. All rights reserved.
//

#import "ASCBaseService.h"

@interface ASCCandidateProfileSubmissionService : ASCBaseService
+ (instancetype)sharedInstance;
- (void)pushImage:(UIImage *)image named:(NSString*)name withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;
@end
