//
//  ASCCandidateResumeDeleteService.m
//  candid8
//
//  Created by islet on 11/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "ASCCandidateResumeDeleteService.h"
#import "ASCResume.h"
#import "ASCLoginService.h"

#import <Objection/Objection.h>
@implementation ASCCandidateResumeDeleteService
objection_register_singleton(ASCCandidateResumeDeleteService)
+ (instancetype)sharedInstance
{
    return [[JSObjection defaultInjector] getObject:[ASCCandidateResumeDeleteService class]];
}

- (void)deleteResumeWith:(NSString *)aResumeId  withResponseBlock:(ASCServiceResponseBlock)aResponseBlock{
    NSString *urlString = [NSString stringWithFormat:@"%@/candidateResumes/%@",[self getDynamicBaseUrlPrefix], aResumeId];
    NSDictionary *params = @{@"access_token" : [ASCLoginService sharedInstance].tokenInfo.accessToken};
    [self deleteWithUrl:urlString params:params andProcesseWithReponseBlock:aResponseBlock];
}
-(id)parseRseponse:(id)aResponseObject {
    
    //convert JSON to objects
    NSError *error = nil;
    if (!error) {
        if ([aResponseObject isKindOfClass:[NSHTTPURLResponse class]]) {
            NSHTTPURLResponse * res = aResponseObject;
            if (res.statusCode == 200) {
                return @{@"Sucess":@YES};
            }
        }
        
    }
    return nil;
}


@end
