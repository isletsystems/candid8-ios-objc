//
//  ASCJobViewedService.m
//  candid8
//
//  Created by Aldrin Lenny on 27/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobViewedService.h"

#import <Objection/Objection.h>

@implementation ASCJobViewedService
objection_register_singleton(ASCJobViewedService)
+ (instancetype)sharedInstance
{
    return [[JSObjection defaultInjector] getObject:[ASCJobViewedService class]];
}

- (void)markViewedJob:(NSString *)aJobId ofCandidate:(NSString *)aCandidateId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@/jobViewed.json?cid=%@&jid=%@&viewed=yes",[self getDynamicBaseUrlPrefix], aCandidateId, aJobId];

    [self fetchWithUrl:urlString andProcesseWithReponseBlock:aResponseBlock];
}

-(id)parseRseponse:(id)aResponseObject {
    
    //convert JSON to objects
    BOOL success = [aResponseObject[@"success"] boolValue];
    if (!success) {
        return nil;
    }
    return @(YES);
}


@end
