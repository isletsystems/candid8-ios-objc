//
//  ASCCandidateChangePassword.h
//  candid8
//
//  Created by islet on 14/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "ASCBaseService.h"

@interface ASCCandidateChangePassword : ASCBaseService

@property(nonatomic,strong)NSString * oPassword;
@property(nonatomic,strong)NSString * nPassword;

+ (instancetype)sharedInstance;
- (void)updatePasswordWithOldPassword:(NSString *)oldPassword withNewPassword:(NSString *)newPassword withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;
@end
