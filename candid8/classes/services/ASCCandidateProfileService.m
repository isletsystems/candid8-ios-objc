//
//  ASCCandidateProfileService.m
//  candid8
//
//  Created by Aldrin ML on 28/08/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "ASCCandidateProfileService.h"
#import "ASCLoginService.h"
#import "ASCResume.h"

#import <Objection/Objection.h>

@implementation ASCCandidateProfileService
objection_register_singleton(ASCCandidateProfileService)
+ (instancetype)sharedInstance
{
    return [[JSObjection defaultInjector] getObject:[ASCCandidateProfileService class]];
}

- (void)fetchProfileOfCandidate:(NSString *)aCandidateId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@/candidates/%@",[self getDynamicBaseUrlPrefix], aCandidateId];
    NSDictionary *params = @{@"access_token" : [ASCLoginService sharedInstance].tokenInfo.accessToken};
    [self fetchWithUrl:urlString params:params andProcesseWithReponseBlock:aResponseBlock];
}

-(id)parseRseponse:(id)aResponseObject {
    
    //convert JSON to objects
    NSError *error = nil;
    
    ASCCandindate *c8 = [ASCLoginService sharedInstance].loggedInCandidate;
    //TODO: the candidate data fields coming back from service to be made uniform across.
    //parse if need be.
    
    if (!error) {
        NSArray *resumes = aResponseObject[@"candidateResumes"];
        NSMutableArray<ASCResume, Optional> *parsedResumes = [@[] mutableCopy];
        for (NSDictionary *resDict in resumes) {
            ASCResume *res = [[ASCResume alloc] initWithDictionary:resDict error:&error];
            if (!error) {
                [parsedResumes addObject:res];
            }
        }
        c8.resumes = parsedResumes;
        c8.photo = aResponseObject[@"candidate"][@"photo_file_name"];
        c8.createdAtStr = aResponseObject[@"candidate"][@"created_at"];
        c8.phone = aResponseObject[@"candidate"][@"phone"];
        c8.userId = aResponseObject[@"candidate"][@"user_id"];
//        self.profileDicForCandidate = aResponseObject[@"candidate"];
    }
    return c8;
}
@end
