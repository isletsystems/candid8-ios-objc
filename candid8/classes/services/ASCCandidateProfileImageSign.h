//
//  ASCCandidateProfileImageSign.h
//  candid8
//
//  Created by islet on 24/09/15.
//  Copyright © 2015 AppSails. All rights reserved.
//

#import "ASCBaseService.h"

@interface ASCCandidateProfileImageSign : ASCBaseService
+ (instancetype)sharedInstance;

- (void)fetchImageSavingDetailForName:(NSString *)name type:(NSString *)type size:(NSNumber *)sizeinByte  withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

@end
