//
//  ASCCandidateProfileService.h
//  candid8
//
//  Created by Aldrin ML on 28/08/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "ASCBaseService.h"

@interface ASCCandidateProfileService : ASCBaseService
//@property (nonatomic,strong)NSDictionary * profileDicForCandidate;

+ (instancetype)sharedInstance;

- (void)fetchProfileOfCandidate:(NSString *)aCandidateId withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;
@end
