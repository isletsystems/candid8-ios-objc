//
//  ASCJobSearchByNumberService.m
//  candid8
//
//  Created by Aldrin ML on 7/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "ASCJobSearchByNumberService.h"
#import "ASCLoginService.h"

#import <Objection/Objection.h>

@implementation ASCJobSearchByNumberService

objection_register_singleton(ASCJobSearchByNumberService)

+ (ASCJobSearchByNumberService *)sharedInstance
{
    return [[JSObjection defaultInjector] getObject:[ASCJobSearchByNumberService class]];
}

- (void)searchJobWithRefNumber:(NSString *)aRefNumber withResponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    NSString *urlString = [NSString stringWithFormat:@"%@/jobs?ref_no=%@",[self getDynamicBaseUrlPrefix], aRefNumber];
    NSDictionary *params = @{@"access_token" : [ASCLoginService sharedInstance].tokenInfo.accessToken};
    [self fetchWithUrl:urlString params:params andProcesseWithReponseBlock:aResponseBlock];
}
    
    
-(id)parseRseponse:(id)aResponseObject {
    
    //convert JSON to objects
    NSError *error = nil;
    
    NSArray *employers = aResponseObject[@"employers"];
    NSMutableDictionary *parsedEmployers = [@{} mutableCopy];
    for (NSDictionary *empDict in employers) {
        ASCJobProvider *emp = [[ASCJobProvider alloc] initWithDictionary:empDict error:&error];
        if (!error) {
            [parsedEmployers setObject:emp forKey:emp.eid];
        }
    }
    
    NSArray *questions = aResponseObject[@"jobQuestions"];
    NSMutableDictionary *parsedQuestions = [@{} mutableCopy];
    for (NSDictionary *questDict in questions) {
        ASCJobPostingQuestion *quest = [[ASCJobPostingQuestion alloc] initWithDictionary:questDict error:&error];
        if (!error) {
            [parsedQuestions setObject:quest forKey:quest.qid];
        }
    }
    
    NSArray *jobs = aResponseObject[@"jobs"];
    NSMutableArray *parsedJobs = [@[] mutableCopy];
    for (NSDictionary *jobDict in jobs) {
        ASCJobPosting *job = [[ASCJobPosting alloc] initWithDictionary:jobDict error:&error];
        if (!error) {
            job.employer = [parsedEmployers objectForKey:job.employerId];
            NSArray *jobQuestionIds = jobDict[@"jobQuestion"];
            NSMutableArray<ASCJobPostingQuestion, Optional> *jobQuestions = [@[] mutableCopy];
            for (NSNumber *qid in jobQuestionIds) {
                ASCJobPostingQuestion *quest = parsedQuestions[qid.stringValue];
                if (quest) {
                    [jobQuestions addObject:quest];
                }
            }
            job.questions = jobQuestions;
            [parsedJobs addObject:job];
        }
    }
    return parsedJobs;
}

@end
