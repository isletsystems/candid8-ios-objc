//
//  ASCJobSubmissionService.m
//  candid8
//
//  Created by Aldrin Lenny on 08/05/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobSubmissionService.h"
#import "ASCLoginService.h"
#import "ASCS3Manager.h"
#import "ASCPostActionService.h"
#import "ASCResponseSubmissionOperation.h"
#import "ASCJobPostingDetailsService.h"


#import <Objection/Objection.h>

NSString *const C8_QUESTION_OPTED_OUT_KEY_FORMAT = @"c8_jid%@_qid%@_optedout";
NSString *const C8_QUESTION_RESPONSEMODE_KEY_FORMAT = @"c8_jid%@_qid%@_answermode";

NSString *const C8_QUESTION_RESPONSEDICT_KEY_FORMAT = @"c8_jid%@_qid%@_respdict";

NSString *const C8_QUESTION_VIDEO_ANSWERED_KEY = @"c8_jid%@_qid%@_ans.mp4";
NSString *const C8_QUESTION_VIDEO_ANSWERED_KEY_WITHFOLDERS = @"emp/e%@/job/j%@/can/c%@/c8_j%@_q%@_vdo_ans.mp4";

@interface ASCJobSubmissionService ()

@property (nonatomic, strong) NSOperationQueue *submissionQueue;

@end

@implementation ASCJobSubmissionService

objection_register_singleton(ASCJobSubmissionService)

+ (ASCJobSubmissionService *)sharedInstance
{
    return [[JSObjection defaultInjector] getObject:[ASCJobSubmissionService class]];
}

- (void)submitQuestion:(ASCJobPostingQuestion *)aQuest onJob:(ASCJobPosting *)aJob withResponseHandler:(ASCServiceResponseBlock)aServiceResponseBlock
{
    ASCResponseMode respMode = [aQuest.answer getResponseMode];
    if (respMode == ASCResponseModePending) {
        //check local stuff
        respMode = aQuest.answer.answerType.integerValue;
    }
    if (respMode == ASCResponseModeRecord)
    {
        [[ASCJobFacade sharedInstance] submitVideoResponseOfQuestion:aQuest job:aJob completion:aServiceResponseBlock];
    }
    NSString *writtenText = @"";
    if (respMode == ASCResponseModeWrite)
    {
        writtenText = aQuest.answer.writtenText;
        [[ASCJobFacade sharedInstance] submitWrittenResponse:writtenText job:aJob question:aQuest completion:aServiceResponseBlock];
    }
}

- (void)submitJob:(ASCJobPosting *)aJob withResponseHandler:(ASCServiceResponseBlock)aServiceResponseBlock
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@/candidateJobs/%@",[self getDynamicBaseUrlPrefix], aJob.jid];
    NSMutableDictionary *dataDict = [@{@"candidateJob" : @{@"job_id":aJob.jobId, @"job_applied" : @"true"},
                                       @"access_token" :[ASCLoginService sharedInstance].tokenInfo.accessToken} mutableCopy];
    DLog(@"submission params: %@",dataDict);
    
    [self putWithUrl:urlString params:dataDict andProcesseWithReponseBlock:^(BOOL isSuccess, id resultData) {
        if (isSuccess) {
            //parse the result data
            NSError *error = nil;
            ASCJobPosting *job = [[ASCJobPosting alloc] initWithDictionary:resultData[@"candidateJob"] error:&error];
            if (!error) {
                job.questions = aJob.questions;
                [ASCJobPostingDetailsService sharedInstance].selectedJob = job;
                if (aServiceResponseBlock) {
                    aServiceResponseBlock(TRUE, job);
                    return;
                }
            }
        }
        if (aServiceResponseBlock) {
            aServiceResponseBlock(FALSE, nil);
        }
    }];
}

- (NSDictionary *)getLocalResponseDictForQuestion:(ASCJobPostingQuestion *)aQuest onJob:(ASCJobPosting *)aJob
{
    NSString *key = [NSString stringWithFormat:C8_QUESTION_RESPONSEDICT_KEY_FORMAT, aJob.jid, aQuest.qid];
    NSObject *data = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    if ([data isKindOfClass:[NSDictionary class]])
    {
        return (NSDictionary *)data;
    }
    return nil;
}

- (void)setLocalResponseDict:(NSDictionary *)aDict forQuestion:(ASCJobPostingQuestion *)aQuest onJob:(ASCJobPosting *)aJob
{
    NSString *key = [NSString stringWithFormat:C8_QUESTION_RESPONSEDICT_KEY_FORMAT, aJob.jid, aQuest.qid];
    [[NSUserDefaults standardUserDefaults] setObject:aDict forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)hasAttemptedLocallyQuestion:(ASCJobPostingQuestion *)aQuest onJob:(ASCJobPosting *)aJob
{
    NSDictionary *respDict = [self getLocalResponseDictForQuestion:aQuest onJob:aJob];
    ASCResponseMode respMode = ASCResponseModePending;
    if (respDict)
    {
        //yes there is local data, lets pick that
        respMode = [respDict[C8_RESPMODE_KEY] integerValue];
        NSString *respString = respDict[C8_RESPDATA_KEY];
        
        if (respMode == ASCResponseModeWrite || respMode == ASCResponseModeRecord)
        {
            if (!respString)
            {
                //nothing written or captured yet, so its in pending mode.
                respMode = ASCResponseModePending;
            }
        }
    }
    return respMode == ASCResponseModePending? NO : YES;
}

- (int)getUnSubmittedJobResponsesCount:(ASCJobPosting *)aJob
{
    int count = 0;
    for (ASCJobPostingQuestion *quest in aJob.questions)
    {
        if (![self hasAttemptedLocallyQuestion:quest onJob:aJob])
        {
            count++;
        }
    }
    return count;
}

- (void)mergeLocalResponsesIfAny:(ASCJobPosting *)aJob
{
    for (ASCJobPostingQuestion *quest in aJob.questions)
    {
        //if locally captured and overritten the
        NSDictionary *respDict = [self getLocalResponseDictForQuestion:quest onJob:aJob];
        ASCResponseMode respMode = ASCResponseModePending;
        if (!respDict)
        {
            continue;
        }
        //if there are no answers yet, create one
        ASCJobPostingResponse *answer = quest.answer;
        if (!answer)
        {
            answer = [ASCJobPostingResponse new];
            quest.answer = answer;
        }
        
        //yes there is local data, lets pick that
        respMode = [respDict[C8_RESPMODE_KEY] integerValue];
        NSString *respString = respDict[C8_RESPDATA_KEY];
        
        switch (respMode)
        {
            case ASCResponseModeRecord:
                answer.status = @(respMode);
                answer.answerType =  @(respMode);
                answer.videoLocalUrl = respString; //local path to movie
                break;
                
            case ASCResponseModeWrite:
                answer.status = @(respMode);
                answer.answerType =  @(respMode);
                answer.writtenText = (respString)? respString : @"";
                break;
                
            case ASCResponseModeSkip:
                answer.status = @(ASCResponseModeSkip);
                break;
                
            default:
                answer.status = @(ASCResponseModePending);
                break;
        }
    }
}

- (NSString *)getVideoKeyForQuestion:(ASCJobPostingQuestion *)aQuest onJob:(ASCJobPosting *)aJob
{
    //@"emp/e%@/job/j%@/can/c%@/c8_j%@_q%@_vdo_ans.mp4"
    NSString *cid = [ASCLoginService sharedInstance].loggedInCandidate.cid;
    NSString *key = [NSString stringWithFormat:C8_QUESTION_VIDEO_ANSWERED_KEY_WITHFOLDERS,
                     aJob.employerId, aJob.jobId, cid, aJob.jid, aQuest.qid];
    DLog(@"video key=%@",key);
    return key;
}

- (void)uploadPendingResponsesOfJob:(ASCJobPosting *)aJob withResponseBlock:(ASCServiceResponseBlock)aResponseBlock
{
    __block int submittedQuestionsCount = 0;
    
    if (!self.submissionQueue)
    {
        self.submissionQueue = [[NSOperationQueue alloc] init];
        self.submissionQueue.name = @"C8 Job Submission Queue";
        self.submissionQueue.maxConcurrentOperationCount = 1;
    }
    
    for (ASCJobPostingQuestion *quest in aJob.questions)
    {
        //check and submit any captured jobs.
        //TODO: what if there is any local responses for this question?
        if (quest.answer.isCaptured)
        {
            submittedQuestionsCount++;
//            aResponseBlock(TRUE,@(submittedQuestionsCount)); //send the current submission count back
            continue;
        }
        //make sure the opted out flag is set right
        ASCResponseSubmissionOperation *opr = [[ASCResponseSubmissionOperation alloc] initWithJob:aJob andQuestion:quest withResponseBlock:^(BOOL isSuccess, id resultData) {
            submittedQuestionsCount++;
            if (isSuccess)
            {
                if (submittedQuestionsCount == aJob.questions.count)
                {
                    //all jobs questions uploaded and updated
                    //time to update the job
                    submittedQuestionsCount++; //TODO: is it okay? just to not fire another submission of the job
                    [self submitJob:aJob withResponseHandler:^(BOOL isJobSubmissionSuccess, id resultData) {
                        if (isJobSubmissionSuccess)
                        {
                            aResponseBlock(TRUE,@(aJob.questions.count)); //send the total question count back
                        }
                    }];
                }
                else
                {
                    aResponseBlock(TRUE,@(submittedQuestionsCount)); //send the current submission count back
                }
            }
        }];
        [self.submissionQueue addOperation:opr];
    }
    
    //here if all questions are already captured before
    if (submittedQuestionsCount == aJob.questions.count)
    {
        //all jobs questions uploaded and updated
        //time to update the job
        [self submitJob:aJob withResponseHandler:^(BOOL isJobSubmissionSuccess, id resultData) {
            if (isJobSubmissionSuccess)
            {
                aResponseBlock(TRUE,@(submittedQuestionsCount)); //send the current submission count back
            }
            else
            {
                aResponseBlock(FALSE,resultData);
            }
        }];
    }
}

- (NSString *)localPathToMovieForQuestion:(ASCJobPostingQuestion *)aQuest onJob:(ASCJobPosting *)aJob
{
    NSString *qansmovieName =  [[self getVideoKeyForQuestion:aQuest onJob:aJob] stringByReplacingOccurrencesOfString:@"/" withString:@"-"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *tempPath = [documentsDirectory stringByAppendingFormat:@"/%@", qansmovieName];
    return tempPath;
}

@end
