//
//  ASCVideoResponseSubmissionService.swift
//  candid8
//
//  Created by Aldrin ML on 21/09/15.
//  Copyright © 2015 AppSails. All rights reserved.
//

import Foundation
import AFNetworking

class ASCVideoResponseSubmissionService: ASCBaseService {
    
    class var shared : ASCVideoResponseSubmissionService {
        struct Singleton {
            static let instance = ASCVideoResponseSubmissionService()
        }
        return Singleton.instance
    }
    
    func submitVideoResponse(token: String, job: ASCJobPosting, question: ASCJobPostingQuestion, completion:ASCServiceResponseBlock) {
        
        if question.isAttempted() {
            self.reSubmitVideoResponse(token, job: job, question: question, completion: completion)
        } else {
            
            let s3VideoItemKey =  ASCJobSubmissionService.sharedInstance().getVideoKeyForQuestion(question, onJob: job)
            let s3itemImgUrl = s3VideoItemKey.stringByReplacingOccurrencesOfString("mp4", withString: "jpg")
            
            let urlString = "\(self.getDynamicBaseUrlPrefix())/answers"
            let dataDict = ["answer":["candidate_job_id":job.jid, "question_id":question.qid,
                "answer_type":"0",
                "video_url":s3VideoItemKey, "screen_shot_file_name":s3itemImgUrl],
                "access_token" : token] as [String:AnyObject]
            
            ASCPostActionService.sharedHTTPClient().action(urlString, post: dataDict,
                success: { (requestOp:AFHTTPRequestOperation!, responseData:[NSObject : AnyObject]!) -> Void in
                    do {
                        if let answerDict = responseData["answer"] as? [NSString:AnyObject] {
                            let ans = try ASCJobPostingResponse(dictionary: answerDict)
                            completion(true,ans)
                        }
                        else
                        {
                            completion(false,nil)
                        }
                    } catch let err {
                        completion(false,err as? AnyObject)
                    }
                },
                failure: { (requestOp:AFHTTPRequestOperation!, err:NSError!) -> Void in
                    completion(false,err)
            })
        }
    }
    
    func reSubmitVideoResponse(token: String, job : ASCJobPosting, question: ASCJobPostingQuestion, completion:ASCServiceResponseBlock) {
        let s3VideoItemKey =  ASCJobSubmissionService.sharedInstance().getVideoKeyForQuestion(question, onJob: job)
        let s3itemImgUrl = s3VideoItemKey.stringByReplacingOccurrencesOfString("mp4", withString: "jpg")
        
        let urlString = "\(self.getDynamicBaseUrlPrefix())/answers"
        let dataDict = ["answer":["candidate_job_id":job.jid, "question_id":question.qid,
            "answer_type":"0",
            "video_url":s3VideoItemKey, "screen_shot_file_name":s3itemImgUrl],
            "access_token" : token] as [String:AnyObject]
        
        self.putWithUrl(urlString, params: dataDict) { (status:Bool, responseData:AnyObject!) -> Void in
            if !status {
                completion(false, nil)
                return
            }
            do {
                if let answerDict = responseData["answer"] as? [NSString:AnyObject] {
                    let ans = try ASCJobPostingResponse(dictionary: answerDict)
                    completion(true,ans)
                }
                else
                {
                    completion(false,nil)
                }
            } catch let err {
                completion(false,err as? AnyObject)
            }
        }
    }
}
