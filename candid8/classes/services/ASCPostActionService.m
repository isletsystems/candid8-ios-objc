//
//  ASCPostActionService.m
//  candid8
//
//  Created by Aldrin Lenny on 18/10/12.
//
//

#import "ASCPostActionService.h"
#import <Objection/Objection.h>

@implementation ASCPostActionService

objection_register_singleton(ASCPostActionService)

+(ASCPostActionService *) sharedHTTPClient
{
    return [[JSObjection defaultInjector] getObject:[ASCPostActionService class]];
}

#pragma mark - Custom requests

- (void) post:(NSDictionary*)parameters
      success:(void (^)(AFHTTPRequestOperation *request, NSDictionary *objects))success
      failure:(void (^)(AFHTTPRequestOperation *request, NSError *error))failure
{
    
//    NSString *appURL = [NSString stringWithFormat:@"%@/cfgmod", kPushServicePrefix];
//    
//    [self action:appURL post:parameters success:success failure:failure];
}

- (void) action:(NSString *)aURLAction post:(NSDictionary*)parameters
        success:(void (^)(AFHTTPRequestOperation *request, NSDictionary *objects))success
        failure:(void (^)(AFHTTPRequestOperation *request, NSError *error))failure {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer new];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];

    
    [manager POST:aURLAction parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"JSON: %@", responseObject);
        if(success) {
            success(nil,responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"Error: %@", error);
        if (![self isSessionTimedOut:operation] && failure)
        {
            failure(FALSE, error);
        }
    }];
}

- (void) action:(NSString *)aURLAction post:(NSDictionary*)parameters
      onManager:(AFHTTPRequestOperationManager *)manager
        success:(void (^)(AFHTTPRequestOperation *request, NSDictionary *objects))success
        failure:(void (^)(AFHTTPRequestOperation *request, NSError *error))failure {
    
    [manager POST:aURLAction parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
    }  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"JSON: %@", responseObject);
        if(success) {
            success(operation,responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"Error: %@", error);
        if (![self isSessionTimedOut:operation] && failure)
        {
            failure(FALSE, error);
        }
    }];
}

@end