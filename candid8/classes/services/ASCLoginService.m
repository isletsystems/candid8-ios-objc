//
//  ASCLoginService.m
//  candid8
//
//  Created by Aldrin Lenny on 13/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCLoginService.h"

#import "ASCPostActionService.h"

#import <Objection/Objection.h>


#define kASCAT @"d1e75c5a36910de9640dec8183c9c371c4f17d9f763bba6d44ba99a57678a395"

@implementation ASCLoginService

objection_register_singleton(ASCLoginService)

+ (instancetype)sharedInstance
{
    return [[JSObjection defaultInjector] getObject:[ASCLoginService class]];
}

- (void)loginWithUserName:(NSString *)aUserName password:(NSString *)aPassword andResponseBlock:(ASCServiceResponseBlock)aServiceResponseBlock
{
    NSMutableDictionary *dataDict = [@{
                                       @"grant_type":@"password",
                                       @"username" : aUserName,
                                       @"password": aPassword
                                       } mutableCopy];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer new];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:kASCAT forHTTPHeaderField:@"HTTP_AUTHENTICATION"];
    
    
    [[ASCPostActionService sharedHTTPClient]  action:[NSString stringWithFormat:@"%@/login", [self getDynamicBaseUrlPrefix]] post:dataDict onManager:manager
                                             success:^(AFHTTPRequestOperation *request, NSDictionary *aResponseObject) {
                                                 
                                                 //extract access_token and stuff and store in keychain
                                                 NSString *access_token = aResponseObject[@"access_token"];
                                                 NSString *expires_in = aResponseObject[@"expires_in"];
                                                 NSString *refresh_token = aResponseObject[@"refresh_token"];
                                                 NSString *token_type = aResponseObject[@"token_type"];
                                                 
                                                 NSError *error = nil;
                                                 ASCCandindate *c8 = [[ASCCandindate alloc] initWithDictionary:aResponseObject[@"user"] error:&error];
                                                 self.loggedInCandidate = c8;
                                                 self.tokenInfo = [[ASCTokenInfo alloc] initWithAccessToken:access_token refreshToken:refresh_token tokenType:token_type];
                                                 self.tokenInfo.expiresInSeconds = [expires_in intValue];
                                                 self.tokenInfo.lastTockenUpdatedDateTime = [NSDate date];
                                                 aServiceResponseBlock(TRUE, c8);
                                             }
                                             failure:^(AFHTTPRequestOperation *request, NSError *error){
                                                 self.loggedInCandidate = nil;
                                                 aServiceResponseBlock(FALSE, error);
                                             }];
}

-(id)parseRseponse:(id)aResponseObject {
    
    //convert JSON to objects
    BOOL success = [aResponseObject[@"success"] boolValue];
    if (!success) {
        return nil;
    }
    NSError *error = nil;
    ASCCandindate *c8 = [[ASCCandindate alloc] initWithDictionary:aResponseObject[@"candidate"] error:&error];
    self.loggedInCandidate = c8;
    return c8;
}

- (void)refreshTockenAndResponseBlock:(ASCServiceResponseBlock)aServiceResponseBlock{

    
    NSMutableDictionary *dataDict = [@{
                                       @"grant_type":@"refresh_token",
                                       @"refresh_token" : self.tokenInfo.refreshToken,
                                       } mutableCopy];
    self.tokenInfo = nil;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer new];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:kASCAT forHTTPHeaderField:@"HTTP_AUTHENTICATION"];
    
    
    [[ASCPostActionService sharedHTTPClient]  action:[NSString stringWithFormat:@"%@/login", [self getDynamicBaseUrlPrefix]] post:dataDict onManager:manager
                                             success:^(AFHTTPRequestOperation *request, NSDictionary *aResponseObject) {
                                                 
                                                 //extract access_token and stuff and store in keychain
                                                 NSString *access_token = aResponseObject[@"access_token"];
                                                 NSString *expires_in = aResponseObject[@"expires_in"];
                                                 NSString *refresh_token = aResponseObject[@"refresh_token"];
                                                 NSString *token_type = aResponseObject[@"token_type"];
                                                 
                                                 NSError *error = nil;
                                                 ASCCandindate *c8 = [[ASCCandindate alloc] initWithDictionary:aResponseObject[@"user"] error:&error];
                                                 self.loggedInCandidate = c8;
                                                 self.tokenInfo = [[ASCTokenInfo alloc] initWithAccessToken:access_token refreshToken:refresh_token tokenType:token_type];
                                                 self.tokenInfo.expiresInSeconds = [expires_in intValue];
                                                 self.tokenInfo.lastTockenUpdatedDateTime = [NSDate date];
                                             }
                                             failure:^(AFHTTPRequestOperation *request, NSError *error){
                                                 self.loggedInCandidate = nil;
                                             }];


}
- (void)executeAfterGettingTocken:(ASCServiceAfterRefreshResponseBlock)aResponseBlock{
    NSTimeInterval interVel = self.tokenInfo.lastTockenUpdatedDateTime.timeIntervalSinceNow;
    if (interVel > self.tokenInfo.expiresInSeconds) {
        [self refreshTockenAndResponseBlock:^(BOOL isSuccess, id resultData) {
            aResponseBlock(self.tokenInfo.accessToken);
        }];
        
    }else{
        aResponseBlock(self.tokenInfo.accessToken);
    }

}
@end
