//
//  ASCJobSubmissionService.h
//  candid8
//
//  Created by Aldrin Lenny on 08/05/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCBaseService.h"

#import "ASCJobPosting.h"
#import "ASCJobPostingQuestion.h"


@interface ASCJobSubmissionService : ASCBaseService

+ (ASCJobSubmissionService *)sharedInstance;

- (void)submitQuestion:(ASCJobPostingQuestion *)aQuest onJob:(ASCJobPosting *)aJob withResponseHandler:(ASCServiceResponseBlock)aServiceResponseBlock;

- (void)submitJob:(ASCJobPosting *)aJob withResponseHandler:(ASCServiceResponseBlock)aServiceResponseBlock;

- (BOOL)hasAttemptedLocallyQuestion:(ASCJobPostingQuestion *)aQuest onJob:(ASCJobPosting *)aJob;

- (NSString *)getVideoKeyForQuestion:(ASCJobPostingQuestion *)aQuest onJob:(ASCJobPosting *)aJob;

- (void)uploadPendingResponsesOfJob:(ASCJobPosting *)aJob withResponseBlock:(ASCServiceResponseBlock)aResponseBlock;

- (NSString *)localPathToMovieForQuestion:(ASCJobPostingQuestion *)aQuest onJob:(ASCJobPosting *)aJob;

- (NSDictionary *)getLocalResponseDictForQuestion:(ASCJobPostingQuestion *)aQuest onJob:(ASCJobPosting *)aJob;
- (void)setLocalResponseDict:(NSDictionary *)aDict forQuestion:(ASCJobPostingQuestion *)aQuest onJob:(ASCJobPosting *)aJob;

- (void)mergeLocalResponsesIfAny:(ASCJobPosting *)aJob;

@end
