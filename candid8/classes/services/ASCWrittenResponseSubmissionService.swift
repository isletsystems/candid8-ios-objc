//
//  ASCWrittenResponseSubmissionService.swift
//  candid8
//
//  Created by Aldrin ML on 21/09/15.
//  Copyright © 2015 AppSails. All rights reserved.
//

import Foundation

import AFNetworking

class ASCWrittenResponseSubmissionService: ASCBaseService {

    class var shared : ASCWrittenResponseSubmissionService {
        struct Singleton {
            static let instance = ASCWrittenResponseSubmissionService()
        }
        return Singleton.instance
    }
    
    func submitWrittenResponse(text : String, token: String, job : ASCJobPosting, question: ASCJobPostingQuestion, completion:ASCServiceResponseBlock) {
        
        if question.isAttempted() {
            self.reSubmitWrittenResponse(text, token: token, job: job, question: question, completion: completion)
        } else {

        let urlString = "\(self.getDynamicBaseUrlPrefix())/answers"
        let dataDict = ["answer":["candidate_job_id":job.jid,"notes":text, "question_id":question.qid, "answer_type":"1"],
            "access_token" : token] as [String:AnyObject]
        
        ASCPostActionService.sharedHTTPClient().action(urlString, post: dataDict,
            success: { (requestOp:AFHTTPRequestOperation!, responseData:[NSObject : AnyObject]!) -> Void in
                do {
                    if let answerDict = responseData["answer"] as? [NSString:AnyObject] {
                        let ans = try ASCJobPostingResponse(dictionary: answerDict)
                        completion(true,ans)
                    }
                    else
                    {
                        completion(false,nil)
                    }
                } catch let err {
                    completion(false,err as? AnyObject)
                }
            },
            failure: { (requestOp:AFHTTPRequestOperation!, err:NSError!) -> Void in
                completion(false,err)
            })
        }
    }
    
    func reSubmitWrittenResponse(text : String, token: String, job: ASCJobPosting, question: ASCJobPostingQuestion, completion:ASCServiceResponseBlock) {
        
        let urlString = "\(self.getDynamicBaseUrlPrefix())/answers/\(question.answer.aid)"
        let dataDict = ["answer":["candidate_job_id":job.jid,"notes":text, "question_id":question.qid, "answer_type":"1"],
            "access_token" : token] as [String:AnyObject]
        
        self.putWithUrl(urlString, params: dataDict) { (status:Bool, responseData:AnyObject!) -> Void in
            if !status {
                completion(false, nil)
                return
            }
            do {
                if let answerDict = responseData["answer"] as? [NSString:AnyObject] {
                    let ans = try ASCJobPostingResponse(dictionary: answerDict)
                    completion(true,ans)
                }
                else
                {
                    completion(false,nil)
                }
            } catch let err {
                completion(false,err as? AnyObject)
            }
        }
    }
}
