//
//  ACSGenericWebViewController.m
//  candid8
//
//  Created by islet on 12/08/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "ACSGenericWebViewController.h"

#import <MBProgressHUD/MBProgressHUD.h>

@interface ACSGenericWebViewController()<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *theWebView;

@end

@implementation ACSGenericWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.theWebView.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_theWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
}

- (IBAction)btnDismissTouched:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (IBAction)btnShowSidePanel:(UIButton *)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCShowSideMenuNotification" object:nil];
}

#pragma mark - webview delegates

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
}

@end
