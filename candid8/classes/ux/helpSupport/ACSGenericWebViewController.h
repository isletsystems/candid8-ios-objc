//
//  ACSGenericWebViewController.h
//  candid8
//
//  Created by islet on 12/08/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "IBaseViewController.h"

@interface ACSGenericWebViewController : IBaseViewController

@property (nonatomic, strong) NSString *urlString;

@end
