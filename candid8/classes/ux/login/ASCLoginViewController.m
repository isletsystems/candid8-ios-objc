//
//  ASCLoginViewController.m
//  candid8
//
//  Created by Aldrin Lenny on 13/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCLoginViewController.h"

#import "AppConfigs.h"
#import "ASCLoginService.h"
#import "ASCJobListingViewController.h"

#import <ColorUtils/ColorUtils.h>
#import <MBProgressHUD/MBProgressHUD.h>

NSString static *kC8UserPasswordKey = @"ASC_USER_PASS";
NSString static *kC8UserEmailIdKey = @"ASC_USER_EMAIL";


@interface ASCLoginViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailIdTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (weak, nonatomic) IBOutlet UILabel *erroMessageLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;
@property (weak, nonatomic) IBOutlet UITextField *activeField;
@property (strong, nonatomic)  ASCCandindate *loggedInCandidate;
@property (weak, nonatomic) IBOutlet UILabel *rememberMeLabel;
@property (weak, nonatomic) IBOutlet UISwitch *rememberMeSwitch;
@property (weak, nonatomic) IBOutlet UIView *emailBGView;
@property (weak, nonatomic) IBOutlet UIView *pwdBGView;


- (IBAction)rememberAction:(id)sender;

- (IBAction)loginAction:(id)sender;
- (IBAction)returnToLogin:(UIStoryboardSegue *)segue;

@end

@implementation ASCLoginViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.rememberMeSwitch setOnTintColor:[UIColor colorWithString:kC8ThemeColor]];
    self.loginButton.backgroundColor = [UIColor colorWithString:kC8ThemeButtonColor];
    [self.loginButton setTitleColor:[UIColor colorWithString:kC8ThemeButtonTextColor] forState:UIControlStateNormal];
    self.loginButton.layer.cornerRadius = 5;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.emailBGView.layer.cornerRadius = 5;
    self.emailBGView.layer.masksToBounds = YES;
    self.pwdBGView.layer.cornerRadius = 5;
    self.pwdBGView.layer.masksToBounds = YES;
}

- (void)hideKeyboard
{
    [self.emailIdTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self performSelector:@selector(displayContent) withObject:self afterDelay:1.0];
}

- (void)displayContent
{
    [UIView setAnimationsEnabled:NO];
    self.erroMessageLabel.text = @"";
    self.emailIdTextField.placeholder = NSLocalizedString(@"login.EMAIL_PLACEHOLDER", nil);
    self.passwordTextField.placeholder = NSLocalizedString(@"login.pwd.PLACEHOLDER", nil);
    
    [self.loginButton setTitle:NSLocalizedString(@"login.LOGIN_BTN_TITLE", nil) forState:UIControlStateNormal];
    [self.forgotPasswordButton setTitle:NSLocalizedString(@"login.FORGOT_PASSWORD_BTN_LABEL", nil) forState:UIControlStateNormal];
    [self registerForKeyboardNotifications];
    
    self.rememberMeLabel.text = NSLocalizedString(@"login.EMAIL_REMEMBER_LABEL", nil);
    
#if defined(DEBUG_CONFIG)
    self.emailIdTextField.text = kTestUserName;
    self.passwordTextField.text = kTestUserPass;
#endif
    
    
    NSString *rememberdEmail = [[NSUserDefaults standardUserDefaults] objectForKey:kC8UserEmailIdKey];
    if (rememberdEmail)
    {
        self.emailIdTextField.text = rememberdEmail;
        self.passwordTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:kC8UserPasswordKey];
        self.rememberMeSwitch.on = YES;
    }
    [UIView setAnimationsEnabled:YES];

}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unregisterForKeyboardNotifications];
}

- (IBAction)rememberAction:(id)sender
{
    
}

- (IBAction)loginAction:(id)sender
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    self.erroMessageLabel.text = @"";
    [self hideKeyboard];
    
    NSString *emailId = [self.emailIdTextField text];
    NSString *password = [self.passwordTextField text];
    
    
    //validate the fields if they are not empty
    if ([emailId isEqualToString:@""] || [password isEqualToString:@""])
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        alert.showCircularIcon = false;
        alert.contentViewCornerRadius = 0.0;
        alert.buttonCornerRadius = 5.0;
        [alert showNotice:NSLocalizedString(@"login.ALERT_TITLE", nil)
                 subTitle:NSLocalizedString(@"login.EMAIL_NOT_EMPTY", nil)
         closeButtonTitle:NSLocalizedString(@"c8.TITLE_OK", @"cancel") duration:0.0f colorStyle:kC8ButtonBgColorInt colorTextButton:0xf5f5f5];
        return;
    }
    if (self.rememberMeSwitch.on)
    {
        [[NSUserDefaults standardUserDefaults] setObject:emailId forKey:kC8UserEmailIdKey];
        [[NSUserDefaults standardUserDefaults] setObject:password forKey:kC8UserPasswordKey];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kC8UserEmailIdKey];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kC8UserPasswordKey];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //validate if the email id is valid
    
    //invoke the service call to login
    __weak __typeof__(self) weakSelf = self;
    //if success segue to main view, else stay here with an error
    [[ASCLoginService sharedInstance] loginWithUserName:emailId password:password andResponseBlock:^(BOOL aStatus, id resultData) {
        
        __strong __typeof__(self) strongSelf = weakSelf;
        [MBProgressHUD hideAllHUDsForView:strongSelf.view animated:NO];
        if (aStatus)
        {
            strongSelf.loggedInCandidate = resultData;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCLoginSuccessNotification" object:nil];
        }
        else
        {
            NSString *errMsg = NSLocalizedString(@"login.EMAIL_NOT_VALID", nil);
            if ([resultData isKindOfClass:[NSError class]])
            {
                NSError *err = resultData;
                if ([err.userInfo[@"NSLocalizedDescription"] isEqualToString:@"The Internet connection appears to be offline."])
                {
                    errMsg = NSLocalizedString(@"c8.NO_INTERNET", nil);
                }
            }
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            alert.showCircularIcon = false;
            alert.contentViewCornerRadius = 0.0;
            alert.buttonCornerRadius = 5.0;
            [alert showNotice:NSLocalizedString(@"login.ALERT_TITLE", nil)
                     subTitle:errMsg
             closeButtonTitle:NSLocalizedString(@"c8.TITLE_OK", @"cancel") duration:0.0f colorStyle:kC8ButtonBgColorInt colorTextButton:0xf5f5f5];
        }
    }];
}

- (IBAction)returnToLogin:(UIStoryboardSegue *)segue
{
    self.erroMessageLabel.text = @"";
    self.emailIdTextField.text = @"";
    self.passwordTextField.text = @"";
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeField = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self loginAction:nil];
    return NO;
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.contentScrollView.contentInset = contentInsets;
    self.contentScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.activeField.frame.origin))
    {
        [self.contentScrollView scrollRectToVisible:self.activeField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.contentScrollView.contentInset = contentInsets;
    self.contentScrollView.scrollIndicatorInsets = contentInsets;
}

@end
