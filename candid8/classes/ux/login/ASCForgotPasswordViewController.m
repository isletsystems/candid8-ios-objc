//
//  ASCForgotPasswordViewController.m
//  candid8
//
//  Created by Aldrin Lenny on 16/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCForgotPasswordViewController.h"

#import "ASCForgotPasswordService.h"
#import <ColorUtils/ColorUtils.h>
#import <Toast/UIView+Toast.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface ASCForgotPasswordViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailIdTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (weak, nonatomic) IBOutlet UILabel *erroMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *resetPasswordLabel;

@property (weak, nonatomic) IBOutlet UIView *emailBGView;

- (IBAction)retrievePasswordAction:(id)sender;
- (IBAction)backToLoginAction:(id)sender;

@end

@implementation ASCForgotPasswordViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.forgotPasswordButton.layer.cornerRadius = 5;
    self.forgotPasswordButton.backgroundColor = [UIColor colorWithString:kC8ThemeButtonColor];
    [self.forgotPasswordButton setTitleColor:[UIColor colorWithString:kC8ThemeButtonTextColor] forState:UIControlStateNormal];
    self.emailBGView.layer.cornerRadius = 5;
    self.emailBGView.layer.masksToBounds = YES;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
    self.resetPasswordLabel.text =  NSLocalizedString(@"forgotpw.RESET_TITLE", nil);
    
    self.emailIdTextField.placeholder = NSLocalizedString(@"login.EMAIL_PLACEHOLDER", nil);
    [self.loginButton setTitle:NSLocalizedString(@"forgotpw.BACKTO_LOGIN_BTN_TITLE", nil) forState:UIControlStateNormal];
    [self.forgotPasswordButton setTitle:NSLocalizedString(@"forgotpw.LOGIN_BTN_TITLE", nil) forState:UIControlStateNormal];
}

- (void)hideKeyboard
{
    [self.emailIdTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.erroMessageLabel.text = @"";
    
}

- (IBAction)retrievePasswordAction:(id)sender
{
    self.erroMessageLabel.text = @"";
    [self.emailIdTextField resignFirstResponder];
    NSString *emailId = [self.emailIdTextField text];
    
    //validate the fields if they are not empty
    if ([emailId isEqualToString:@""])
    {
        self.erroMessageLabel.text = NSLocalizedString(@"forgotpw.EMAIL_NOT_EMPTY", nil);
        return;
    }
    
    //validate the email id
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    //invoke the service call to login
    //if success segue to main view, else stay here with an error
    __weak __typeof__(self) weakSelf = self;
    [[ASCForgotPasswordService sharedInstance] resetPasswordWithEmailId:emailId andResponseBlock:^(BOOL aStatus, id resultData) {
        __strong __typeof__(self) strongSelf = weakSelf;
        
        
        [MBProgressHUD hideAllHUDsForView:strongSelf.view animated:NO];
        if (aStatus) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf displayThanksToast];
            });
        } else {
            strongSelf.erroMessageLabel.text = NSLocalizedString(@"forgotpw.EMAIL_NOT_VALID", nil);
        }
    }];
}

- (IBAction)backToLoginAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)displayThanksToast
{
    [self.view makeToast:NSLocalizedString(@"forgotpw.EMAIL_CHECK", nil)];
    [self performSelector:@selector(backToLoginAction:) withObject:self afterDelay:3.0];
}

@end
