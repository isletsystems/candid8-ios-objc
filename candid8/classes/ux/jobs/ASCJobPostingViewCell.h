//
//  ASCJobPostingViewCell.h
//  candid8
//
//  Created by Aldrin Lenny on 16/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//
#import "ASCJobPostingView.h"
@class ASCJobPostingViewCell;
typedef void (^ASCJobPostingActionInCell)(ASCJobSelectedOption acn,ASCJobPostingViewCell * cell);

@interface ASCJobPostingViewCell : UITableViewCell<IBaseConfigurableView>
@property (nonatomic,copy) ASCJobPostingActionInCell handler;
@end
