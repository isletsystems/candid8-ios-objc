//
//  ASCJobDetailViewController.m
//  candid8
//
//  Created by Aldrin Lenny on 16/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobDetailViewController.h"

#import "ASCLoginService.h"
#import "ASCJobPostingDetailsService.h"
#import "ASCJobQuestionViewCell.h"
#import "ASCPostActionService.h"
#import "ASCJobSubmissionService.h"
#import "ASCJobPostingResponse.h"
#import "ASCJobPostingView.h"

#import "AppConfigs.h"
#import "ColorUtils.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "candid8-Swift.h"

//@import SLCAlertView;

#import "SCLAlertView-Swift.h"

NSString *TOT_PREVIEW_ANSWER_SID = @"previewAnswerSegue";

@interface ASCJobDetailViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UIActionSheetDelegate>

@property (nonatomic, weak) IBOutlet UITableView *questionsTableView;
@property (nonatomic, strong) ASCJobPosting *jobPosting;
@property (weak, nonatomic) IBOutlet UIView *jobViewContainer;
@property (strong, nonatomic) ASCJobPostingView *jobView;

@property (weak, nonatomic) IBOutlet UILabel *questionsLabel;

@property (nonatomic, readwrite) int submissionProgressCount;

@property (weak, nonatomic) IBOutlet UIButton *submitJobButton;
@property (weak, nonatomic) IBOutlet UILabel *responseSummaryLabel;
@property (nonatomic, strong) UIActionSheet *responseActionSheet;
@property (nonatomic, strong) UIActionSheet *skipWarnActionSheet;

@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (weak, nonatomic) IBOutlet UIView *seperatorLineView;
@property (weak, nonatomic) IBOutlet UIButton *btnTerms;
@property (weak, nonatomic) IBOutlet UIButton *btnPrivacy;


- (IBAction)goBack:(id)sender;
- (IBAction)doSubmitApplication:(id)sender;

@end

@implementation ASCJobDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.titleView.backgroundColor = [UIColor colorWithString:kC8ThemeColor];
    self.submitJobButton.backgroundColor = [UIColor colorWithString:kC8ThemeColor];
    self.btnTerms.tintColor = [UIColor colorWithString:kC8ThemeColor];
    self.btnPrivacy.tintColor = [UIColor colorWithString:kC8ThemeColor];
    self.seperatorLineView.backgroundColor = [UIColor colorWithString:kC8LightBlueColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.submissionProgressCount = 0;
    
    [self displayJobHeader];
    self.jobPosting = [ASCJobPostingDetailsService sharedInstance].selectedJob;
    [self displayInfo];
}

- (void)displayJobHeader
{
    if (!self.jobView)
    {
        self.jobView = [[ASCJobPostingView alloc] initWithFrame:CGRectInset(self.jobViewContainer.frame, 1, 1) andDetailFlag:YES];
        [self.jobViewContainer addSubview:self.jobView];
    }
    [self.jobView configureWithDict:@{@"JOB":[ASCJobPostingDetailsService sharedInstance].selectedJob}];
}

- (void) displayInfo
{
    //this is the original selected job which has the employer info
    self.responseSummaryLabel.text = [self responseSummaryText];
    
    self.questionsLabel.text = NSLocalizedString(@"job.QUESTIONS", nil);
    self.submitJobButton.enabled = FALSE;
    self.submitJobButton.backgroundColor = [UIColor colorWithString:kC8DisableSubmitColor];
    
    if (![self.jobPosting isSubmitted])
    {
        //check if the job expired
        if (![self.jobPosting isExpired])
        {
           if ([self isReadyToSubmit])
           {
               self.submitJobButton.enabled = TRUE;
               self.submitJobButton.backgroundColor = [UIColor colorWithString:kC8ThemeColor];
           }
            //lets do merge the local data with the cloud data
            [[ASCJobSubmissionService sharedInstance] mergeLocalResponsesIfAny:self.jobPosting];
        }
    }
    [self.questionsTableView reloadData];
}

#pragma mark - UITableView Delegate/Datasource methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.jobPosting.questions count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ASCJobQuestionViewCell *cell = (ASCJobQuestionViewCell *)[tableView dequeueReusableCellWithIdentifier:@"jobQuestionCell" forIndexPath:indexPath];
    [cell configureWithDict:@{@"QUEST":self.jobPosting.questions[indexPath.row], @"IDX":@(indexPath.row+1)}];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [ASCJobPostingDetailsService sharedInstance].selectedQuestionIndex = indexPath.row;
    [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion = self.jobPosting.questions[indexPath.row];
    //take the user to response scene
    [self performSegueWithIdentifier:@"responseSegue" sender:self];
}

- (IBAction)goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - submission stuff
- (IBAction)doSubmitApplication:(id)sender
{
    //if all jobs questions are responded to or skipped already
    if (![self isReadyToSubmit])
    {
        //check to see if all jobs are responded to by capturing answers either locally
        for (ASCJobPostingQuestion *quest in self.jobPosting.questions)
        {
            if (![[ASCJobSubmissionService sharedInstance] hasAttemptedLocallyQuestion:quest onJob:self.jobPosting])
            {
                [self showJobSubmissionWarningAlert];
                return;
            }
        }
    }
    //prompt user for confirmation now that the job is ready for submission.
    [self showJobSubmissionProceedAlert];
}


-(void)showJobSubmissionWarningAlert
{
    SCLFlatAlertView *alert = [[SCLFlatAlertView alloc] init];
    SCLButton *yesButton = [alert addButton:NSLocalizedString(@"c8.TITLE_OK", nil) action:^(void) {}];
    [alert showNotice:NSLocalizedString(@"job.submit.WARNING_TITLE", nil)
             subTitle:NSLocalizedString(@"job.submit.WARNING_MESSAGE", nil)
     closeButtonTitle:NSLocalizedString(@"c8.TITLE_CANCEL", @"cancel") duration:0.0f colorStyle:0xCFCFCF colorTextButton:0xFFFFFF];
    yesButton.backgroundColor = [UIColor colorWithString:kC8ThemeColor];

}

-(void)showJobSubmissionProceedAlert
{
    SCLFlatAlertView *alert = [[SCLFlatAlertView alloc] init];
    alert.showCloseButton = YES;
    SCLButton *yesButton = [alert addButton:NSLocalizedString(@"c8.TITLE_PROCEED", nil) action:^(void) {
        [self doJobSubmissionProcessing];
    }];
    [alert showNotice:NSLocalizedString(@"job.submit.CONFIRMATION_TITLE", nil)
             subTitle:NSLocalizedString(@"job.submit.CONFIRMATION_MESSAGE", nil)
     closeButtonTitle:NSLocalizedString(@"c8.TITLE_CANCEL", @"cancel") duration:0.0f colorStyle:0xCFCFCF colorTextButton:0xFFFFFF];
    yesButton.backgroundColor = [UIColor colorWithString:kC8ThemeColor];

}

- (void)doJobSubmissionProcessing
{
    //submit each question that is locally captured but not submitted
    //then submit the job
    //TODO: fix the service to use new APIs.
    self.responseSummaryLabel.text = @"";
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    __weak __typeof__(self) weakSelf = self;
    [[ASCJobSubmissionService sharedInstance] uploadPendingResponsesOfJob:self.jobPosting withResponseBlock:^(BOOL isSuccess, id resultData) {
        
        __strong __typeof__(self) strongSelf = weakSelf;
        if (isSuccess)
        {
            NSNumber *processingCount = resultData;
            if ([processingCount intValue] == strongSelf.jobPosting.questions.count)
            {
                //done last
                [strongSelf performSelectorOnMainThread:@selector(displayJobSubmissionSuccessAlert) withObject:strongSelf waitUntilDone:NO];
            }
            else
            {
                strongSelf.submissionProgressCount = [processingCount intValue];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [strongSelf displayJobSubmissionProgressInfo];
                });
            }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:strongSelf.view animated:YES];
                [self displayJobSubmissionFailureAlert];
            });
        }
    }];
}

-(void)displayJobSubmissionSuccessAlert
{
    //show thankyou for submission
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    SCLFlatAlertView *alert = [[SCLFlatAlertView alloc] init];
    SCLButton *yesButton = [alert addButton:NSLocalizedString(@"c8.TITLE_OK", nil) action:^(void) {
        self.jobPosting = [ASCJobPostingDetailsService sharedInstance].selectedJob;
        self.submitJobButton.enabled = FALSE;
        self.responseSummaryLabel.text = [self responseSummaryText];
        [self displayInfo];
    }];
    [alert showSuccess:NSLocalizedString(@"job.submit.SUCCESS_TITLE", nil)
             subTitle:NSLocalizedString(@"job.submit.SUCCESS_MESSAGE", nil)
      closeButtonTitle:NSLocalizedString(@"c8.TITLE_CANCEL", @"cancel") duration:0.0f colorStyle:0xCFCFCF colorTextButton:0xFFFFFF];
    yesButton.backgroundColor = [UIColor colorWithString:kC8ThemeColor];

}

-(void)displayJobSubmissionFailureAlert
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    //show failed for submission
    SCLFlatAlertView *alert = [[SCLFlatAlertView alloc] init];
    SCLButton *yesButton = [alert addButton:NSLocalizedString(@"c8.TITLE_OK", nil) action:^(void) {
        //TODO?
    }];
    [alert showError:NSLocalizedString(@"job.submit.FAILURE_TITLE", nil)
             subTitle:NSLocalizedString(@"job.submit.FAILURE_MESSAGE", nil)
    closeButtonTitle:NSLocalizedString(@"c8.TITLE_CANCEL", @"cancel") duration:0.0f colorStyle:0xCFCFCF colorTextButton:0xFFFFFF];
    yesButton.backgroundColor = [UIColor colorWithString:kC8ThemeColor];

}

-(void)displayJobSubmissionProgressInfo
{
    self.responseSummaryLabel.text = [NSString stringWithFormat:NSLocalizedString(@"job.submit.PROGRESS_MESSAGE", nil), self.submissionProgressCount, (unsigned long)self.jobPosting.questions.count];
}


- (BOOL)isReadyToSubmit
{
    //go through each question and see if any of the
    //check how many of the job questions are answered / skipped
    int attemptedCount = [self getTotalAttemptedCount];
    if (attemptedCount == self.jobPosting.questions.count)
    {
        return YES;
    }
    return NO;
}

- (NSString *)responseSummaryText
{
//    if ([self.jobPosting isSubmitted])
//    {
//        return NSLocalizedString(@"job.submit.ALREADY_SUBMITTED_MESSAGE", nil);
//    }
//    if ([self isReadyToSubmit])
//    {
//        return NSLocalizedString(@"job.submit.READY_MESSAGE", nil);
//    }
    return [NSString stringWithFormat:NSLocalizedString(@"job.submit.PENDING_QUESTIONS_MESSAGE", nil), [self getTotalAttemptedCount], self.jobPosting.questions.count];
}

- (int)getTotalAttemptedCount
{
    int attemptedCount = 0;
    for (ASCJobPostingQuestion *quest in self.jobPosting.questions)
    {
        if ([quest isAttempted])
        {
            attemptedCount++;
        }
        else
        {
            //check if its attempted locally already
            if ([[ASCJobSubmissionService sharedInstance] hasAttemptedLocallyQuestion:quest onJob:self.jobPosting])
            {
                attemptedCount++;
            }
        }
    }
    return attemptedCount;
}

@end
