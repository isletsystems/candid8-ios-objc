//
//  ASCJobListingViewController.m
//  candid8
//
//  Created by Aldrin Lenny on 16/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobListingViewController.h"

#import "ASCJobPostingViewCell.h"
#import "ASCJobFacade.h"
#import "ASCJobPostingDetailsService.h"
#import "ASCJobListingService.h"
#import "ASCLoginService.h"
#import "ASCJobDeleteService.h"

#import <ColorUtils/ColorUtils.h>
#import <ISRefreshControl/ISRefreshControl.h>

#import <SDWebImage/UIImageView+WebCache.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <SBPickerSelector/SBPickerSelector.h>
#import "SCLAlertView-Swift.h"

typedef enum : NSInteger
{
    ASCJobStatusNew = 0,
    ASCJobStatusSubmitted = 1
} ASCJobStatus;

static BOOL shownWelcomInThisSession = FALSE;

@interface ASCJobListingViewController ()<UITableViewDataSource, UITableViewDelegate, SBPickerSelectorDelegate>

@property (nonatomic, weak) IBOutlet UITableView *jobTableView;
@property (nonatomic, strong) NSArray *jobPostings;

@property (weak, nonatomic) IBOutlet UIView *segmentContainerView;
@property (nonatomic, strong) ISRefreshControl *refreshControl;

@property (weak, nonatomic) IBOutlet UIView *welcomeContainer;
@property (weak, nonatomic) IBOutlet UILabel *candidateWelcomeLabel;
@property (weak, nonatomic) IBOutlet UILabel *candidateLastLoginLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *candidateWelcomeViewHeightContstraint;
@property (weak, nonatomic) IBOutlet UIButton *draftJobsButton;
@property (weak, nonatomic) IBOutlet UILabel *draftJobsLabel;
@property (weak, nonatomic) IBOutlet UIButton *submittedJobsButton;
@property (weak, nonatomic) IBOutlet UILabel *submittedJobsLabel;


@property (weak, nonatomic) IBOutlet UILabel *selectedJobFilterNameLabel;
@property (nonatomic, assign)  NSInteger selectedJobFilterNameIndex;

@property (nonatomic, assign) NSInteger selectedSegment;

@property (weak, nonatomic) IBOutlet UIView *titleView;

@end

@implementation ASCJobListingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.titleView.backgroundColor = [UIColor colorWithString:kC8ThemeColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popToHome) name:@"kASCLoginSuccessNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showJobByNumberView) name:@"kASCShowJobApplyByNumberNotification" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(takeToProfile) name:@"kASCShowProfile" object:nil];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    //pull to refresh
    self.refreshControl = [ISRefreshControl new];
    [self.jobTableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self
                            action:@selector(refreshContent)
                  forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.selectedJobFilterNameIndex = 0;
    [self initHeader];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self refreshContent];
}

- (void)refreshContent {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *cid = [ASCLoginService sharedInstance].loggedInCandidate.cid;
    __weak __typeof__(self) weakSelf = self;
    [[ASCJobFacade sharedInstance] fetchJobsOfCandidate:cid withResponseBlock:^(BOOL isSuccess, id resultData) {
        __strong __typeof__(self) strongSelf = weakSelf;
        if (isSuccess)
        {
            [ASCLoginService sharedInstance].loggedInCandidate.jobs = resultData;
            [strongSelf filterByJobStatus:strongSelf.selectedSegment];
        }
        
        [MBProgressHUD hideAllHUDsForView:strongSelf.view animated:NO];
        [strongSelf.refreshControl endRefreshing];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - notification support from menu
- (void)popToHome
{
    NSArray *vcs = self.navigationController.viewControllers;
    for (UIViewController *ctrl in vcs)
    {
        if ([ctrl isKindOfClass:[ASCJobListingViewController class]])
        {
            [self.navigationController popToViewController:ctrl animated:YES];
            return;
        }
    }
}

- (void)popToHelpSuport{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"showHelpSupport" sender:self];
    });
    
}
- (void)showJobByNumberView
{
    [self popToHome];
    [self performSelector:@selector(takeToshowJobByNumberView) withObject:self afterDelay:.3];
    
}

- (void)takeToshowJobByNumberView
{
    [self performSegueWithIdentifier:@"showJobByNumberSID" sender:self];
}

- (void)takeToProfile{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"showProfile" sender:self];
    });

}

#pragma mark - segment header stuff
- (void) initHeader
{
    self.selectedJobFilterNameLabel.text = @"Newest";
    self.selectedJobFilterNameIndex = 0;
    
    NSString *firstTimeFlag = [[NSUserDefaults standardUserDefaults] stringForKey:@"C8_APP_START_FIRST_TIME"];
    self.selectedSegment = self.draftJobsButton.tag;
    
    self.candidateWelcomeViewHeightContstraint.constant = 100;
    self.welcomeContainer.alpha = 1.0;
    if (!firstTimeFlag) {
        self.candidateWelcomeLabel.text = [NSString stringWithFormat:@"Welcome %@", [ASCLoginService sharedInstance].loggedInCandidate.fullName];
        self.candidateLastLoginLabel.text = @"";
        
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"C8_APP_START_FIRST_TIME"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        if (!shownWelcomInThisSession)
        {
            shownWelcomInThisSession = TRUE;
            //hide it with an animation
            self.candidateWelcomeLabel.text = [NSString stringWithFormat:@"Welcome back %@", [ASCLoginService sharedInstance].loggedInCandidate.fullName];
            self.candidateLastLoginLabel.text = @"a while ago (TBD)";
            
            [UIView animateWithDuration:2.f
                                  delay:0.f
                                options:UIViewAnimationOptionCurveEaseIn
                             animations:^{
                                 [self.welcomeContainer setAlpha:0.0f];
                             }
                             completion:^(BOOL finished) {
                                 self.candidateWelcomeViewHeightContstraint.constant = 0;
                             }];
        } else {
            self.candidateWelcomeViewHeightContstraint.constant = 0;
        }
    }
}

- (IBAction)jobStatusTypeChanged:(id)sender
{
    NSInteger idx = ((UIButton *)sender).tag;
    [self filterByJobStatus:idx];
}
#pragma mark - show details
- (void)showDetailsForitemIndex:(NSInteger)index{
    ASCJobPosting *jobPosting = self.jobPostings[index];
    [ASCJobPostingDetailsService sharedInstance].selectedJob = jobPosting;
    
    //mark the job as viewed if not already viewed
    if (![jobPosting isViewed])
    {
//        [[ASCJobFacade sharedInstance] markViewedJob:jobPosting.jid ofCandidate:[ASCLoginService sharedInstance].loggedInCandidate.cid withResponseBlock:^(BOOL isSuccess, id resultData) {
//            if (isSuccess)
//            {
//                //cool.
//            }
//        }];
    }
    [self performSegueWithIdentifier:@"jobDetailSegue" sender:self];

}

- (void)removeJobAtIndex:(NSInteger)index
{
    ASCJobPosting *jobToDelete = [self.jobPostings objectAtIndex:index];
    __weak __typeof__(self) weakSelf = self;
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    alert.showCircularIcon = false;
    alert.contentViewCornerRadius = 0.0;
    alert.buttonCornerRadius = 5.0;
    
    //Using Block
    SCLButton *yesButton = [alert addButton:NSLocalizedString(@"c8.TITLE_YES", @"YES action") action:^(void) {
        __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
        [[ASCJobFacade sharedInstance] deleteJob:jobToDelete withResponseBlock:^(BOOL isSuccess, id resultData) {
            
            [MBProgressHUD hideAllHUDsForView:strongSelf.view animated:NO];
            if (isSuccess)
            {
                //update the whole list
                NSMutableArray<ASCJobPosting, Optional> *allJobsUpdated = [[ASCLoginService sharedInstance].loggedInCandidate.jobs mutableCopy];
                [allJobsUpdated removeObject:jobToDelete];
                [ASCLoginService sharedInstance].loggedInCandidate.jobs = allJobsUpdated;
                strongSelf.jobPostings = allJobsUpdated;
                
                DLog(@"job deleted: %@",jobToDelete.jid);
                [strongSelf filterByJobStatus:self.selectedSegment];
            }
            else
            {
                //TODO: delete failed message
            }
        }];
    }];
    [alert showNotice:NSLocalizedString(@"job.withdraw.alert.title", @"Are you sure?")
              subTitle:[NSString stringWithFormat:NSLocalizedString(@"job.withdraw.alert.message", @"withdraw job"), jobToDelete.refName]
     closeButtonTitle:NSLocalizedString(@"c8.TITLE_CANCEL", @"cancel") duration:0.0f colorStyle:0xCFCFCF colorTextButton:0xFFFFFF];
    yesButton.backgroundColor = [UIColor colorWithString:kC8ThemeColor];

}

#pragma mark - tableview delegate / datasources

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.jobPostings count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ASCJobPostingViewCell *cell = (ASCJobPostingViewCell *)[tableView dequeueReusableCellWithIdentifier:@"jobCell" forIndexPath:indexPath];
     __weak __typeof__(self) weakSelf = self;
    cell.handler = ^(ASCJobSelectedOption acn,ASCJobPostingViewCell * cell){
        __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
        if (acn == ASCJobSelectedOptionDetails) {
            
            [strongSelf showDetailsForitemIndex:indexPath.row];
        }else if (acn == ASCJobSelectedOptionRemove){
            [strongSelf removeJobAtIndex:indexPath.row];
        }
    };

    [cell configureWithDict:@{@"JOB":self.jobPostings[indexPath.row], @"IDX" : @(indexPath.row + 1), @"SEG" : @(self.selectedSegment)}];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self showDetailsForitemIndex:indexPath.row];
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UITableViewCellEditingStyleDelete == editingStyle) {
        [self removeJobAtIndex:indexPath.row];
       
    }
   
}

#pragma mark - exit segue support

- (IBAction)unwindToHome:(UIStoryboardSegue *)unwindSegue
{
    [self popToHome];
}


#pragma mark - filter support
- (void)filterByJobStatus:(NSInteger)anIndex
{
    NSMutableArray *filteredJobs = [@[] mutableCopy];
    NSArray *allJobs = [ASCLoginService sharedInstance].loggedInCandidate.jobs;
    self.selectedSegment = anIndex;
    int draftsCount = 0;
    int appliedCount = 0;
    for (ASCJobPosting *job in allJobs)
    {
        if ([job isSubmitted]) {
            appliedCount++;
        } else {
            draftsCount++;
        }
        if (anIndex == 0 && ![job isSubmitted])
        { //new jobs
            [filteredJobs addObject:job];
        }
        if (anIndex == 1 && [job isSubmitted])
        {
            [filteredJobs addObject:job];
        }
    }
    NSSortDescriptor *sortDesc = nil;
    if (self.selectedJobFilterNameIndex == 0) {
        sortDesc = [[NSSortDescriptor alloc] initWithKey:@"startDate" ascending:YES];
    } else if (self.selectedJobFilterNameIndex == 1) {
        sortDesc = [[NSSortDescriptor alloc] initWithKey:@"endDate" ascending:YES];
    } else if (self.selectedJobFilterNameIndex == 2) {
        sortDesc = [[NSSortDescriptor alloc] initWithKey:@"refName" ascending:YES];
    } else if (self.selectedJobFilterNameIndex == 3) {
        sortDesc = [[NSSortDescriptor alloc] initWithKey:@"companyName" ascending:YES];
    }
    NSArray *sortDescriptors = @[sortDesc];
    self.jobPostings = [filteredJobs sortedArrayUsingDescriptors:sortDescriptors];
    
    self.draftJobsLabel.text = [NSString stringWithFormat:@"%@ (%d)", NSLocalizedString(@"jobs.SEGMENT_TITLE_NEW", nil), draftsCount];
    self.submittedJobsLabel.text = [NSString stringWithFormat:@"%@ (%d)", NSLocalizedString(@"jobs.SEGMENT_TITLE_APPLIED", nil), appliedCount];
    
    self.draftJobsLabel.textColor = (self.selectedSegment == 0)? [UIColor colorWithString:kC8SelectedSegmentColor] : [UIColor blackColor];
    self.submittedJobsLabel.textColor = (self.selectedSegment == 1)? [UIColor colorWithString:kC8SelectedSegmentColor] : [UIColor blackColor];
    

    [self.jobTableView reloadData];
}


#pragma mark - SBPickerSelector delegate

- (IBAction)doShowJobFilterByNames:(id)sender
{
    SBPickerSelector *picker = [SBPickerSelector picker];
    
    
    picker.pickerData = [@[NSLocalizedString(@"jobs.FILTER_NEWLY_ADDED", nil),
                           NSLocalizedString(@"jobs.FILTER_EXPIRING_SOON", nil),
                           NSLocalizedString(@"jobs.FILTER_ALPHA_JOBROLE", nil),
                           NSLocalizedString(@"jobs.FILTER_ALPHA_COMPANY", nil)] mutableCopy];
    picker.pickerType = SBPickerSelectorTypeText;
    picker.delegate = self;
    picker.doneButtonTitle = NSLocalizedString(@"c8.TITLE_OK", nil);
    picker.cancelButtonTitle = NSLocalizedString(@"c8.TITLE_CANCEL", nil);
    [picker showPickerOver:self];
}

-(void)pickerSelector:(SBPickerSelector *)selector selectedValue:(NSString *)value index:(NSInteger)idx
{
    self.selectedJobFilterNameLabel.text = value;
    self.selectedJobFilterNameIndex = idx;
    [self filterByJobStatus:self.selectedSegment];
}

@end
