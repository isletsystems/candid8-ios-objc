//
//  ASCApplyByJobIdViewController.m
//  candid8
//
//  Created by Aldrin Lenny on 27/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCApplyByJobIdViewController.h"
#import "ASCJobFacade.h"
#import "ASCLoginService.h"
#import "ASCJobPostingView.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <ColorUtils/ColorUtils.h>


@interface ASCApplyByJobIdViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *applyByJobNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UITextField *jobNumberTextField;

- (IBAction)dismissMe:(id)sender;
- (IBAction)goFindJob:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *jobResultContainerView;
@property (weak, nonatomic) IBOutlet UIView *jobPostingContainerView;
@property (weak, nonatomic) IBOutlet UIButton *addToDraftsButton;
@property (weak, nonatomic) IBOutlet UILabel *jobFoundLabel;
@property (weak, nonatomic) IBOutlet UIView *jobNumberContainerView;

@property (nonatomic, strong) ASCJobPostingView *jobView;
@property (nonatomic, strong) ASCJobPosting *searchedJob;

@property (weak, nonatomic) IBOutlet UIButton *goButton;

@end

@implementation ASCApplyByJobIdViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissMe:) name:@"kASCLogoutNotification" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.errorLabel.text = @"";
    [self.jobNumberTextField becomeFirstResponder];
    [self jobFoundPreparation:false];
    self.goButton.tintColor = [UIColor colorWithString: kC8ThemeColor];
    self.addToDraftsButton.tintColor = [UIColor colorWithString: kC8ThemeColor];
}

- (IBAction)doAddToDrafts:(id)sender
{
    [[ASCJobFacade sharedInstance] applyForJobId:self.searchedJob.jid withResponseBlock:^(BOOL isSuccess, id resultData) {
        if (isSuccess)
        {
            [self dismissMe:nil];
        }
        else
        {
            //show eror
            self.errorLabel.text = NSLocalizedString(@"job.NUMBER_NOT_FOUND", nil);
        }
    }];
}

- (IBAction)searchAgainAction:(id)sender
{
    [self jobFoundPreparation:false];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.jobNumberTextField resignFirstResponder];
}

#pragma mark - button actions
- (IBAction)dismissMe:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)goFindJob:(id)sender
{
    [self.jobNumberTextField resignFirstResponder];
    
    NSString *jobNumber = self.jobNumberTextField.text;
    if ([jobNumber isEqualToString:@""])
    {
        //show error
        self.errorLabel.text = NSLocalizedString(@"job.NUMBER_EMPTY", nil);
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[ASCJobFacade sharedInstance] searchForJobWithNumber:jobNumber withResponseBlock:^(BOOL isSuccess, id resultData) {
        if (isSuccess)
        {
            [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            NSArray *jobs = (NSArray *)resultData;
            if (jobs && jobs.count > 0) {
                self.searchedJob = jobs[0];
                
                [self jobFoundPreparation:true];
                if (self.jobView)
                {
                    [self.jobView removeFromSuperview];
                }
                self.jobView = [[ASCJobPostingView alloc] initWithFrame:CGRectInset(self.jobPostingContainerView.bounds, 6, 6)];
                
                [self.jobPostingContainerView addSubview:self.jobView];
                
                [self.jobView configureWithDict:@{@"JOB":self.searchedJob, @"IDX" : @(0), @"ASC_HIDE_ELLIPSES":@YES}];
                return;
            }
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
        //show eror
        self.errorLabel.text = NSLocalizedString(@"job.NUMBER_NOT_FOUND", nil);
    }];
}


- (void)jobFoundPreparation:(BOOL)showFlag
{
    self.jobResultContainerView.hidden = !showFlag;
    self.jobNumberContainerView.hidden = showFlag;
}

@end
