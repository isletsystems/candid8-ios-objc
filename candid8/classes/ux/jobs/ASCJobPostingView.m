//
//  ASCJobPostingView.m
//  candid8
//
//  Created by Aldrin Lenny on 30/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobPostingView.h"
#import "AppConfigs.h"
#import "UIView+PREBorderView.h"
#import <ColorUtils/ColorUtils.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "ASCAppDelegate.h"

#import "ASCJobPosting.h"

#import "UIViewController+MJPopupViewController.h"
#import "ASCTSPopoverController.h"
#import "ASCJobSubmissionService.h"

@interface ASCJobPostingView()<TSPopoverDismissDelegate>{
    
}
@property (nonatomic,strong)UIButton *dismissNumberPadButton;
@property (nonatomic, strong) UINib *nib;
@property (nonatomic, strong) UINib * popUpNib;
@property (weak, nonatomic) IBOutlet UIButton *ellipsesButton;

@property (nonatomic, weak) IBOutlet UILabel *jobIdLabel;
@property (nonatomic, weak) IBOutlet UILabel *jobTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *jobPostingCompanyLabel;
@property (nonatomic, weak) IBOutlet UILabel *jobStartDateLabel;
@property (nonatomic, weak) IBOutlet UILabel *jobEndDateLabel;
@property (nonatomic, weak) IBOutlet UIImageView *jobPostingCompanyLogoImageView;
@property (nonatomic, weak) IBOutlet UIImageView *statusIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *jobNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobStatusLabel;

@property (weak, nonatomic) IBOutlet UIView *headerContainerView;
@property (weak, nonatomic) IBOutlet UIView *footerContainerView;

@property (nonatomic,strong) ASCJobPostingView * popView;
@property (nonatomic,weak) ASCTSPopoverController *popoverView;

@property (nonatomic, strong) ASCJobPosting *aJobPosting;
@property (nonatomic, strong) NSNumber *currSegment;
@property (weak, nonatomic) IBOutlet UIView *jobQAIndicatorView;
@end

@implementation ASCJobPostingView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self)
    {
        if (!self.nib)
        {
            self.nib = [UINib nibWithNibName:NSStringFromClass([ASCJobPostingView class]) bundle:nil];
        }
        NSArray *nib = [self.nib instantiateWithOwner:self options:nil];
        self = nib[0];
        self.frame = frame;
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame andMicroFlag:(BOOL)isMicro
{
    if (self)
    {
        if (!self.nib)
        {
            self.nib = [UINib nibWithNibName:@"ASCJobPostingMicroView" bundle:nil];
        }
        NSArray *nib = [self.nib instantiateWithOwner:self options:nil];
        self = nib[0];
        self.frame = CGRectMake(0,0,frame.size.width, frame.size.height);
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame andDetailFlag:(BOOL)isMicro
{
    if (self)
    {
        if (!self.nib)
        {
            self.nib = [UINib nibWithNibName:@"ASCJobPostingDetailView" bundle:nil];
        }
        NSArray *nib = [self.nib instantiateWithOwner:self options:nil];
        self = nib[0];
        self.frame = CGRectMake(0,0,frame.size.width, frame.size.height);
    }
    return self;
}


- (void)configureWithDict:(NSDictionary *)aDict
{
    self.aJobPosting = aDict[@"JOB"];
    [self.headerContainerView addBorderWithColor:[UIColor colorWithString:kC8LineColor] andWidth:0.5 atPosition:PREBorderPositionBottom];
    
    self.jobTitleLabel.text = self.aJobPosting.refName;
    self.jobPostingCompanyLabel.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"job.POSTED_BY", nil),self.aJobPosting.companyName];
    self.jobEndDateLabel.text = @"";
    self.jobNumberLabel.text = self.aJobPosting.refNo;
    if (self.aJobPosting.endDate)
    {
        if ([self.aJobPosting isExpired])
        {
            self.jobEndDateLabel.text = NSLocalizedString(@"job.EXPIRY_STATUS_EXPIRED", nil);
        }
        else
        {
            self.jobEndDateLabel.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"job.EXPIRY_STATUS_OPEN", nil),[ASCJobPosting formattedDate:self.aJobPosting.endDate]];
        }
    }
    else
    {
        self.jobEndDateLabel.text = NSLocalizedString(@"job.EXPIRY_STATUS_NO_CLOSE_DATE", nil);
    }
    
    self.jobStatusLabel.text = NSLocalizedString(@"job.STATUS_DRAFT", nil);
    if ([self.aJobPosting isSubmitted]) {
        self.jobStatusLabel.text = NSLocalizedString(@"job.STATUS_SUBMITTED", nil);
    }
    
    [self.jobPostingCompanyLogoImageView sd_setImageWithURL:[NSURL URLWithString:self.aJobPosting.ikn == nil ? self.aJobPosting.employer.logo : self.aJobPosting.ikn]
                                           placeholderImage:[UIImage imageNamed:@"icon_job_placeholder"]];
    
    BOOL hideElipses = [aDict[@"ASC_HIDE_ELLIPSES"] boolValue];
    if (hideElipses) {
        self.ellipsesButton.hidden = true;
    } else {
        self.ellipsesButton.hidden = false;
    }
    
    self.currSegment = aDict[@"SEG"];
    [self renderJobQAIndicatorContent];
}


- (IBAction)btnDetailTouched:(UIButton *)sender {
    if (!self.popUpNib) {
        self.popUpNib =  [UINib nibWithNibName:@"ASCJobPostingPopUp" bundle:nil];
    }
    self.popView = [_popUpNib instantiateWithOwner:self options:nil][0];
    UILabel *jobNumLabel = (UILabel *)[self.popView viewWithTag:100];
    jobNumLabel.text = self.aJobPosting.refNo;
    
    UIButton *widrawApplnButtn = (UIButton *)[self.popView viewWithTag:200];
    if (self.currSegment && widrawApplnButtn && self.currSegment.intValue == 0) {
        [widrawApplnButtn setTitle:NSLocalizedString(@"job.delete.button.title", nil) forState:UIControlStateNormal];
    } else {
        [widrawApplnButtn setTitle:NSLocalizedString(@"job.withdraw.button.title", nil) forState:UIControlStateNormal];
    }
    
    self.popView.onOptionSelect = self.onOptionSelect;
    self.popView.frame = self.bounds;
    [self addSubview:self.popView];
}

-(CGSize)intrinsicContentSize
{
    return CGSizeMake([UIScreen mainScreen].applicationFrame.size.width, self.frame.size.height);
}

- (IBAction)btnShowDetailTouch:(UIButton *)sender {
    [self removePopUpWithOutCallingBlock];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.onOptionSelect) {
            self.onOptionSelect(ASCJobSelectedOptionDetails,self);
        }
        self.popView.onOptionSelect = nil;

    });
}

- (IBAction)btnRemoveTouch:(UIButton *)sender {
    [self removePopUpWithOutCallingBlock];
    if (self.onOptionSelect) {
        self.onOptionSelect(ASCJobSelectedOptionRemove,self);
    }
    
    self.popView.onOptionSelect = nil;
}

- (IBAction)removePopUpView:(UIButton *)sender{
    if (self.onOptionSelect) {
        self.onOptionSelect(ASCJobSelectedOptionClickedDismissBtn,self);
    }
    [self removePopUpWithOutCallingBlock];
}

- (void)removePopUpWithOutCallingBlock{
    if (self.popView) {
        [self.popView removeFromSuperview];
        self.popView = nil;
    }else{
        [self removeFromSuperview];
    }
    if (self.dismissNumberPadButton) {
        [self.dismissNumberPadButton  removeFromSuperview];
        self.dismissNumberPadButton = nil;
    }else{
        [self.popView.dismissNumberPadButton  removeFromSuperview];
        self.popView.dismissNumberPadButton = nil;
    }
}

#pragma mark -  TSPopoverDismissDelegate
-(void)popOverControllerDidDismissed{
    
    self.popoverView.delegate = nil;
    self.popoverView = nil;
    if (self.onOptionSelect) {
        self.onOptionSelect(ASCJobSelectedOptionClickedDismissBtn,self);
    }
}


-(void)renderJobQAIndicatorContent {
    NSUInteger qCount = self.aJobPosting.questions.count;
    CGFloat separatorWidth = 2.0f;
    for (UIView *vu in self.jobQAIndicatorView.subviews) {
        [vu removeFromSuperview];
    }
    if (qCount == 0) {
        return;
    }
    CGFloat qWidth = (self.frame.size.width-(separatorWidth*qCount-1))/qCount;
    CGFloat qHeight = self.jobQAIndicatorView.frame.size.height;
    CGFloat x = 0;
    int idx = 0;
    for (ASCJobPostingQuestion *quest in self.aJobPosting.questions) {
        BOOL isAttempted = quest.isAttempted;
        if (!isAttempted) {
            //check locally
            if ([[ASCJobSubmissionService sharedInstance] hasAttemptedLocallyQuestion:quest onJob:self.aJobPosting]) {
                isAttempted = TRUE;
            }
        }
        UIView *qVu = [[UIView alloc] initWithFrame:CGRectMake(x + ((qWidth+separatorWidth)*idx), 0, qWidth, qHeight)];
        qVu.backgroundColor = isAttempted ? [UIColor colorWithString:kC8LineColor] : [UIColor lightGrayColor];
        [self.jobQAIndicatorView addSubview:qVu];
        idx++;
    }
}


@end
