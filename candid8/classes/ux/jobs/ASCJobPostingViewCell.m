//
//  ASCJobPostingViewCell.m
//  candid8
//
//  Created by Aldrin Lenny on 16/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobPostingViewCell.h"



@interface ASCJobPostingViewCell()

@property (nonatomic, strong) ASCJobPostingView *jobView;

@end

@implementation ASCJobPostingViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)configureWithDict:(NSDictionary *)aDict
{
    if (self.jobView)
    {
        [self.jobView removeFromSuperview];
    }
    if (aDict && aDict[@"micro"])
    {
        self.jobView = [[ASCJobPostingView alloc] initWithFrame:CGRectInset(self.contentView.frame, 6, 6) andMicroFlag:YES];
    }
    else
    {
        self.jobView = [[ASCJobPostingView alloc] initWithFrame:CGRectInset(self.contentView.frame, 6, 6)];
    }
    [self.contentView addSubview:self.jobView];
    
    [self.jobView configureWithDict:aDict];
    self.jobView.popUpOptions = @[@(ASCJobSelectedOptionDetails),@(ASCJobSelectedOptionRemove)];
    if (self.handler) {
           __weak __typeof__(self) weakSelf = self;
        self.jobView.onOptionSelect = ^(ASCJobSelectedOption actn,ASCJobPostingView *theView){
            __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
            strongSelf.handler(actn,strongSelf);
        };
    }
}

@end
