//
//  ASCJobPostingView.h
//  candid8
//
//  Created by Aldrin Lenny on 30/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, ASCJobSelectedOption) {
    ASCJobSelectedOptionClickedDismissBtn,
    ASCJobSelectedOptionDetails,
    ASCJobSelectedOptionRemove
};
@class ASCJobPostingView;
typedef void (^ASCJobOptionAction)(ASCJobSelectedOption actn,ASCJobPostingView *theView);

@interface ASCJobPostingView : UIView<IBaseConfigurableView>{
    
}

- (instancetype)initWithFrame:(CGRect)frame andMicroFlag:(BOOL)isMicro;

@property (nonatomic,copy)ASCJobOptionAction onOptionSelect;
@property (nonatomic,strong)NSArray * popUpOptions;

- (instancetype)initWithFrame:(CGRect)frame andDetailFlag:(BOOL)isMicro;

@end
