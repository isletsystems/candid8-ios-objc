//
//  ASCMoreViewController.m
//  candid8
//
//  Created by Aldrin Lenny on 16/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCMoreViewController.h"
#import <ColorUtils/ColorUtils.h>
#import "ASCLoginService.h"
#import "ASCJobListingViewController.h"
#import "ASCUtils.h"
#import "ASCCandidateProfileService.h"

#import <RESideMenu/RESideMenu.h>
#import <RESideMenu/UIViewController+RESideMenu.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "ASCS3Manager.h"

@interface ASCMoreViewController ()


@property (weak, nonatomic) IBOutlet UILabel *candidateNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *candidatePhotoImageView;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UIView *moreView;

- (IBAction)takeMeToC8Site:(id)sender;
- (IBAction)likeInFB:(id)sender;
- (IBAction)followInTwitter:(id)sender;
- (IBAction)followInGPlus:(id)sender;
@end

@implementation ASCMoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.moreView.backgroundColor = [UIColor colorWithString:kC8DarkGrayColor];
    self.candidateNameLabel.textColor = [UIColor colorWithString:kC8ThemeColor];
    self.candidatePhotoImageView.layer.cornerRadius = self.candidatePhotoImageView.frame.size.width/2;
    self.candidatePhotoImageView.layer.masksToBounds = TRUE;
    self.candidatePhotoImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.candidatePhotoImageView.layer.borderWidth = 2.0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin) name:@"kASCLoginSuccessNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdatePhoto:) name:@"kASCProfilePhotoUpdatedNotification" object:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.versionLabel.text = [NSString stringWithFormat:@"ver %@", [ASCUtils appVersionInfo]];
}

- (void)didLogin {
    
    ASCCandindate *c8 = [ASCLoginService sharedInstance].loggedInCandidate;
    self.candidateNameLabel.text = [c8 fullName];
    
    [self.candidatePhotoImageView sd_setImageWithURL:[NSURL URLWithString:c8.photo]
                                    placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    __weak __typeof__(self) weakSelf = self;
    
    if ([ASCLoginService sharedInstance].loggedInCandidate) {
        [[ASCCandidateProfileService sharedInstance] fetchProfileOfCandidate:[ASCLoginService sharedInstance].loggedInCandidate.cid withResponseBlock:^(BOOL isSuccess, id resultData) {
            __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
            if (isSuccess) {
                [strongSelf displayProfilePic:(ASCCandindate *)resultData];
            }
        }];
    }
}

- (void)didUpdatePhoto:(NSNotification *)notification
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
        self.candidatePhotoImageView.image = notification.object;
    });
}


-(void)displayProfilePic:(ASCCandindate *)theCandida8
{
    [[ASCS3Manager sharedInstance] loadImagefromS3Key:theCandida8.photo withCompletion:^(BOOL isSuccess, id resultData) {
        if (isSuccess) {
            self.candidatePhotoImageView.image = (UIImage *)resultData;
        }
    }];
    
}

- (IBAction)takeMeToC8Site:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kSiteURL]];
}

- (IBAction)likeInFB:(id)sender
{
}

- (IBAction)followInTwitter:(id)sender
{
}

- (IBAction)followInGPlus:(id)sender
{
}

- (IBAction)takeMeToJobs:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCLoginSuccessNotification" object:nil];
    //toggles menu
    [self.sideMenuViewController hideMenuViewController];
}
- (IBAction)takeMeToProfile:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCShowProfile" object:nil];
    //toggles menu
    [self.sideMenuViewController hideMenuViewController];
}
- (IBAction)takeMeToHelpSupport:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCShowHelpSupport" object:nil];
    //toggles menu
    [self.sideMenuViewController hideMenuViewController];
}

- (IBAction)takeMeToJobSearch:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCShowJobApplyByNumberNotification" object:nil];
    //toggles menu
    [self.sideMenuViewController hideMenuViewController];
}

- (IBAction)takeMeToLogin:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCLogoutNotification" object:nil];
    //toggles menu
    [self.sideMenuViewController hideMenuViewController];
}




@end
