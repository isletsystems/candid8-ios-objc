//
//  IBaseConfigurableView.h
//  candid8
//
//  Created by Aldrin Lenny on Jun 23, 2014.
//
//

@protocol IBaseConfigurableView <NSObject>

- (void)configureWithDict:(NSDictionary *)aDict;

@end
