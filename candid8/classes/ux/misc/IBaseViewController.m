//
//  IBaseViewController.m
//  candid8
//
//  Created by Aldrin Lenny on 16/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "IBaseViewController.h"
#import <ColorUtils/ColorUtils.h>
#import <RESideMenu/RESideMenu.h>
#import <RESideMenu/UIViewController+RESideMenu.h>

@interface IBaseViewController ()

@end

@implementation IBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithString:kC8LightGrayColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}

#pragma mark - common nav methods

- (IBAction)goHome:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCGoHomeNotification" object:nil];
}


- (IBAction)showSideMenu:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCShowSideMenuNotification" object:nil];
}

- (IBAction)showJobSearch:(id)sender
{
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"JobSearchScene"];
    if (vc) {
        [self presentViewController:vc animated:YES completion:^{
            //do nothing
        }];
    }
}


- (IBAction)showTCs:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCShowTCsNotification" object:nil];
}


- (IBAction)showPrivacyPolicy:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCShowPrivacyNotification" object:nil];
}

@end
