//
//  SCLFlatAlertView.swift
//  candid8
//
//  Created by Aldrin Lenny on 25/10/15.
//  Copyright © 2015 AppSails. All rights reserved.
//

import Foundation

import SCLAlertView

class SCLFlatAlertView : SCLAlertView {
    
    required init() {
        super.init()
        showCircularIcon = false
        showCloseButton = false
        contentViewCornerRadius = 0.0
        buttonCornerRadius = 5.0
        fieldCornerRadius = 0.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
