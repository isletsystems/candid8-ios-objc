//
//  IBaseViewController.h
//  candid8
//
//  Created by Aldrin Lenny on 16/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//



#import "AppConfigs.h"

@interface IBaseViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *footerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

- (IBAction)goHome:(id)sender;

- (IBAction)showSideMenu:(id)sender;

- (IBAction)showJobSearch:(id)sender;


- (IBAction)showTCs:(id)sender;
- (IBAction)showPrivacyPolicy:(id)sender;

@end
