//
//  ASCWrittenResponseView.m
//  candid8
//
//  Created by Aldrin Lenny on 24/06/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCWrittenResponseView.h"
#import "ASCJobPostingDetailsService.h"
#import "ASCJobSubmissionService.h"

NSInteger kMaxCharsForWrittenAnswer = 500;

@interface ASCWrittenResponseView()<UITextViewDelegate>

@property (nonatomic, strong) UINib *nib;
@property (nonatomic, weak) IBOutlet UITextView *writtenTextView;
@property (nonatomic, weak) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UILabel *responseLabel;
@property (weak, nonatomic) IBOutlet UILabel *maxCharsLabel;

- (IBAction)saveResponse:(id)sender;

@end

@implementation ASCWrittenResponseView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self)
    {
        if (!self.nib)
        {
            self.nib = [UINib nibWithNibName:NSStringFromClass([ASCWrittenResponseView class]) bundle:nil];
        }
        NSArray *nib = [self.nib instantiateWithOwner:self options:nil];
        self = nib[0];
        self.frame = CGRectMake(0,0,frame.size.width, frame.size.height);
    }
    return self;
}

- (void)configureWithDict:(NSDictionary *)aDict
{
    ASCJobPostingQuestion *selQuest = [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion;
    ASCJobPosting *selJob = [ASCJobPostingDetailsService sharedInstance].selectedJob;
    
    NSString *writtenText = selQuest.answer.writtenText;
    if (!writtenText)
    {
        writtenText = @"";
    }
    
    self.doneButton.hidden = TRUE;
    if ([selJob isSubmitted])
    {
        self.writtenTextView.hidden = TRUE;
        self.responseLabel.hidden = FALSE;
        self.responseLabel.text = writtenText;
    }
    else
    {
        if ([selJob isExpired])
        {
            self.writtenTextView.hidden = YES;
            self.responseLabel.hidden = NO;
            self.responseLabel.text = writtenText;
        }
        else
        {
            self.writtenTextView.editable = TRUE;
            self.writtenTextView.hidden = FALSE;
            self.responseLabel.hidden = TRUE;
            self.writtenTextView.text = writtenText;
        }
        
        [self updateMaxCharsInfo:writtenText];
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    self.doneButton.hidden = FALSE;
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(responseTextViewTapped:)])
    {
        [self.delegate responseTextViewTapped:textView];
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    BOOL canChange = textView.text.length + (text.length - range.length) <= kMaxCharsForWrittenAnswer;

    return canChange;
}

-(void)textViewDidChange:(UITextView *)textView
{
    [self updateMaxCharsInfo:textView.text];
}

- (void)updateMaxCharsInfo:(NSString *)aText
{
    self.maxCharsLabel.text = [NSString stringWithFormat:NSLocalizedString(@"job.response.TEXT_MAXCHAR", nil), aText.length, kMaxCharsForWrittenAnswer];
}

- (IBAction)saveResponse:(id)sender
{
    [self.writtenTextView resignFirstResponder];
    self.doneButton.hidden = TRUE;
    
    //check if empty
    NSString *responseTextTrimmed = [self.writtenTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (responseTextTrimmed && [responseTextTrimmed isEqualToString:@""])
    {
        //response text cannot be empty.
        //show alert
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"job.response.TEXT_EMPTY_TITLE", nil) message:NSLocalizedString(@"job.response.TEXT_EMPTY_MESSAGE", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"c8.TITLE_OK", nil), nil] show];
        return;
    }
    
    //save to user defaults
    ASCJobPostingQuestion *selQuest = [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion;
    ASCJobPosting *selJob = [ASCJobPostingDetailsService sharedInstance].selectedJob;
    selQuest.answer.writtenText = responseTextTrimmed;
    selQuest.answer.status = @(ASCResponseModeWrite);
    selQuest.answer.answerType = @(ASCResponseModeWrite);
    selQuest.answer.url = @"";
    
    //save it locally for reuse on relaunch
    NSDictionary *respDict = @{C8_RESPMODE_KEY: @(ASCResponseModeWrite), C8_RESPDATA_KEY : responseTextTrimmed};
    [[ASCJobSubmissionService sharedInstance] setLocalResponseDict:respDict forQuestion:selQuest onJob:selJob];
}

@end
