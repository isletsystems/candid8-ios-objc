//
//  ASCResponseWrittenViewController.swift
//  candid8
//
//  Created by Aldrin ML on 1/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

import UIKit

class ASCResponseWrittenViewController: ASCBaseViewController {
    
    let kMaxCharsForWrittenAnswer = 500
    
    @IBOutlet weak var writtenResponseTextView: UITextView!
    @IBOutlet weak var characterInfoLabel: UILabel!
    @IBOutlet weak var switchToVideoButton: UIButton!
    @IBOutlet weak var finishButton: UIButton!
    @IBOutlet weak var finishLabel: UILabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var seperatorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let bgColor = UIColor(string: AppConfig.Theme.kC8BlueColor)
        titleView?.backgroundColor = bgColor
        let sepColor = UIColor(string: AppConfig.Theme.kC8LightBlueColor)
        seperatorView?.backgroundColor = sepColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "hideKeyboard:", name: "hideKeyboard", object: nil)
        updateContent()
        updateTypedCharactersInfo(writtenResponseTextView.text)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func updateTypedCharactersInfo(text:String) {
        characterInfoLabel.text = "\(text.characters.count)/\(kMaxCharsForWrittenAnswer)"
    }
    
    func updateContent() {
        let selJob = ASCJobPostingDetailsService.sharedInstance().selectedJob
        if selJob.isExpired() || selJob.isSubmitted() {
            writtenResponseTextView.editable = false
            finishButton.enabled = false
            switchToVideoButton?.enabled = false
        } else {
            writtenResponseTextView.editable = true
            finishButton.enabled = true
            switchToVideoButton?.enabled = true
        }
        
        if let selQuestion = ASCJobPostingDetailsService.sharedInstance().selectedJobQuestion {
            writtenResponseTextView?.text = selQuestion.answer?.notes
        }
    }
    
    @IBAction func switchResponseType(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName(kASCDoVideoResponse, object: nil)
    }
    
    @IBAction func doFinishAction(sender: AnyObject) {
        
        //validate if the response is not empty.
        guard let responseText = self.writtenResponseTextView.text where (!responseText.isEmpty && responseText.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()).characters.count != 0 )else {
            let alert = SCLFlatAlertView()
            let okBtn = alert.addButton(NSLocalizedString("c8.TITLE_OK", comment: "")) { () -> Void in
            }
            alert.showError(NSLocalizedString("job.response.TEXT_EMPTY_TITLE", comment: ""), subTitle: NSLocalizedString("job.response.TEXT_EMPTY_MESSAGE", comment: ""))
            okBtn.backgroundColor = UIColor(string: kC8ButtonColor)
            
            return
        }
        
        let alert = SCLFlatAlertView()
        let yesButton = alert.addButton(NSLocalizedString("c8.TITLE_YES", comment: "")) { () -> Void in
            //validate the response text
            let selectedJob = ASCJobPostingDetailsService.sharedInstance().selectedJob
            if let selQuestion = ASCJobPostingDetailsService.sharedInstance().selectedJobQuestion {
                ASCJobFacade.sharedInstance().submitWrittenResponse(responseText,
                    job: selectedJob,
                    question:selQuestion,
                    completion: { (status:Bool, response:AnyObject!) -> Void in
                        
                        debugPrint("status:\(status), response:\(response)")
                        if status {
                            let salert = SCLFlatAlertView()
                            let okBtn = salert.addButton(NSLocalizedString("c8.TITLE_OK", comment: "")) { () -> Void in
                                ASCJobPostingDetailsService.sharedInstance().selectedJobQuestion.answer = response as? ASCJobPostingResponse
                                self.updateContent()
                            }
                            salert.showSuccess(NSLocalizedString("job.response.save.success.title", comment: ""),
                                subTitle: NSLocalizedString("job.response.save.success.message", comment: ""))
                            okBtn.backgroundColor = UIColor(string: kC8ButtonColor)
                            
                        } else {
                            let salert = SCLFlatAlertView()
                            let okBtn = alert.addButton(NSLocalizedString("c8.TITLE_OK", comment: "")) { () -> Void in
                            }
                            salert.showError(NSLocalizedString("job.response.save.failed.title", comment: ""),
                                subTitle: NSLocalizedString("job.response.save.failed.message", comment: ""))
                            okBtn.backgroundColor = UIColor(string: kC8ButtonColor)
                        }
                })
            }
        }
        alert.addButton(NSLocalizedString("c8.TITLE_CANCEL", comment: "")) { () -> Void in
        }
        alert.showNotice(NSLocalizedString("job.response.alert.title", comment: ""),
            subTitle: NSLocalizedString("job.response.alert.message", comment: ""))
        yesButton.backgroundColor = UIColor(string: kC8ButtonColor)
    }
}

extension ASCResponseWrittenViewController : UITextViewDelegate {
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        let canChange = textView.text.characters.count + (text.characters.count - range.length) <=  kMaxCharsForWrittenAnswer
        return canChange
    }
    
    func textViewDidChange(textView: UITextView) {
        updateTypedCharactersInfo(textView.text)
    }
    
    @IBAction func hideKeyboard(sender: AnyObject) {
        self.writtenResponseTextView.resignFirstResponder()
    }
}
