//
//  ASCJobQuestionViewCell.h
//  candid8
//
//  Created by Aldrin Lenny on 17/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBaseConfigurableView.h"

@interface ASCJobQuestionViewCell : UITableViewCell<IBaseConfigurableView>

@end
