//
//  ASCResponseRecorderViewController.m
//  candid8
//
//  Created by Aldrin Lenny on 07/08/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCResponseRecorderViewController.h"

#import <AssetsLibrary/AssetsLibrary.h>
#import <PBJVision/PBJVision.h>
#import "ASCJobPostingDetailsService.h"
#import "ASCJobQuestionAnswerRecordingOverlayView.h"
#import "ASCJobSubmissionService.h"

@interface ASCResponseRecorderViewController ()<PBJVisionDelegate, ASCVideoRecorderDelegate>

@property (nonatomic, strong) ASCJobQuestionAnswerRecordingOverlayView *overlay;

@end

@implementation ASCResponseRecorderViewController {
    
    ALAssetsLibrary *_assetLibrary;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//https://github.com/piemonte/PBJVideoPlayer
//https://github.com/piemonte/PBJVision
//http://stackoverflow.com/questions/13465746/ios-avfoundation-setting-orientation-of-video

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //reset record flag
    [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion.answer.recordRightAwayFlag = nil;
    [self setupCapture];
}

//http://www.sebastianborggrewe.de/only-make-one-single-view-controller-rotate/
- (void)canRotate {}

- (BOOL)shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self adjustCameraForOrientation];
}

- (void)setupCapture
{
    PBJVision *vision = [PBJVision sharedInstance];
    vision.delegate = self;
    [vision setCameraMode:PBJCameraModeVideo];
    if ([vision isCameraDeviceAvailable:PBJCameraDeviceFront])
    {
        [vision setCameraDevice:PBJCameraDeviceFront];
    } else if ([vision isCameraDeviceAvailable:PBJCameraDeviceBack])
    {
        [vision setCameraDevice:PBJCameraDeviceBack];
    }
    [vision setFocusMode:PBJFocusModeAutoFocus];
    
    if (!self.overlay)
    {
        self.overlay = [[ASCJobQuestionAnswerRecordingOverlayView alloc] initWithFrame:self.view.frame];
    }
    [self.overlay configureWithJobPostingQuestion:[ASCJobPostingDetailsService sharedInstance].selectedJobQuestion andRecorderDelegate:self];
    AVCaptureVideoPreviewLayer *_previewLayer = [[PBJVision sharedInstance] previewLayer];
    _previewLayer.frame = self.overlay.bounds;
    _previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:vision.previewLayer];
    [self.view addSubview:self.overlay];
    
    [vision startPreview];
}

#pragma mark - ASCVideoRecorderDelegate methods
- (void)videoRecordingSessionCancelled
{
    [self dismissViewControllerAnimated:YES completion:^{
        DLog(@"videoRecordingSessionCancelled");
        if ([[PBJVision sharedInstance] isRecording]) {
            [[PBJVision sharedInstance] cancelVideoCapture];
        }

    }];
}

- (void)videoRecordingSessionTimerExpired
{
    //stop recording
    //start saving the file
    DLog(@"videoRecordingSessionTimerExpired");
    if ([[PBJVision sharedInstance] isRecording]) {
        [[PBJVision sharedInstance] endVideoCapture];
    }
}

- (void)videoRecordingSessionStarted
{
    //anything to do now?
    //prepare the file to save recording and start recording
    
    DLog(@"videoRecordingSessionStarted");
    [[PBJVision sharedInstance] startVideoCapture];
}

- (void)videoRecordingSessionStopped
{
    //stop recording
    //start saving the file
    DLog(@"videoRecordingSessionStopped");
    
    [[PBJVision sharedInstance] endVideoCapture];
}

- (void)toggleCameraSide
{
    PBJVision *vision = [PBJVision sharedInstance];
    if (vision.cameraDevice == PBJCameraDeviceFront)
    {
        if ([vision isCameraDeviceAvailable:PBJCameraDeviceBack])
        {
            [vision setCameraDevice:PBJCameraDeviceBack];
        }
    }
    else
    {
        if ([vision isCameraDeviceAvailable:PBJCameraDeviceFront])
        {
            [vision setCameraDevice:PBJCameraDeviceFront];
        }
    }
}

- (void)adjustCameraForOrientation
{
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    PBJVision *vision = [PBJVision sharedInstance];
    CGRect newFrame = self.overlay.bounds;
    if (UIDeviceOrientationIsPortrait(orientation))
    {
        if (newFrame.size.width > newFrame.size.height)
        {
            newFrame = CGRectMake(0, 0, newFrame.size.height, newFrame.size.width);
        }
        
        if (orientation == UIDeviceOrientationPortrait)
        {
            [vision setCameraOrientation:PBJCameraOrientationPortrait];
        }
        else
        {
            [vision setCameraOrientation:PBJCameraOrientationPortrait];
        }
    }
    else
    {
        if (newFrame.size.height > newFrame.size.width)
        {
            newFrame = CGRectMake(0, 0, newFrame.size.height, newFrame.size.width);
        }
        if (orientation == UIDeviceOrientationLandscapeLeft)
        {
            [vision setCameraOrientation:PBJCameraOrientationLandscapeRight];
        }
        else
        {
            [vision setCameraOrientation:PBJCameraOrientationLandscapeLeft];
        }
    }
    vision.previewLayer.frame = newFrame;
}


#pragma mark - PBJVisionDelegate

- (void)vision:(PBJVision *)vision capturedVideo:(NSDictionary *)videoDict error:(NSError *)error
{
    if (error && [error.domain isEqual:PBJVisionErrorDomain] && error.code == PBJVisionErrorCancelled) {
        DLog(@"recording session cancelled");
        return;
    } else if (error) {
        DLog(@"encounted an error in video capture (%@)", error);
        return;
    }
    
    NSString *videoPath = [videoDict objectForKey:PBJVisionVideoPathKey];
    
//http://iosdevelopertips.com/data-file-management/iphone-file-system-creating-renaming-and-deleting-files.html
    NSError *fileError;
    
    // Create file manager
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    
    NSString *respVideoPath = [self localPathToMovie];
    
    // remove the file first if it exists
    if ([fileMgr removeItemAtPath:respVideoPath error:&error] != YES)
        NSLog(@"Unable to delete file: %@", [error localizedDescription]);
    
    // move the captured file to the destination file
    if ([fileMgr moveItemAtPath:videoPath toPath:respVideoPath error:&fileError] != YES)
    {
        DLog(@"Unable to move file: %@", [error localizedDescription]);
    }
    else
    {
        ASCJobPostingResponse *questionResponse = [ASCJobPostingResponse new];
        questionResponse.videoLocalUrl = respVideoPath;
        ASCJobPostingQuestion *selQuest = [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion;
        selQuest.answer = questionResponse;
        
        //locally save store it in pref.
        ASCJobPosting *selJob = [ASCJobPostingDetailsService sharedInstance].selectedJob;
        [[ASCJobSubmissionService sharedInstance]
            setLocalResponseDict:@{C8_RESPMODE_KEY:@(ASCResponseModeRecord), C8_RESPDATA_KEY:respVideoPath}
            forQuestion:selQuest
            onJob:selJob];
    }
    [self stillImageCapture];
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)visionDidStartVideoCapture:(PBJVision *)vision
{
}

- (NSString *)localPathToMovie
{
    ASCJobPosting *selJob = [ASCJobPostingDetailsService sharedInstance].selectedJob;
    ASCJobPostingQuestion *selQuest = [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion;
    return [[ASCJobSubmissionService sharedInstance] localPathToMovieForQuestion:selQuest onJob:selJob];
}

- (void)stillImageCapture
{
    ASCJobPostingQuestion *selQuest = [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion;
    
    UIImage *stillImage = [self generateStillImageFromVideoPath:[NSURL fileURLWithPath:selQuest.answer.videoLocalUrl] atTimeSlice:0.0];
    if (stillImage)
    {
        //save to local path
        NSString *stillImageUrl = [selQuest.answer.videoLocalUrl stringByReplacingOccurrencesOfString:C8_VIDEO_TYPE_EXTENSION withString:C8_IMAGE_TYPE_EXTENSION];
        selQuest.answer.stillImageLocalUrl = stillImageUrl;
        BOOL status = [UIImageJPEGRepresentation(stillImage, 1.0) writeToFile:stillImageUrl atomically:YES];
        DLog(@"file write status: %d, %@", status, stillImageUrl);
    }
}

- (UIImage *)generateStillImageFromVideoPath:(NSURL *)aVideoPathURL atTimeSlice:(Float64)aTimeSlice
{
    UIImage *myImage = nil;
    
    AVAsset *myAsset = [AVURLAsset URLAssetWithURL:aVideoPathURL options:nil];;
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:myAsset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    Float64 durationSeconds = CMTimeGetSeconds([myAsset duration]);
    aTimeSlice = MAX(0, aTimeSlice);
    aTimeSlice = MIN(aTimeSlice, durationSeconds);
    CMTime atPoint = CMTimeMakeWithSeconds(aTimeSlice, 600);
    
    NSError *error;
    CMTime actualTime;
    CGImageRef imageAtGivenSlice = [imageGenerator copyCGImageAtTime:atPoint
                                                          actualTime:&actualTime error:&error];
    if (imageAtGivenSlice != NULL)
    {
        NSString *actualTimeString = (__bridge NSString *)CMTimeCopyDescription(NULL, actualTime);
        NSString *requestedTimeString = (__bridge NSString *)CMTimeCopyDescription(NULL, atPoint);
        DLog(@"Got still image: Asked for %@, got %@", requestedTimeString, actualTimeString);
        // Do something interesting with the image.
        myImage = [[UIImage alloc] initWithCGImage:imageAtGivenSlice];
        CGImageRelease(imageAtGivenSlice);
    }
    return myImage;
}


@end
