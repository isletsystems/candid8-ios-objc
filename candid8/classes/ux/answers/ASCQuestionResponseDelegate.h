//
//  ASCQuestionResponseDelegate.h
//  candid8
//
//  Created by Aldrin Lenny on 28/06/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

@protocol ASCVideoResponseDelegate <NSObject>

@optional
- (void)videoPlayTapped;
- (void)videoRecordTapped;

@end


@protocol ASCWrittenResponseDelegate<NSObject>

@optional
- (void)responseTextViewTapped:(UITextView *)aView;

@end