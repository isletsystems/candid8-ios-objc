//
//  ASCRecordedResponseView.h
//  candid8
//
//  Created by Aldrin Lenny on 24/06/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASCQuestionResponseDelegate.h"
#import "IBaseConfigurableView.h"

@interface ASCRecordedResponseView : UIView<IBaseConfigurableView>

@property (nonatomic, strong) id<ASCVideoResponseDelegate> delegate;

@end
