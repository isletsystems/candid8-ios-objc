//
//  ASCResponseViewController.m
//  candid8
//
//  Created by Aldrin Lenny on 24/06/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCResponseViewController.h"
#import "ASCJobPostingDetailsService.h"
#import "ASCOptedOutResponseView.h"
#import "ASCRecordedResponseView.h"
#import "ASCWrittenResponseView.h"

#import "ASCQuestionResponseDelegate.h"
#import "ASCJobQuestionResponseVideoPlayerViewController.h"
#import "ASCS3Manager.h"
#import "ASCJobSubmissionService.h"

#import <MediaPlayer/MediaPlayer.h>
#import <CoreMedia/CoreMedia.h>
#import "UIView+PREBorderView.h"
#import <ColorUtils/ColorUtils.h>
#import "AppConfigs.h"

#import "ASCJobPostingViewCell.h"
#import "ASCResponseTableViewCell.h"
#import "ASCJobQuestionViewCell.h"

#pragma mark - tableview extension
@interface UITableView (MyCoolExtension)

- (NSIndexPath *)indexPathForCellContainingView:(UIView *)view;

@end

@interface ASCResponseViewController ()<IBaseConfigurableView, ASCVideoResponseDelegate,ASCWrittenResponseDelegate,
    UIActionSheetDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) ASCJobPosting *jobPosting;

@property (nonatomic, strong) IBOutlet UIView *responseContainerView;

@property (nonatomic, weak) IBOutlet UILabel *switchResponseLabel;
@property (weak, nonatomic) IBOutlet UIView *jobHeaderView;

@property (weak, nonatomic) IBOutlet UITableView *mainTableView;


@property (weak, nonatomic) IBOutlet UIView *footerContainerView;
@property (weak, nonatomic) IBOutlet UIButton *prevQuestBBI;
@property (nonatomic, weak) IBOutlet UILabel *prevQuestLabel;
@property (weak, nonatomic) IBOutlet UIButton *nextQuestBBI;
@property (nonatomic, weak) IBOutlet UILabel *nextQuestLabel;
@property (weak, nonatomic) IBOutlet UIButton *switchResponseTypeBBI;

@property (nonatomic, readwrite) ASCResponseMode currRespMode;
@property (nonatomic, strong) UIActionSheet *actionSheet;

- (IBAction)showSwitchResponseSheet:(id)sender;

- (IBAction)showPrevQuest:(id)sender;
- (IBAction)showNextQuest:(id)sender;

@end

@implementation ASCResponseViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self configureWithDict:nil];
    NSString *recordRightAwayFlag = [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion.answer.recordRightAwayFlag;
    if (recordRightAwayFlag)
    {
        [self performSelector:@selector(videoRecordTapped) withObject:self afterDelay:.20];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


-(void)configureWithDict:(NSDictionary *)aDict
{
    self.jobPosting = [ASCJobPostingDetailsService sharedInstance].selectedJob;
    
    self.switchResponseLabel.text = NSLocalizedString(@"job.response.SWITCH_INFO", nil);
    self.prevQuestLabel.text = NSLocalizedString(@"job.response.PREV_TITLE", nil);
    self.nextQuestLabel.text = NSLocalizedString(@"job.response.NEXT_TITLE", nil);
    
    [self setupFooterControls];
    [self.mainTableView reloadData];
}

- (void)setupFooterControls
{
    //hide these items by default
    self.switchResponseTypeBBI.hidden = YES;
    self.switchResponseLabel.hidden = YES;
    
    if (![self.jobPosting isSubmitted])
    {
        //show response type switcher
        self.switchResponseTypeBBI.hidden = NO;
        self.switchResponseLabel.hidden = NO;
    }
    
    //check if job expired
    if ([self.jobPosting isExpired])
    {
        //hide switch icon
        self.switchResponseTypeBBI.hidden = YES;
        self.switchResponseLabel.hidden = YES;
    }
    
    //check if job expired
    if ([self.jobPosting.questions count] == 1)
    {
        //hide prev next icons
        self.prevQuestBBI.hidden = YES;
        self.prevQuestLabel.hidden  = YES;
        self.nextQuestBBI.hidden = YES;
        self.nextQuestLabel.hidden = YES;
    }
    else
    {
        long currIdx = [ASCJobPostingDetailsService sharedInstance].selectedQuestionIndex;
        
        //if at first question?
        if (currIdx == 0)
        {
            //no prev
            self.prevQuestBBI.hidden = YES;
            self.prevQuestLabel.hidden  = YES;
            self.nextQuestBBI.hidden = NO;
            self.nextQuestLabel.hidden = NO;
        }
        else if (currIdx == self.jobPosting.questions.count-1)
        {
            //if at last question?
            self.nextQuestBBI.hidden = YES;
            self.nextQuestLabel.hidden = YES;
            self.prevQuestBBI.hidden = NO;
            self.prevQuestLabel.hidden  = NO;
        }
        else
        {   //somewhere in the middle
            self.prevQuestBBI.hidden = NO;
            self.prevQuestLabel.hidden  = NO;
            self.nextQuestBBI.hidden = NO;
            self.nextQuestLabel.hidden = NO;
        }
    }
}

- (IBAction)goBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - video response delegate
- (void)videoPlayTapped
{
    [self performSegueWithIdentifier:@"showVideo" sender:self];
}

- (void)videoRecordTapped
{
    [self startRecorder:nil];
}

#pragma mark - text response delegate
-(void)responseTextViewTapped:(UITextView *)aView
{
    CGPoint pointInTable = [aView.superview convertPoint:aView.frame.origin toView:self.mainTableView];
    CGPoint contentOffset = self.mainTableView.contentOffset;
    
    contentOffset.y = (pointInTable.y - aView.inputAccessoryView.frame.size.height - 60);
    
//    NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
    
    [self.mainTableView setContentOffset:contentOffset animated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showVideo"])
    {
        ASCJobQuestionResponseVideoPlayerViewController *c8VideoPlayerVC = (ASCJobQuestionResponseVideoPlayerViewController *)segue.destinationViewController;
        c8VideoPlayerVC.movieURL = [self prepareMovieURL];
    }
}

- (NSURL *)prepareMovieURL
{
    NSURL *movieURL = nil;

    ASCJobPosting *selJob = [ASCJobPostingDetailsService sharedInstance].selectedJob;
    ASCJobPostingQuestion *selQuest = [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion;
    
    if (selQuest.answer && [selQuest.answer isCaptured])
    {
        NSString *qansmovieName =  [[[NSURL URLWithString:selQuest.answer.url] pathComponents] lastObject];
            
        movieURL = [[ASCS3Manager sharedInstance] getPreSignedURLWithBucketItemName:qansmovieName];
    }
    else
    {
        //check to see if this question was earlier opted out by candidate
        //all local, candidate hasn't submitted the answers yet
        NSString *moviePathString = [[ASCJobSubmissionService sharedInstance] localPathToMovieForQuestion:selQuest onJob:selJob];
        movieURL = [self localPathToMovie:moviePathString];
        DLog(@"local movie URL: %@", moviePathString);
    }
    return movieURL;
}

- (NSURL *)localPathToMovie:(NSString *)movieUrl
{
    BOOL movieFileExists = [[NSFileManager defaultManager] fileExistsAtPath:movieUrl];
    if (movieFileExists)
    {
        return  [NSURL URLWithString:[NSString stringWithFormat:@"file://%@",movieUrl]];
    }
    return nil;
}

#pragma mark - response switching stuff

- (IBAction)showSwitchResponseSheet:(id)sender {
    
    if ([self.jobPosting isSubmitted]) {
        return; //shouldn't be here at all
    }
    
    ASCJobPostingQuestion *selQuest = [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion;
    
    ASCResponseMode respMode = [selQuest.answer getResponseMode];
    NSString *actionTitle = NSLocalizedString(@"job.response.SWITCH_TITLE", nil);
    
    switch (respMode) {
        
        case ASCResponseModeWrite:
        {
            NSString *title1 = NSLocalizedString(@"job.response.OPTION_RECORD", nil);
            NSString *title2 = NSLocalizedString(@"job.response.OPTION_SKIP", nil);
            self.actionSheet = [[UIActionSheet alloc] initWithTitle:actionTitle delegate:self cancelButtonTitle:NSLocalizedString(@"c8.TITLE_CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:title1, title2, nil];
            self.actionSheet.tag = ASCResponseModeWrite;
            [self.actionSheet showInView:self.view];
            
        }
            
            break;
        case ASCResponseModeSkip:
        {
            NSString *title1 = NSLocalizedString(@"job.response.OPTION_RECORD", nil);
            NSString *title2 = NSLocalizedString(@"job.response.OPTION_WRITE", nil);
            self.actionSheet = [[UIActionSheet alloc] initWithTitle:actionTitle delegate:self cancelButtonTitle:NSLocalizedString(@"c8.TITLE_CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:title1, title2, nil];
            self.actionSheet.tag = ASCResponseModeSkip;
            [self.actionSheet showInView:self.view];
        }
            
            break;
            
        case ASCResponseModeRecord:
        default:
        {
            NSString *title1 = NSLocalizedString(@"job.response.OPTION_WRITE", nil);
            NSString *title2 = NSLocalizedString(@"job.response.OPTION_SKIP", nil);
            self.actionSheet = [[UIActionSheet alloc] initWithTitle:actionTitle delegate:self cancelButtonTitle:NSLocalizedString(@"c8.TITLE_CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:title1, title2, nil];
            self.actionSheet.tag = ASCResponseModeRecord;
            [self.actionSheet showInView:self.view];
            
        }

            break;
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 2)
    {
        return; //CANCEL
    }
    //save the selected response mode without any specific answer
    ASCJobPostingQuestion *selQuest = [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion;
    
    
    if (actionSheet.tag == ASCResponseModeRecord)
    {
        selQuest.answer.status = (buttonIndex == 0)? @(ASCResponseModeWrite) : @(ASCResponseModeSkip);
    }
    else if (actionSheet.tag == ASCResponseModeWrite)
    {
        selQuest.answer.status = (buttonIndex == 0)? @(ASCResponseModeRecord) : @(ASCResponseModeSkip);
    }
    else if (actionSheet.tag == ASCResponseModeSkip)
    {
        selQuest.answer.status = (buttonIndex == 0)? @(ASCResponseModeRecord) : @(ASCResponseModeWrite);
    }
    [[ASCJobSubmissionService sharedInstance] setLocalResponseDict:@{C8_RESPMODE_KEY:@([selQuest.answer getResponseMode])} forQuestion:selQuest onJob:self.jobPosting];
    
    [self configureWithDict:nil];
}

#pragma mark - show prev next
- (IBAction)showNextQuest:(id)sender
{
    long currIdx = [ASCJobPostingDetailsService sharedInstance].selectedQuestionIndex;
    long idx = currIdx+1;
    if (idx >= self.jobPosting.questions.count)
    {
        idx = 0;
    }
    [ASCJobPostingDetailsService sharedInstance].selectedQuestionIndex = idx;
    [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion = self.jobPosting.questions[idx];
    [self configureWithDict:nil];
}

- (IBAction)showPrevQuest:(id)sender
{
    long currIdx = [ASCJobPostingDetailsService sharedInstance].selectedQuestionIndex;
    long idx = currIdx-1;
    if (idx < 0)
    {
        idx = self.jobPosting.questions.count - 1;
    }
    [ASCJobPostingDetailsService sharedInstance].selectedQuestionIndex = idx;
    [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion = self.jobPosting.questions[idx];
    [self configureWithDict:nil];
}

-(IBAction)startRecorder:(id)sender
{
    [self performSegueWithIdentifier:@"responseRecorderSID" sender:self];
}


#pragma mark - table view data source and delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) // QUESTION CELL
    {
        return 80;
    }
    if (indexPath.row == 2)
    {
        return self.view.frame.size.height - 263;
    }
    return 70;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.row)
    {
        case 0:
            //header
            
            self.jobPosting = [ASCJobPostingDetailsService sharedInstance].selectedJob;
            cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell" forIndexPath:indexPath];
            [((ASCJobPostingViewCell *)cell) configureWithDict:@{@"JOB":self.jobPosting, @"micro": @(YES)}];
            break;
            
        case 1:
            //question
            cell = [tableView dequeueReusableCellWithIdentifier:@"questCell" forIndexPath:indexPath];
            [((ASCJobQuestionViewCell *)cell) configureWithDict:@{@"QUEST":[ASCJobPostingDetailsService sharedInstance].selectedJobQuestion, @"IDX": @([ASCJobPostingDetailsService sharedInstance].selectedQuestionIndex+1), @"OFIDX":@(YES), @"TXTEMTPYOKAY":@(YES)}];
            break;
            
        case 2:
            //response
            cell = [tableView dequeueReusableCellWithIdentifier:@"respCell" forIndexPath:indexPath];
            [((ASCResponseTableViewCell *)cell) configureWithDict:@{@"JOB":self.jobPosting, @"micro": @(YES), @"DELEGATE":self}];
            break;
            
        default:
            break;
    }
    return cell;
}

@end


@implementation UITableView (MyCoolExtension)

- (NSIndexPath *)indexPathForCellContainingView:(UIView *)view {
    while (view != nil) {
        if ([view isKindOfClass:[UITableViewCell class]]) {
            return [self indexPathForCell:(UITableViewCell *)view];
        } else {
            view = [view superview];
        }
    }
    
    return nil;
}

@end