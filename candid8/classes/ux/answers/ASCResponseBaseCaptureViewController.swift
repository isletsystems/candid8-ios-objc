//
//  ASCResponseBaseCaptureViewController.swift
//  candid8
//
//  Created by Aldrin ML on 2/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

import UIKit

class ASCResponseBaseCaptureViewController : ASCBaseViewController {
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var questionTitleLabel: UILabel!
    @IBOutlet weak var questionIndexContainerView: UIView!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var prevButton: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    weak var currentViewController: UIViewController?
    
    var selectedQuestionIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "hideKeyboard"))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "doSwitchToVideoResponse:", name: kASCDoVideoResponse, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "doSwitchToWrittenResponse:", name: kASCDoWrittenResponse, object: nil)
        updateContent()
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func hideKeyboard() {
        NSNotificationCenter.defaultCenter().postNotificationName("hideKeyboard", object: nil)
    }
    
    func updateContent() {
        selectedQuestionIndex = ASCJobPostingDetailsService.sharedInstance().selectedQuestionIndex
        if let selQuestion = ASCJobPostingDetailsService.sharedInstance().selectedJobQuestion {
            questionTitleLabel?.text = selQuestion.title
            
            var isVideo = true
            //check if it has been already answered and if so what type it is
            //based on that load the corresponding view controller.
            if !selQuestion.isAttempted() {
                //video view is the default one.
                if selQuestion.answer != nil && selQuestion.answer.answerType.integerValue == ASCResponseModeWrite.rawValue {
                    isVideo = false
                }
            } else {
                if selQuestion.answer.answerType.integerValue == ASCResponseModeWrite.rawValue {
                    isVideo = false
                }
            }
            if isVideo {
                headerTitleLabel?.text = NSLocalizedString("job.response.header.record.title", comment: "")
                
                let newViewController = self.storyboard?.instantiateViewControllerWithIdentifier("videoAnswerScene")
                newViewController!.view.translatesAutoresizingMaskIntoConstraints = false
                self.cycleFromViewController(self.currentViewController, toViewController: newViewController!)
                self.currentViewController = newViewController
            } else {
                
                headerTitleLabel?.text = NSLocalizedString("job.response.header.write.title", comment: "")
                let newViewController = self.storyboard?.instantiateViewControllerWithIdentifier("textAnswerScene")
                newViewController!.view.translatesAutoresizingMaskIntoConstraints = false
                self.cycleFromViewController(self.currentViewController, toViewController: newViewController!)
                self.currentViewController = newViewController
            }
        }
        renderJobQAIndicatorContent(self.questionIndexContainerView)
    }
    
    //http://spin.atomicobject.com/2015/10/13/switching-child-view-controllers-ios-auto-layout/
    func cycleFromViewController(oldViewController: UIViewController?, toViewController newViewController: UIViewController) {
        oldViewController?.willMoveToParentViewController(nil)
        self.addChildViewController(newViewController)
        self.addSubview(newViewController.view, toView:self.containerView!)
        newViewController.view.alpha = 0
        newViewController.view.layoutIfNeeded()
        UIView.animateWithDuration(0.5, animations: {
            newViewController.view.alpha = 1
            oldViewController?.view.alpha = 0
            },
            completion: { finished in
                oldViewController?.view.removeFromSuperview()
                oldViewController?.removeFromParentViewController()
                newViewController.didMoveToParentViewController(self)
        })
    }
    
    func addSubview(subView:UIView, toView parentView:UIView) {
        parentView.addSubview(subView)
        
        var viewBindingsDict = [String: AnyObject]()
        viewBindingsDict["subView"] = subView
        parentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[subView]|",
            options: [], metrics: nil, views: viewBindingsDict))
        parentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[subView]|",
            options: [], metrics: nil, views: viewBindingsDict))
    }
    
    func doSwitchToVideoResponse(notif:NSNotification) {
        
        let alertView = SCLFlatAlertView()
        let yesBtn = alertView.addButton(NSLocalizedString("c8.TITLE_YES", comment: "")) {
            
            if let selQuestion = ASCJobPostingDetailsService.sharedInstance().selectedJobQuestion {
                //TODO, mark this question as type video?
                if !selQuestion.isAttempted() {
                    selQuestion.answer = ASCJobPostingResponse()
                }
                selQuestion.answer.answerType = ASCResponseModeRecord.rawValue
                self.updateContent()
            }
        }
        alertView.addButton(NSLocalizedString("c8.TITLE_NO", comment: "")) {
        }
        alertView.showNotice(NSLocalizedString("job.response.SWITCH_INFO", comment: ""),
            subTitle: NSLocalizedString("job.response.SWITCH_TO_VIDEO", comment: ""))
        yesBtn.backgroundColor = UIColor(string:kC8ThemeColor)
    }
    
    func doSwitchToWrittenResponse(notif:NSNotification) {
        
        let alertView = SCLFlatAlertView()
        let yesBtn = alertView.addButton(NSLocalizedString("c8.TITLE_YES", comment: "")) {
            
            if let selQuestion = ASCJobPostingDetailsService.sharedInstance().selectedJobQuestion {
                if !selQuestion.isAttempted() {
                    selQuestion.answer = ASCJobPostingResponse()
                }
                selQuestion.answer.answerType = ASCResponseModeWrite.rawValue
                self.updateContent()
            }
        }
        alertView.addButton(NSLocalizedString("c8.TITLE_NO", comment: "")) {
        }
        alertView.showNotice(NSLocalizedString("job.response.SWITCH_INFO", comment: ""),
            subTitle: NSLocalizedString("job.response.SWITCH_TO_WRITE", comment: ""))
        yesBtn.backgroundColor = UIColor(string:kC8ThemeColor)
    }
    
    func renderJobQAIndicatorContent(containerView:UIView){
        
        let currJob = ASCJobPostingDetailsService.sharedInstance().selectedJob
        let qCount = currJob.questions.count
        let separatorWidth:CGFloat = 2.0
        
        prevButton.hidden = selectedQuestionIndex == 0
        nextButton.hidden = selectedQuestionIndex == qCount-1
        
        for vu in containerView.subviews {
            vu.removeFromSuperview()
        }
        
        if (qCount == 0) {
            return
        }
        let maxSepratorWidth : CGFloat = separatorWidth * (CGFloat)(qCount - 1)
        let maxItemWidth = containerView.frame.size.width - maxSepratorWidth
        let qWidth = maxItemWidth/CGFloat(qCount)
        
        var idx = 0
        for quest in currJob.questions {
            
            var isAttempted = false
            if let qestn = quest as? ASCJobPostingQuestion, let ans = qestn.answer {
                isAttempted = ans.published
            }
            if !isAttempted {
                if (ASCJobSubmissionService.sharedInstance().hasAttemptedLocallyQuestion(quest as! ASCJobPostingQuestion, onJob:currJob)) {
                    isAttempted = true;
                }
            }
            let nextX = (qWidth+separatorWidth) * CGFloat(idx)
            var y : CGFloat = 0
            var qHeight = containerView.frame.size.height - 4
            if idx != selectedQuestionIndex || qCount == 1 { //for single questions show half height
                y = qHeight / CGFloat(4)
                qHeight = qHeight/2
            }
            let qVu = UIView(frame: CGRectMake(nextX, y, qWidth, qHeight))
            qVu.backgroundColor = isAttempted ? UIColor(string: "#54bfe3") : UIColor.lightGrayColor()
            containerView.addSubview(qVu)
            idx++
        }
    }
    
    @IBAction func showNextQuestion(sender: AnyObject) {
        let questions = ASCJobPostingDetailsService.sharedInstance().selectedJob.questions
        
        if (selectedQuestionIndex + 1) >= questions.count {
            return
        }
        selectedQuestionIndex++
        ASCJobPostingDetailsService.sharedInstance().selectedQuestionIndex = selectedQuestionIndex;
        
        if let selQuestion = questions[selectedQuestionIndex] as? ASCJobPostingQuestion {
            ASCJobPostingDetailsService.sharedInstance().selectedJobQuestion = selQuestion
        }
        updateContent()
    }
    
    @IBAction func showPreviousQuestion(sender: AnyObject) {
        if (selectedQuestionIndex - 1) < 0 {
            return
        }
        
        let questions = ASCJobPostingDetailsService.sharedInstance().selectedJob.questions
        selectedQuestionIndex--
        ASCJobPostingDetailsService.sharedInstance().selectedQuestionIndex = selectedQuestionIndex;
        if let selQuestion = questions[selectedQuestionIndex] as? ASCJobPostingQuestion {
            ASCJobPostingDetailsService.sharedInstance().selectedJobQuestion = selQuestion
        }
        updateContent()
    }

}
