//
//  ASCRecordedResponseView.m
//  candid8
//
//  Created by Aldrin Lenny on 24/06/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCRecordedResponseView.h"
#import "ASCJobPostingDetailsService.h"
#import "ASCJobSubmissionService.h"
#import "ASCS3Manager.h"
#import "AppConfigs.h"
#import "UIView+PREBorderView.h"
#import <ColorUtils/ColorUtils.h>

#import <MBProgressHUD/MBProgressHUD.h>
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface ASCRecordedResponseView()

@property (nonatomic, strong) UINib *nib;
@property (nonatomic, weak) IBOutlet UIImageView *stillImageView;

@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UIView *rerecordContainerView;

@property (weak, nonatomic) IBOutlet UIView *rerecordView;
@property (weak, nonatomic) IBOutlet UIView *recordOnlyView;
@property (weak, nonatomic) IBOutlet UIView *playOnlyView;

@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UILabel *playVideoLabel;
@property (weak, nonatomic) IBOutlet UILabel *playVideoLabel2;

@property (weak, nonatomic) IBOutlet UIButton *rerecordVideoButton;
@property (weak, nonatomic) IBOutlet UILabel *rerecordVideoLabel;

@property (weak, nonatomic) IBOutlet UIButton *recordVideoButton;
@property (weak, nonatomic) IBOutlet UILabel *recordVideoLabel;

- (IBAction)playVideo:(id)sender;
- (IBAction)recordVideo:(id)sender;

@end

@implementation ASCRecordedResponseView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self)
    {
        if (!self.nib)
        {
            self.nib = [UINib nibWithNibName:NSStringFromClass([ASCRecordedResponseView class]) bundle:nil];
        }
        NSArray *nib = [self.nib instantiateWithOwner:self options:nil];
        self = nib[0];
        self.frame = CGRectMake(0,0,frame.size.width, frame.size.height);
        self.playVideoLabel.text = NSLocalizedString(@"record.PLAY_LABEL_TITLE", nil);
        self.playVideoLabel2.text = NSLocalizedString(@"record.PLAY_LABEL_TITLE", nil);
        self.rerecordVideoLabel.text = NSLocalizedString(@"record.RERECORD_LABEL_TITLE", nil);
        self.recordVideoLabel.text = NSLocalizedString(@"record.RECORD_LABEL_TITLE", nil);
    }
    return self;
}


-(void)configureWithDict:(NSDictionary *)aDict
{
    //check if there is any video existing
    ASCJobPostingQuestion *selQuest = [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion;
    ASCJobPosting *selJob = [ASCJobPostingDetailsService sharedInstance].selectedJob;
    self.rerecordContainerView.hidden = YES;
    if ([selJob isSubmitted])
    {
        self.playOnlyView.hidden = NO;
        self.rerecordContainerView.hidden = NO;
        self.recordOnlyView.hidden = YES;
        self.rerecordView.hidden = YES;
    }
    else if (selQuest.answer.videoLocalUrl)
    {
        self.playOnlyView.hidden = YES;
        self.recordOnlyView.hidden = YES;
        
        [self.separatorView addLineWithColor:[UIColor colorWithString:kC8LineColor] andWidth:0.5 atPosition:PREBorderPositionLeft];
        
        if ([selJob isExpired])
        {
            self.rerecordView.hidden = YES;
        }
        else
        {
            self.rerecordView.hidden = NO;
            self.rerecordContainerView.hidden = NO;
        }
    }
    else
    {
        self.playOnlyView.hidden = YES;
        if ([selJob isExpired])
        {
            self.recordOnlyView.hidden = YES;
        }
        else
        {
            self.recordOnlyView.hidden = NO;
            self.rerecordContainerView.hidden = YES;
        }
        self.rerecordView.hidden = YES;
        return;
    }
    //check if there is any existing still image
    NSURL *stillImageURL = [self prepareStillImageURL];
    if (stillImageURL)
    {
        [self showStillImageWithURL:stillImageURL];
    }
}

- (NSURL *)prepareStillImageURL
{
    NSURL *stillImageURL = nil;
    ASCJobPostingQuestion *selQuest = [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion;
    
    if (selQuest.answer && [selQuest.answer isCaptured] && !selQuest.answer.videoLocalUrl)
    {
        //S3 side video
        NSString *qansmovieName = [[[NSURL URLWithString:selQuest.answer.url] pathComponents] lastObject];
        
        //load the corresponding still image from url
        NSString *s3StillImageKey = [qansmovieName stringByReplacingOccurrencesOfString:C8_VIDEO_TYPE_EXTENSION withString:C8_IMAGE_TYPE_EXTENSION];
        stillImageURL = [[ASCS3Manager sharedInstance] getPreSignedURLWithBucketItemName:s3StillImageKey];
    }
    else
    {
        //check if there is any video captured aleready for this one
        NSString *moviePathString = [[ASCJobSubmissionService sharedInstance] localPathToMovieForQuestion:selQuest onJob:[ASCJobPostingDetailsService sharedInstance].selectedJob];
        BOOL movieFileExists = [[NSFileManager defaultManager] fileExistsAtPath:moviePathString];
        if (movieFileExists)
        {
            NSString *localImageFileName =
            [moviePathString stringByReplacingOccurrencesOfString:C8_VIDEO_TYPE_EXTENSION withString:C8_IMAGE_TYPE_EXTENSION];
            stillImageURL = [NSURL URLWithString:[NSString stringWithFormat:@"file://%@", localImageFileName]];
        }
    }
    DLog(@"preparedStillImageURL: %@", stillImageURL);
    return stillImageURL;
}


- (void)showStillImageWithURL:(NSURL *)aURL
{
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    //had to do this to clear the images
    [[SDImageCache sharedImageCache] clearDisk];
    [[SDImageCache sharedImageCache] clearMemory];
    
    __weak __typeof__(self) weakSelf = self;
    [self.stillImageView sd_setImageWithURL:aURL completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        __strong __typeof__(self) strongSelf = weakSelf;
        [MBProgressHUD hideAllHUDsForView:strongSelf animated:YES];
    }];
}

#pragma mark - play record actions
- (void)playTapped:(UIGestureRecognizer *)aGesture
{
    [self playVideo:nil];
}


- (IBAction)playVideo:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(videoPlayTapped)])
    {
        [self.delegate videoPlayTapped];
    }
}

- (IBAction)recordVideo:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(videoRecordTapped)])
    {
        [self.delegate videoRecordTapped];
    }
}


@end
