//
//  ASCResponseVideoRecordViewController.swift
//  candid8
//
//  Created by Aldrin ML on 1/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

import UIKit

class ASCResponseVideoRecordViewController: ASCBaseViewController {
    
    let kC8_VIDEO_TYPE = "video/mp4"
    let kC8_IMAGE_TYPE = "image/jpeg"
    let kC8_VIDEO_TYPE_EXTENSION = "mp4"
    let kC8_IMAGE_TYPE_EXTENSION = "jpg"
    
    let playVideoScene = "videoPlayerScene"
    let recordVideoScene = "videoRecorderScene"
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var seperatorView: UIView!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var rerecordLabel: UILabel!
    
    @IBOutlet weak var rerecordButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    
    @IBOutlet weak var stillImageView: UIImageView!
    @IBOutlet weak var switchToTextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let bgColor = UIColor(string: AppConfig.Theme.kC8BlueColor)
        titleView?.backgroundColor = bgColor
        let sepColor = UIColor(string: AppConfig.Theme.kC8LightBlueColor)
        seperatorView?.backgroundColor = sepColor
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        updateContent()
    }
    
    func updateContent() {
        let selQ = ASCJobPostingDetailsService.sharedInstance().selectedJobQuestion
        
        if selQ.answer != nil && selQ.answer.isCaptured() {
            rerecordLabel.text = NSLocalizedString("record.RERECORD_LABEL_TITLE", comment: "")
            playButton.enabled = true
            playButton.hidden = false
        } else {
            rerecordLabel.text = NSLocalizedString("record.RECORD_LABEL_TITLE", comment: "")
            playButton.enabled = false
            playButton.hidden = true
        }
        
        //check if job expired or submitted, then disable record button
        let selJob = ASCJobPostingDetailsService.sharedInstance().selectedJob
        if selJob.isExpired() || selJob.isSubmitted() {
            rerecordButton.enabled = false
            switchToTextButton?.enabled = false
        } else {
            rerecordButton.enabled = true
            switchToTextButton?.enabled = true
        }
        
        if let stillImageURL = self.prepareStillImageURL() {
            do {
                let imageData = try NSData(contentsOfURL: stillImageURL, options:NSDataReadingOptions())
                stillImageView.image = UIImage(data: imageData)
                playButton.enabled = true
            } catch let err {
                debugPrint(err)
            }
        }
    }
    
    @IBAction func playVideo(sender: AnyObject) {
        let scene = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier(playVideoScene)
        if let videoPlayerScene = scene as? ASCJobQuestionResponseVideoPlayerViewController {
            videoPlayerScene.movieURL = self.prepareMovieURL()
        }
        self.presentViewController(scene, animated: true) { () -> Void in
            //do nothing
        }
    }
    
    @IBAction func rerecordVideo(sender: AnyObject) {
        let scene = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier(recordVideoScene)
        self.presentViewController(scene, animated: true) { () -> Void in
            //do nothing
        }
    }
    
    @IBAction func switchResponseType(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName(kASCDoWrittenResponse, object: nil)
    }
    
    func prepareMovieURL() -> NSURL? {
        let selQ = ASCJobPostingDetailsService.sharedInstance().selectedJobQuestion
        if selQ.answer != nil && selQ.answer.isCaptured() {
            //https://c8webapp.s3-ap-southeast-2.amazonaws.com/emp/e105/job/j354/can/c12/c8_j239_q1431_vdo_ans.mp4
            if let qansmovieName = selQ.answer.url {
                debugPrint("vdoUrl path =\(qansmovieName)")
                let vdoUrl = ASCS3Manager.sharedInstance().getPreSignedURLWithBucketItemName(qansmovieName)
                debugPrint("vdoUrl to play = \(vdoUrl.absoluteString)")
                return vdoUrl
            }
        } else {
            //local movie to be returned
            //check if there is any video captured aleready for this one
            let moviePathString = ASCJobSubmissionService.sharedInstance().localPathToMovieForQuestion(selQ, onJob: ASCJobPostingDetailsService.sharedInstance().selectedJob)
            
            if ( NSFileManager.defaultManager().fileExistsAtPath(moviePathString)) {
                return NSURL(string: "file://\(moviePathString)")
            }
        }
        return nil
    }
    
    func prepareStillImageURL() -> NSURL? {
        var stillImageURL : NSURL?
        let selQ = ASCJobPostingDetailsService.sharedInstance().selectedJobQuestion
        if selQ.answer != nil && selQ.answer.isCaptured() && selQ.answer.videoLocalUrl == nil {
            //S3 link to be prepared
            
            //S3 side video
            //https://c8webapp.s3-ap-southeast-2.amazonaws.com/emp/e105/job/j354/can/c12/c8_j239_q1431_vdo_ans.mp4
            if let qansmovieName = selQ.answer.url {
                debugPrint("vdoUrl path =\(qansmovieName)")
                //load the corresponding still image from url
                let s3StillImageKey = qansmovieName.stringByReplacingOccurrencesOfString(kC8_VIDEO_TYPE_EXTENSION, withString: kC8_IMAGE_TYPE_EXTENSION)
                stillImageURL = ASCS3Manager.sharedInstance().getPreSignedURLWithBucketItemName(s3StillImageKey)
            }
        } else {
            //local movie if exists, return the still image generated from it 
            let moviePathString = ASCJobSubmissionService.sharedInstance().localPathToMovieForQuestion(selQ, onJob: ASCJobPostingDetailsService.sharedInstance().selectedJob)
            
            if (NSFileManager.defaultManager().fileExistsAtPath(moviePathString)) {
                let imagePathString = moviePathString.stringByReplacingOccurrencesOfString(kC8_VIDEO_TYPE_EXTENSION, withString: kC8_IMAGE_TYPE_EXTENSION)
                stillImageURL = NSURL(string: "file://\(imagePathString)")
            }
        }
        return stillImageURL
    }
}
