//
//  ASCJobQuestionView.m
//  candid8
//
//  Created by Aldrin Lenny on 11/09/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobQuestionView.h"

#import "UIView+PREBorderView.h"
#import <ColorUtils/ColorUtils.h>
#import "AppConfigs.h"
#import "ASCJobPostingDetailsService.h"
#import "ASCJobSubmissionService.h"

@interface ASCJobQuestionView()

@property (nonatomic, strong) UINib *nib;

@property (nonatomic, weak) IBOutlet UILabel *questionTitle;
@property (nonatomic, weak) IBOutlet UILabel *questionDescription;
@property (nonatomic, weak) IBOutlet UILabel *maxDuration;
@property (nonatomic, weak) IBOutlet UIImageView *statusIconImageView;
@property (weak, nonatomic) IBOutlet UIView *iconContainerView;

@end

@implementation ASCJobQuestionView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        if (!self.nib)
        {
            self.nib = [UINib nibWithNibName:NSStringFromClass([ASCJobQuestionView class]) bundle:nil];
        }
        NSArray *nib = [self.nib instantiateWithOwner:self options:nil];
        self = nib[0];
        self.frame = frame;
    }
    return self;
}

-(void)configureWithDict:(NSDictionary *)aDict
{
    ASCJobPostingQuestion *aJobQuestion = aDict[@"QUEST"];
    self.questionTitle.text = aJobQuestion.title;
    
    ASCResponseMode respMode = [aJobQuestion.answer getResponseMode];
    BOOL hasContent = NO;
    if ([[ASCJobSubmissionService sharedInstance] hasAttemptedLocallyQuestion:aJobQuestion onJob:[ASCJobPostingDetailsService sharedInstance].selectedJob]) {
        respMode = aJobQuestion.answer.answerType.integerValue;
    }
        
    if (respMode == ASCResponseModeRecord)
    {
        //check if there is any real video url, if not revert it back to pending
        if (aJobQuestion.answer.videoLocalUrl && ![aJobQuestion.answer.videoLocalUrl isEqualToString:@""])
        {
            self.statusIconImageView.image = [UIImage imageNamed:@"icon_recorded"];
            hasContent = YES;
        }
        else if (aJobQuestion.answer.url && ![aJobQuestion.answer.url isEqualToString:@""] && ![aJobQuestion.answer.url isEqualToString:@"n.a"])
        {
            self.statusIconImageView.image = [UIImage imageNamed:@"icon_recorded"];
            hasContent = YES;
        }
    }
    else if (respMode == ASCResponseModeWrite)
    {
        if (aJobQuestion.answer.writtenText && ![aJobQuestion.answer.writtenText isEqualToString:@""])
        {
            self.statusIconImageView.image = [UIImage imageNamed:@"icon_write"];
            hasContent = YES;
        }
        if (aDict[@"TXTEMTPYOKAY"]) {
            self.statusIconImageView.image = [UIImage imageNamed:@"icon_write"];
            hasContent = YES;
        }
    }
    else if (respMode == ASCResponseModeSkip)
    {
        hasContent = YES;
        self.statusIconImageView.image = [UIImage imageNamed:@"icon_skipped"];
    }
    
    
    if (!hasContent || respMode == ASCResponseModePending)
    {
        aJobQuestion.answer.status = @(ASCResponseModePending);
        self.statusIconImageView.image = nil;
        self.backgroundColor = [UIColor colorWithString:kC8JobQuestionUnasweredColor];
        self.questionTitle.textColor = [UIColor blackColor];
    }
    else
    {
        self.backgroundColor = [UIColor colorWithString:kC8JobQuestionAsweredColor];
        self.questionTitle.textColor = [UIColor whiteColor];
    }
}


@end
