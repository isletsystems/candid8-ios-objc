//
//  ASCBaseViewController.swift
//  candid8
//
//  Created by Aldrin ML on 2/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

import UIKit

class ASCBaseViewController: UIViewController {

    let jobSearchSceneId = "JobSearchScene"
    let kASCDoVideoResponse = "ASCDoVideoResponse"
    let kASCDoWrittenResponse = "ASCDoWrittenResponse"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    @IBAction func showJobSearch(sender: AnyObject) {
        if let jobSearchScene = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier(jobSearchSceneId) as? UIViewController {
            self.presentViewController(jobSearchScene, animated: true) { () -> Void in
                //do nothing
            }
        }
    }
    
    @IBAction func goBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func toggleSideMenu(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName("kASCShowSideMenuNotification", object: nil)
    }
    
}
