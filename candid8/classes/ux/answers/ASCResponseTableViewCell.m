//
//  ASCResponseTableViewCell.m
//  candid8
//
//  Created by Aldrin Lenny on 11/09/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCResponseTableViewCell.h"
#import "ASCJobPostingDetailsService.h"
#import "ASCWrittenResponseView.h"
#import "ASCRecordedResponseView.h"
#import "ASCOptedOutResponseView.h"

@implementation ASCResponseTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)configureWithDict:(NSDictionary *)aDict
{
    CGRect rect = self.frame;
    //remove children if any from response container view
    for (UIView *vu in [self.contentView subviews]) {
        [vu removeFromSuperview];
    }
    ASCJobPostingQuestion *selQuest = [ASCJobPostingDetailsService sharedInstance].selectedJobQuestion;
    ASCResponseMode respMode = [selQuest.answer getResponseMode];
    switch (respMode)
    {
        case ASCResponseModeWrite:
        {
            ASCWrittenResponseView *vu = [[ASCWrittenResponseView alloc] initWithFrame:rect];
            vu.delegate = aDict[@"DELEGATE"];
            [self.contentView addSubview:vu];
            [vu configureWithDict:nil];
        }
            break;
            
        case ASCResponseModeSkip:
        {
            ASCOptedOutResponseView *vu = [[ASCOptedOutResponseView alloc] initWithFrame:rect];
            [self.contentView addSubview:vu];
        }
            break;
            
        case ASCResponseModeRecord:
        default:
        {
            ASCRecordedResponseView *vu = [[ASCRecordedResponseView alloc] initWithFrame:rect];
            [self.contentView addSubview:vu];
            vu.delegate = aDict[@"DELEGATE"];
            [vu configureWithDict:nil];
        }
            break;
    }
}

@end
