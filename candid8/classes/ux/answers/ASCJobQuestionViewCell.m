//
//  ASCJobQuestionViewCell.m
//  candid8
//
//  Created by Aldrin Lenny on 17/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobQuestionViewCell.h"

#import "ASCJobQuestionView.h"
#import "AppConfigs.h"
#import <ColorUtils/ColorUtils.h>

@interface ASCJobQuestionViewCell()

@property (nonatomic, strong) ASCJobQuestionView *questionView;

@end

@implementation ASCJobQuestionViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)configureWithDict:(NSDictionary *)aDict
{
    self.contentView.backgroundColor = [UIColor colorWithString:kC8JobQuestionSeparatorColor];
    
    if (!self.questionView)
    {
        self.questionView = [[ASCJobQuestionView alloc] initWithFrame:CGRectInset(self.contentView.frame, 0, 0.4)];
        [self.contentView addSubview:self.questionView];
    }
    [self.questionView configureWithDict:aDict];
}


@end
