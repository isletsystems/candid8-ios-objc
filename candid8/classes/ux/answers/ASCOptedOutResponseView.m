//
//  ASCOptedOutResponseView.m
//  candid8
//
//  Created by Aldrin Lenny on 24/06/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCOptedOutResponseView.h"

#import "UIView+PREBorderView.h"
#import <ColorUtils/ColorUtils.h>

@interface ASCOptedOutResponseView()

@property (nonatomic, strong) UINib *nib;

@property (weak, nonatomic) IBOutlet UILabel *skippedLabel;

@end

@implementation ASCOptedOutResponseView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self)
    {
        if (!self.nib)
        {
            self.nib = [UINib nibWithNibName:NSStringFromClass([ASCOptedOutResponseView class]) bundle:nil];
        }
        NSArray *nib = [self.nib instantiateWithOwner:self options:nil];
        self = nib[0];
        self.frame = CGRectMake(0,0,frame.size.width, frame.size.height);
        
        self.skippedLabel.text = NSLocalizedString(@"job.response.SKIPPED", nil);
        
        [self.skippedLabel addLineWithColor:[UIColor redColor] andWidth:0.5 atPosition:PREBorderPositionTop];
        [self.skippedLabel addLineWithColor:[UIColor redColor] andWidth:0.5 atPosition:PREBorderPositionBottom];
        [self.skippedLabel addLineWithColor:[UIColor redColor] andWidth:0.5 atPosition:PREBorderPositionLeft];
        [self.skippedLabel addLineWithColor:[UIColor redColor] andWidth:0.5 atPosition:PREBorderPositionRight];
    }
    return self;
}

@end
