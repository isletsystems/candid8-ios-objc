//
//  ASCJobQuestionView.h
//  candid8
//
//  Created by Aldrin Lenny on 11/09/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASCJobQuestionView : UIView<IBaseConfigurableView>

@end
