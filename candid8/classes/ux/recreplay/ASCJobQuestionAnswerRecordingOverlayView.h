//
//  ASCJobQuestionAnswerRecordingOverlayView.h
//  candid8
//
//  Created by Aldrin Lenny on 11/02/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ASCVideoRecorderDelegate.h"
#import "ASCJobPostingQuestion.h"

@interface ASCJobQuestionAnswerRecordingOverlayView : UIView

- (void)configureWithJobPostingQuestion:(ASCJobPostingQuestion *)aQuestion andRecorderDelegate:(id<ASCVideoRecorderDelegate>)aRecorderDelegate;

@end
