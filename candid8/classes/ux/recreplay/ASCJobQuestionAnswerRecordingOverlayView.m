//
//  ASCJobQuestionAnswerRecordingOverlayView.m
//  candid8
//
//  Created by Aldrin Lenny on 11/02/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobQuestionAnswerRecordingOverlayView.h"

#import "SFCountdownView.h"
#import "SFRoundProgressCounterView.h"

#import "AppConfigs.h"
#import "UIView+PREBorderView.h"

#import "ASCJobPosting.h"
#import "ASCJobPostingQuestion.h"
#import "ASCVideoRecorderDelegate.h"
#import <ColorUtils/ColorUtils.h>

@interface ASCJobQuestionAnswerRecordingOverlayView()<SFRoundProgressCounterViewDelegate, SFCountdownViewDelegate>


@property (nonatomic, weak) IBOutlet UILabel *questionDescLabel;
@property (nonatomic, weak) IBOutlet UILabel *questionTimerCountdownLabel;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;
@property (nonatomic, weak) IBOutlet UIButton *toggleRecordingButton;

@property (nonatomic, strong) id<ASCVideoRecorderDelegate> recorderDelegate;
@property (nonatomic, strong) ASCJobPostingQuestion *jobQuestion;

@property (nonatomic, strong) UINib *nib;
@property (nonatomic, readwrite) NSInteger videoCountDownValue;
@property (nonatomic, readwrite) BOOL recordingStarted;
@property (nonatomic, readwrite) long elapsedTimeSinceRecordingStarted;
@property (weak, nonatomic) IBOutlet UIImageView *recordingIndicatorImageView;

@property (weak, nonatomic) IBOutlet UIView *preCountDownContainerView;
@property (weak, nonatomic) IBOutlet SFCountdownView *preCountDownView;

@property (weak, nonatomic) IBOutlet UIView *extraTimeCountDownContainerView;
@property (weak, nonatomic) IBOutlet SFCountdownView *extraTimeCountDownView;
@property (weak, nonatomic) IBOutlet SFRoundProgressCounterView *recordingCountDownView;
@property (weak, nonatomic) IBOutlet UIButton *toggleCameraButton;
@property (weak, nonatomic) IBOutlet UIButton *toggleOrientationButton;

- (IBAction)cancelAction:(id)sender;
- (IBAction)toggleRecordingAction:(id)sender;
- (IBAction)toggleOrientationAction:(id)sender;

@end

@implementation ASCJobQuestionAnswerRecordingOverlayView

- (id)initWithFrame:(CGRect)frame
{
	if (self = [super initWithFrame:frame])
    {
        if (!self.nib)
        {
            self.nib = [UINib nibWithNibName:@"ASCJobQuestionAnswerRecordingOverlayView" bundle:nil];
        }
        NSArray *views = [self.nib instantiateWithOwner:self options:nil];
		
		self = views[0];
        self.frame = frame;
        self.recordingStarted = NO;
        [self.toggleRecordingButton setTitle:NSLocalizedString(@"record.START_BTN_TITLE", nil)
                                    forState:UIControlStateNormal];
        [self.cancelButton setTitle:NSLocalizedString(@"c8.TITLE_CANCEL", nil)
                                    forState:UIControlStateNormal];
    }
    return self;
}

- (IBAction)toggleRecordingAction:(id)sender {
    if (!self.recordingStarted)
    {
        self.recordingStarted = YES;
        [self doPreRecordingSetup];
    }
    else
    {
        [self completeCaptureSession];
    }
}

- (IBAction)toggleOrientationAction:(id)sender
{
    if ([self.recorderDelegate respondsToSelector:@selector(toggleCameraOrientation)])
    {
        [self.recorderDelegate toggleCameraOrientation];
    }
}

- (IBAction)cancelAction:(id)sender
{
    [self.preCountDownView stop];
    [self.recordingCountDownView stop];
    [self.recorderDelegate videoRecordingSessionCancelled];
}

- (void)configureWithJobPostingQuestion:(ASCJobPostingQuestion *)aQuestion andRecorderDelegate:(id<ASCVideoRecorderDelegate>)aRecorderDelegate
{
    
    self.recorderDelegate = aRecorderDelegate;
    self.jobQuestion = aQuestion;
    
    [self displayQuestionInfo];
}

- (void)displayQuestionInfo
{
    self.questionDescLabel.text = self.jobQuestion.title;
    self.questionTimerCountdownLabel.text = [NSString stringWithFormat:@"%ld", (long)self.jobQuestion.maxTime];
    //hide the prep view
    self.preCountDownContainerView.hidden = YES;
    self.extraTimeCountDownContainerView.hidden = YES;
    self.toggleCameraButton.hidden = NO;
    
    //show max duration plus the extra time
    long maxDuration = self.jobQuestion.maxTime * 1000; //to milliseconds
    self.recordingCountDownView.hideFraction = YES;
    self.recordingCountDownView.intervals = @[@(maxDuration), @(3000)]; //extra 3 seconds
}

- (void)completeCaptureSession
{
    self.recordingStarted = NO;
    [self.preCountDownView stop];
    [self.recordingCountDownView stop];
    [self.recorderDelegate videoRecordingSessionStopped];
}

- (void)doPreRecordingSetup
{
    //show the prep view
    self.preCountDownContainerView.hidden = NO;
    self.extraTimeCountDownContainerView.hidden = YES;
    self.toggleCameraButton.hidden = YES;
    
    self.toggleRecordingButton.enabled = NO;
    //start the prep timer
    self.preCountDownView.delegate = self;
    self.preCountDownView.countdownFrom = 3;
    self.preCountDownView.backgroundAlpha = 0.6;
    self.preCountDownView.finishText = NSLocalizedString(@"record.GO_LABEL_TITLE", nil);
    self.preCountDownView.countdownColor = [UIColor whiteColor];
    [self.preCountDownView updateAppearance];
    [self.preCountDownView start];
    
    [self configureRecordingCountDownView];
}

- (void)doExtraTimeSetup
{
    //show the prep view
    self.extraTimeCountDownContainerView.hidden = NO;
    
    self.extraTimeCountDownView.delegate = self;
    self.extraTimeCountDownView.countdownFrom = 3;
    self.extraTimeCountDownView.backgroundAlpha = 0.6;
    self.extraTimeCountDownView.finishText = NSLocalizedString(@"record.DONE_LABEL_TITLE", nil);
    self.extraTimeCountDownView.countdownColor = [UIColor whiteColor];
    [self.extraTimeCountDownView updateAppearance];
    [self.extraTimeCountDownView start];
}


- (void)configureRecordingCountDownView
{
    //now fire off the recording circular count down
    self.recordingCountDownView.delegate = self;
    
    // thickness of outer circle
    self.recordingCountDownView.outerCircleThickness = [NSNumber numberWithFloat:15.0f];
    
    // thickness of inner circle
    self.recordingCountDownView.innerCircleThickness = [NSNumber numberWithFloat:20.0f];
    
    // track color of outer circle
    self.recordingCountDownView.innerTrackColor = [UIColor colorWithString:kC8LineColor];
    
    // track color of inner circle
    self.recordingCountDownView.outerTrackColor = [UIColor blackColor];
    
    // distance between two circles (if multiple intervals)
    self.recordingCountDownView.circleDistance = [NSNumber numberWithFloat:5.0];
    
    // set color of outer progress circles
    self.recordingCountDownView.outerProgressColor = [UIColor colorWithString:kC8LineColor];
    
    // set color of inner progress circle
    self.recordingCountDownView.innerProgressColor = [UIColor blackColor];
    
    // set color of counter label
    self.recordingCountDownView.labelColor = [UIColor blackColor];
}

#pragma mark - SFCountdownViewDelegate method
- (void) countdownFinished:(SFCountdownView *)view
{
    if (view == self.preCountDownView)
    {
        self.preCountDownContainerView.hidden = YES;
        self.toggleRecordingButton.enabled = YES;
        
        [self.toggleRecordingButton setTitle:NSLocalizedString(@"record.STOP_BTN_TITLE", nil) forState:UIControlStateNormal];
        
        [self.recordingCountDownView start];
        
        //also start the recording
        [self.recorderDelegate videoRecordingSessionStarted];
    }
    else if (view == self.extraTimeCountDownView)
    {
        self.extraTimeCountDownContainerView.hidden = YES;
    }
}

#pragma mark - SFRoundProgressCounterViewDelegate methods
- (void)countdownDidEnd:(SFRoundProgressCounterView*)progressCounterView
{
    [self completeCaptureSession];
}

- (void)intervalDidEnd:(SFRoundProgressCounterView*)progressCounterView WithIntervalPosition:(int)position
{
    if (position == 1)
    {
        //normal time over, if its still actively recording, then
        //start the 5 seconds warning indicator
        [self doExtraTimeSetup];
    }
    else if (position == 2)
    {
        //extra time over.
    }
}

- (void)counter:(SFRoundProgressCounterView *)progressCounterView didReachValue:(unsigned long long)value
{
    self.elapsedTimeSinceRecordingStarted = value;
}

- (IBAction)toggleCameraSide:(id)sender
{
    if (self.recorderDelegate && [self.recorderDelegate respondsToSelector:@selector(toggleCameraSide)])
    {
        [self.recorderDelegate toggleCameraSide];
    }
}
@end
