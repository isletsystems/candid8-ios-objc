//
//  ASCJobQuestionResponseVideoPlayerView.m
//  candid8
//
//  Created by Aldrin Lenny on 15/02/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobQuestionResponseVideoPlayerView.h"


@interface ASCJobQuestionResponseVideoPlayerView()

@end

@implementation ASCJobQuestionResponseVideoPlayerView
+ (Class)layerClass {
    return [AVPlayerLayer class];
}
- (AVPlayer*)player {
    return [(AVPlayerLayer *)[self layer] player];
}
- (void)setPlayer:(AVPlayer *)player {
    [(AVPlayerLayer *)[self layer] setPlayer:player];
}
@end
