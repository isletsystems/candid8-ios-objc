//
//  ASCJobQuestionResponseVideoPlayerView.h
//  candid8
//
//  Created by Aldrin Lenny on 15/02/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ASCJobQuestionResponseVideoPlayerView : UIView

@property (nonatomic) AVPlayer *player;

@end

