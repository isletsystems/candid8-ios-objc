//
//  ASCJobQuestionResponseVideoPlayerViewController.h
//  candid8
//
//  Created by Aldrin Lenny on 13/04/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASCJobQuestionResponseVideoPlayerViewController : UIViewController

@property (nonatomic, strong) NSURL *movieURL;

@end
