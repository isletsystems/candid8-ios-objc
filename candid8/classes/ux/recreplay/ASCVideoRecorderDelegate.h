//
//  ASCVideoRecorderDelegate.h
//  candid8
//
//  Created by Aldrin Lenny on 12/02/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ASCVideoRecorderDelegate <NSObject>

- (void)videoRecordingSessionCancelled;
- (void)videoRecordingSessionTimerExpired;
- (void)videoRecordingSessionStarted;
- (void)videoRecordingSessionStopped;

@optional
- (void)toggleCameraSide;
- (void)toggleCameraOrientation;

@end
