//
//  ASCResumeTableViewCell.h
//  candid8
//
//  Created by islet on 09/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASCResume.h"
@class ASCResumeTableViewCell;
typedef void (^BtnActionBlockResume)(ASCResumeTableViewCell *cell);

@interface ASCResumeTableViewCell : UITableViewCell{
    BtnActionBlockResume btnClickHandle;
}
@property (strong, nonatomic) IBOutlet UILabel *lblResumeUploaded;
@property (strong, nonatomic) IBOutlet UIImageView *imgResume;
@property (nonatomic,strong)ASCResume * resume;
@property (strong, nonatomic) IBOutlet UILabel *lblFileName;
@property (strong, nonatomic) IBOutlet UILabel *lblFileTypeSize;
- (void)setResume:(ASCResume *)aResum andOnAction:(BtnActionBlockResume)ahandle;
@end
