//
//  ASCProfileViewController.m
//  candid8
//
//  Created by islet on 12/08/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "ASCProfileViewController.h"
#import <CTAssetsPickerController/CTAssetsPickerController.h>
#import "ASCCandidateProfileService.h"
#import "ASCLoginService.h"
#import <ColorUtils/ColorUtils.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "ASCResumeTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ASCCandidateResumeDeleteService.h"
#import "ASCCandidateSaveProfile.h"
#import "ASCCandidateChangePassword.h"
#import "ASCCandidateDelete.h"

#import "ASCCandidateProfileSubmissionService.h"
@import AssetsLibrary;

int const NumberOfSaticRows = 7;
@interface ASCProfileViewController ()<UITableViewDelegate,UITableViewDataSource,CTAssetsPickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UIImageView *profileBg;
@property (weak, nonatomic) IBOutlet UILabel *lblC8Name;
@property (weak, nonatomic) IBOutlet UILabel *lblCreatedDate;
@property (weak, nonatomic) IBOutlet UITableView *tblProfile;
@property (nonatomic,strong) NSArray * resumesArray;

@property (weak, nonatomic) IBOutlet UILabel *lblMyProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteProfile;

@property (weak, nonatomic) IBOutlet UIButton *btnTerms;
@property (weak, nonatomic) IBOutlet UIButton *btnPrivacy;
@property (weak, nonatomic) IBOutlet UIButton *btnChangePhoto;
@property (nonatomic,strong) ASCCandindate *candid8;
@property (nonatomic, assign) BOOL isViewDisAppearsForPhotoPicking;

@end

@implementation ASCProfileViewController


-(void)viewDidLoad {
    [super viewDidLoad];
    
    self.profilePic.layer.cornerRadius = self.profilePic.frame.size.width/2;
    self.profilePic.layer.masksToBounds = TRUE;
    self.profilePic.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.profilePic.layer.borderWidth = 2.0;
    
    self.lblMyProfile.backgroundColor = [UIColor colorWithString:kC8ThemeColor];
    self.lblMyProfile.text = NSLocalizedString(@"profile.MAIN_TITLE", @"My Profile");
    self.tblProfile.backgroundColor = [UIColor colorWithString:kC8LightGrayColor];
    self.btnDeleteProfile.backgroundColor = [UIColor colorWithString:kC8ThemeColor];
    self.btnTerms.tintColor =  [UIColor colorWithString:kC8ThemeColor];
    self.btnPrivacy.tintColor = [UIColor colorWithString:kC8ThemeColor];
    self.btnChangePhoto.tintColor = [UIColor colorWithString:kC8ThemeColor];
    _isViewDisAppearsForPhotoPicking = NO;
    [UIView setAnimationsEnabled:NO];
    [_btnChangePhoto setTitle:NSLocalizedString(@"profile.BTN_TITLE_CHANGE_PHOTO", @">Change photo") forState:UIControlStateNormal];
    [_btnDeleteProfile setTitle:NSLocalizedString(@"profile.BTN_DELETE_TITLE", @"DELETE MY ACCOUNT") forState:UIControlStateNormal];
    [UIView setAnimationsEnabled:YES];
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.profileBg.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        blurEffectView.alpha = 0.7;
        [self.profileBg addSubview:blurEffectView];
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!self.isViewDisAppearsForPhotoPicking) {
        self.candid8 = [ASCLoginService sharedInstance].loggedInCandidate;
        self.lblC8Name.text = self.candid8.displayName;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        __weak __typeof__(self) weakSelf = self;
        [[ASCCandidateProfileService sharedInstance] fetchProfileOfCandidate:self.candid8.cid withResponseBlock:^(BOOL isSuccess, id resultData) {
            __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
            [MBProgressHUD hideAllHUDsForView:strongSelf.view animated:NO];
            if (isSuccess) {
                strongSelf.candid8 = (ASCCandindate *)resultData;
                [strongSelf loadDetailsOfTheC8];
            } else {
                //TODO: alert error
            }
        }];
        
    }
}

#pragma mark - Button Actions
- (IBAction)btnDismissTouch:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (IBAction)btnShowShideMenu:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCShowSideMenuNotification" object:nil];
}

- (IBAction)btnActionChangePrfilePic:(id)sender {
    
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            // init picker
            CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
            
            picker.title = NSLocalizedString(@"Select Photo", @"");
            
            // set delegate
            picker.delegate = self;
            
            self.isViewDisAppearsForPhotoPicking = YES;
            
            // to present picker as a form sheet in iPad
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                picker.modalPresentationStyle = UIModalPresentationFormSheet;
            
            // present picker
            [self presentViewController:picker animated:YES completion:nil];
            
        });
    }];
}

- (IBAction)btnDeleteAccountTouch:(id)sender {
    __weak __typeof__(self) weakSelf = self;
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(@"profile.pwd.EDIT_DELETE_RESUME_TITLE",nil)
                                          message:[NSString stringWithFormat:NSLocalizedString(@"profile.del.MESSAGE", nil)]
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"c8.TITLE_YES", @"YES action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [MBProgressHUD showHUDAddedTo:weakSelf.view animated:YES];
                                   [[ASCCandidateDelete sharedInstance] deleteAccountForCandidate:self.candid8.cid withResponseBlock:^(BOOL isSuccess, id resultData) {
                                       __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
                                       [MBProgressHUD hideAllHUDsForView:strongSelf.view animated:NO];
                                       if (isSuccess) {
                                           [strongSelf showOkAlertWithTitle:NSLocalizedString(@"profile.del.RESPONSE_MESSAGE_TITLE",nil) withMessage:NSLocalizedString(@"profile.del.RESPONSE_MESSAGE_SUCESS_BODY",nil) onOKClick:^{
                                               [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCLogoutNotification" object:nil];
                                           }];
                                           
                                           
                                       }else{
                                           [strongSelf showOkAlertWithTitle:NSLocalizedString(@"profile.del.RESPONSE_MESSAGE_TITLE", nil) withMessage:NSLocalizedString(@"profile.del.RESPONSE_MESSAGE_FAIL_BODY",nil) onOKClick:^{
                                               
                                           }];
                                       }
                                   }];
                                   
                                   
                               }];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"c8.TITLE_NO", @"NO action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                       
                                   }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
    
    NSLog(@"remove clicked");
    
}

- (IBAction)btnTermsOfUseTouch:(id)sender {
}

- (IBAction)btnPrivacyPolicyTouch:(UIButton *)sender {
}

#pragma mark - Local Methods
- (void)loadDetailsOfTheC8{
    self.lblC8Name.text = self.candid8.displayName;
    
    self.resumesArray = self.candid8.resumes;
    NSDateFormatter * adateFormatter = [[NSDateFormatter alloc] init];
    [adateFormatter setDateFormat:kC8_DATE_FORMAT];
    NSDate * createdDate = [adateFormatter dateFromString:self.candid8.createdAtStr];
    if (createdDate) {
        [adateFormatter setDateFormat:NSLocalizedString(@"profile.CREATED_DATE_FORMAT", nil)];
        self.lblCreatedDate.text = [adateFormatter stringFromDate:createdDate];
    }
    [self.tblProfile reloadData];
    
    [[ASCS3Manager sharedInstance] loadImagefromS3Key:self.candid8.photo withCompletion:^(BOOL isSuccess, id resultData) {
        if (isSuccess) {
            self.profilePic.image = (UIImage *)resultData;
            self.profileBg.image = (UIImage *)resultData;
        }
    }];
}

typedef void (^TextInputComplete)(BOOL isCancel,id userData);

- (void)getDetailForCell:(ASCProfileDetailEditCell *)editCell onClicking:(TextInputComplete)handle{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(@"Profile", nil)
                                          message:[NSString stringWithFormat:@"Enter %@",editCell.lblTitle.text]
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    BOOL isPasswordAlert = [editCell.lblTitle.text isEqualToString:NSLocalizedString(@"profile.pwd.TITLE", @"")];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.text = editCell.lblDetail.text;
         
         textField.tag = 100;
         if (isPasswordAlert) {
             textField.placeholder = NSLocalizedString(@"profile.pwd.EDIT_OLD_PASSWORD", nil);
             textField.text = nil;
             textField.secureTextEntry = YES;
         }
     }];
    if (isPasswordAlert) {
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
         {
             textField.text = nil;
             textField.tag = 200;
             textField.placeholder = NSLocalizedString(@"profile.pwd.EDIT_NEW_PASSWORD", nil);
             textField.secureTextEntry = YES;
         }];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
         {
             textField.text = nil;
             textField.tag = 300;
             textField.placeholder = NSLocalizedString(@"profile.pwd.EDIT_CONFIRM_PASSWORD", nil);
             textField.secureTextEntry = YES;
         }];
    }
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"c8.TITLE_OK", nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   UITextField *login = alertController.textFields.firstObject;
                                   editCell.lblDetail.text = login.text;
                                   if (isPasswordAlert) {
                                       NSMutableDictionary * passDic = [@{} mutableCopy];
                                       for (UITextField * txtFild in alertController.textFields) {
                                           if (txtFild.text.length > 0) {
                                               passDic[txtFild.placeholder] = txtFild.text;
                                           }
                                           
                                       }
                                       editCell.lblDetail.text = @"******";
                                       
                                       handle(NO,passDic);
                                   }else{
                                       handle(NO,login.text);
                                   }
                                   
                               }];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"c8.TITLE_CANCEL", nil)
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       handle(YES,nil);
                                   }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)updateCandidat8Details{
    __weak __typeof__(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:weakSelf.view animated:YES];
    [[ASCCandidateSaveProfile sharedInstance] pushDetailsOfCandidate:[ASCLoginService sharedInstance].loggedInCandidate withResponseBlock:^(BOOL isSuccess, id resultData) {
        
        __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
        [MBProgressHUD hideAllHUDsForView:strongSelf.view animated:NO];
        if (isSuccess) {
        }
        
    }];
}

- (void)changePassword:(NSString *)oldPassword newPassword:(NSString *)newPass{
    __weak __typeof__(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:weakSelf.view animated:YES];
    [[ASCCandidateChangePassword sharedInstance] updatePasswordWithOldPassword:oldPassword withNewPassword:newPass withResponseBlock:^(BOOL isSuccess, id resultData) {
        __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
        [MBProgressHUD hideAllHUDsForView:strongSelf.view animated:NO];
        if (isSuccess) {
            [strongSelf showOkAlertWithTitle:nil withMessage:NSLocalizedString(@"profile.pwd.MESSAGE_PASSWORD_CHANGE_SUCESS",@"Password Change Sucess!!") onOKClick:nil];
        }else{
            NSDictionary * adic = [ASCCandidateChangePassword sharedInstance].errorResponseDictionary;
            [strongSelf showOkAlertWithTitle:nil withMessage:adic[@"errorMsg"] onOKClick:nil];
        }
    }];
}

- (void)deleteResumeForCell:(ASCResumeTableViewCell *)resumeCell{
    NSString * resumeId = resumeCell.resume.rid;
    __weak __typeof__(self) weakSelf = self;
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:NSLocalizedString(@"profile.pwd.EDIT_DELETE_RESUME_TITLE", nil)
                                          message:[NSString stringWithFormat:NSLocalizedString(@"profile.pwd.EDIT_DELETE_RESUME_MESSAGE", nil)]
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"c8.TITLE_YES", nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [MBProgressHUD showHUDAddedTo:weakSelf.view animated:YES];
                                   [[ASCCandidateResumeDeleteService sharedInstance] deleteResumeWith:resumeId withResponseBlock:^(BOOL isSuccess, id resultData) {
                                       __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
                                       [MBProgressHUD hideAllHUDsForView:strongSelf.view animated:NO];
                                       if (isSuccess) {
                                           
                                           NSMutableArray * amutableArr = [strongSelf.resumesArray mutableCopy];
                                           NSInteger index =0;
                                           for (ASCResume * resumes in strongSelf.resumesArray) {
                                               if ([resumeId isEqualToString:resumes.rid]) {
                                                   break;
                                               }
                                               index++;
                                           }
                                           [amutableArr removeObjectAtIndex:index];
                                           strongSelf.resumesArray = amutableArr;
                                           [strongSelf.tblProfile deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index+NumberOfSaticRows inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
                                           
                                       }
                                   }];
                                   
                                   
                               }];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"c8.TITLE_NO", nil)
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                       
                                   }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
    DLog(@"remove clicked");
}

typedef void (^TextOKClicked)();


- (void)showOkAlertWithTitle:(NSString *)title withMessage:(NSString *)message onOKClick:(TextOKClicked)handle{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"c8.TITLE_OK", @"OK action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       if (handle) {
                                           handle();
                                       }
                                   }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource



- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return NumberOfSaticRows+_resumesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.row<NumberOfSaticRows) {
        switch (indexPath.row) {
            case 0:
            {
                ASCMainHeadingCell * mainHead = (ASCMainHeadingCell *) [tableView dequeueReusableCellWithIdentifier:@"ProfileMainHeading" forIndexPath:indexPath];
                mainHead.lblHeading.text = NSLocalizedString(@"profile.DETAILS_TITLE", nil);
                cell = mainHead;
                cell.backgroundColor = [UIColor lightGrayColor];
            }
                break;
            case 1:
            {
                ASCProfileDetailEditCell * detailHead = (ASCProfileDetailEditCell *) [tableView dequeueReusableCellWithIdentifier:@"ProfileDetailEdit" forIndexPath:indexPath];
                
                detailHead.lblTitle.text = NSLocalizedString(@"profile.FIRST_NAME_TITLE", nil);
                detailHead.lblDetail.text = self.candid8.firstName;
                [detailHead.btnEdit setHidden:NO];
                [detailHead.btnEdit setTitle:NSLocalizedString(@"profile.BTN_EDIT_TITLE", nil) forState:UIControlStateNormal];
                __weak __typeof__(self) weakSelf = self;
                [detailHead setblock:^(ASCProfileDetailEditCell * __nonnull editCell) {
                    
                    __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
                    [strongSelf getDetailForCell:editCell onClicking:^(BOOL isCancel, NSString *newText) {
                        if (!isCancel) {
                            if (![weakSelf.candid8.firstName isEqual:newText]) {
                                weakSelf.candid8.firstName = newText;
                                [strongSelf updateCandidat8Details];
                            }
                            
                        }
                    }];
                    
                }];
                cell = detailHead;
                
            }
                break;
            case 2:
            {
                ASCProfileDetailEditCell * detailHead = (ASCProfileDetailEditCell *) [tableView dequeueReusableCellWithIdentifier:@"ProfileDetailEdit" forIndexPath:indexPath];
                
                detailHead.lblTitle.text = NSLocalizedString(@"profile.LAST_NAME_TITLE", nil);
                detailHead.lblDetail.text = self.candid8.lastName;
                [detailHead.btnEdit setHidden:NO];
                [detailHead.btnEdit setTitle:NSLocalizedString(@"profile.BTN_EDIT_TITLE", nil) forState:UIControlStateNormal];
                __weak __typeof__(self) weakSelf = self;
                [detailHead setblock:^(ASCProfileDetailEditCell * __nonnull editCell) {
                    
                    __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
                    [strongSelf getDetailForCell:editCell onClicking:^(BOOL isCancel, NSString *newText) {
                        if (!isCancel) {
                            if (![weakSelf.candid8.lastName isEqual:newText]) {
                                weakSelf.candid8.lastName = newText;
                                [strongSelf updateCandidat8Details];
                            }
                        }
                    }];
                    
                }];
                
                cell = detailHead;
            }
                break;
            case 3:
            {
                ASCProfileDetailEditCell * detailHead = (ASCProfileDetailEditCell *) [tableView dequeueReusableCellWithIdentifier:@"ProfileDetailEdit" forIndexPath:indexPath];
                
                detailHead.lblTitle.text = NSLocalizedString(@"profile.PHONE_NO_TITLE", nil);
                detailHead.lblDetail.text = self.candid8.phone;
                [detailHead.btnEdit setHidden:NO];
                [detailHead.btnEdit setTitle:NSLocalizedString(@"profile.BTN_EDIT_TITLE", @">Edit") forState:UIControlStateNormal];
                __weak __typeof__(self) weakSelf = self;
                [detailHead setblock:^(ASCProfileDetailEditCell * __nonnull editCell) {
                    
                    __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
                    [strongSelf getDetailForCell:editCell onClicking:^(BOOL isCancel, NSString *newText) {
                        if (!isCancel) {
                            if (![weakSelf.candid8.phone isEqual:newText]) {
                                weakSelf.candid8.phone = newText;
                                [strongSelf updateCandidat8Details];
                            }
                        }
                    }];
                    
                }];
                
                cell = detailHead;
            }
                break;
            case 4:
            {
                ASCProfileDetailEditCell * detailHead = (ASCProfileDetailEditCell *) [tableView dequeueReusableCellWithIdentifier:@"ProfileDetailEdit" forIndexPath:indexPath];
                
                detailHead.lblTitle.text = NSLocalizedString(@"profile.EMAIL_TITLE", nil);
                detailHead.lblDetail.text = self.candid8.email;
                [detailHead.btnEdit setHidden:YES];
                cell = detailHead;
            }
                break;
            case 5:
            {
                ASCProfileDetailEditCell * detailHead = (ASCProfileDetailEditCell *) [tableView dequeueReusableCellWithIdentifier:@"ProfileDetailEdit" forIndexPath:indexPath];
                
                detailHead.lblTitle.text =  NSLocalizedString(@"profile.pwd.TITLE", @"PASSWORD");
                detailHead.lblDetail.text = @"******";
                [detailHead.btnEdit setHidden:NO];
                [detailHead.btnEdit setTitle:NSLocalizedString(@"profile.BTN_PASSWORD_CHANGE_TITLE", @">Change") forState:UIControlStateNormal];
                __weak __typeof__(self) weakSelf = self;
                [detailHead setblock:^(ASCProfileDetailEditCell * __nonnull editCell) {
                    
                    __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
                    [strongSelf getDetailForCell:editCell onClicking:^(BOOL isCancel, NSDictionary *retDic) {
                        NSString * oldPassword = retDic[NSLocalizedString(@"profile.pwd.EDIT_OLD_PASSWORD", @"Old Password")];
                        NSString * newPassword = retDic[NSLocalizedString(@"profile.pwd.EDIT_NEW_PASSWORD", @"New Password")];
                        NSString * confirmPassword = retDic[NSLocalizedString(@"profile.pwd.EDIT_CONFIRM_PASSWORD", @"Confirm Password")];
                        if (oldPassword.length<1) {
                            [strongSelf showOkAlertWithTitle:NSLocalizedString(@"profile.pwd.MESSAGE_TITLE", @"Password Error!") withMessage:NSLocalizedString(@"profile.pwd.MESSAGE_ENTER_OLDPASSWORD", @"Please enter Old password") onOKClick:nil];
                        }else if (newPassword.length<1){
                            [strongSelf showOkAlertWithTitle:NSLocalizedString(@"profile.pwd.MESSAGE_TITLE", @"Password Error!") withMessage:NSLocalizedString(@"profile.pwd.MESSAGE_ENTER_NEWPASSWORD", @"Please enter a new password") onOKClick:nil];
                        }else if (confirmPassword.length<1){
                            [strongSelf showOkAlertWithTitle:NSLocalizedString(@"profile.pwd.MESSAGE_TITLE", @"Password Error!") withMessage:NSLocalizedString(@"profile.pwd.MESSAGE_PASSWORD_CONFIRM_FAIL", @"Password and confirm missmatch") onOKClick:nil];
                            
                        }else if ([newPassword isEqualToString:confirmPassword]){
                            [strongSelf changePassword:oldPassword newPassword:newPassword];
                            return ;
                            
                        }else{
                            [strongSelf showOkAlertWithTitle:NSLocalizedString(@"profile.pwd.MESSAGE_TITLE", @"Password Error!") withMessage:NSLocalizedString(@"profile.pwd.MESSAGE_PASSWORD_CONFIRM_FAIL", @"Password and confirm missmatch") onOKClick:nil];
                            
                        }
                        
                        detailHead.lblDetail.text = @"******";
                        NSLog(@"password dic %@",retDic);
                    }];
                    
                }];
                
                cell = detailHead;
            }
                break;
            case 6:
            {
                ASCMainHeadingCell * mainHead = (ASCMainHeadingCell *) [tableView dequeueReusableCellWithIdentifier:@"ProfileMainHeading" forIndexPath:indexPath];
                mainHead.lblHeading.text = NSLocalizedString(@"profile.RESUME_TITLE", @"My Rusumes");
                cell = mainHead;
                cell.backgroundColor = [UIColor lightGrayColor];
            }
                break;
            default:
                break;
        }
        
    }
    else
    {
        //profileResume
        ASCResumeTableViewCell * resumeCell = (ASCResumeTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"profileResume" forIndexPath:indexPath];
        cell = resumeCell;
        cell.backgroundColor = [UIColor colorWithString:kC8LightGrayColor];
        __weak __typeof__(self) weakSelf = self;
        [resumeCell setResume:self.resumesArray[indexPath.row-NumberOfSaticRows] andOnAction:^(ASCResumeTableViewCell *cell) {
            __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
            [strongSelf deleteResumeForCell:cell];
            
        }];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row<NumberOfSaticRows) {
        return 50;
    }else{
        return 150;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}


#pragma mark - CTAssetsPickerControllerDelegate
- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    if (assets.count<1) {
        return;
    }
    PHAsset * anAsset  = assets[0];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [[PHImageManager defaultManager] requestImageDataForAsset:anAsset
                                                          options:nil
                                                    resultHandler:
         ^(NSData *imageData, NSString *dataUTI, UIImageOrientation imgOrientation, NSDictionary *info) {
             UIImage* ciImage =  [UIImage imageWithData:imageData];
             UIImage *img = [UIImage imageWithCGImage:ciImage.CGImage scale:.5 orientation:imgOrientation];
             img = [self rotateImage:img];
             
             self.profileBg.image = img;
             self.profilePic.image = img;
             
             [MBProgressHUD showHUDAddedTo:self.view animated:YES];
             __weak __typeof__(self) weakSelf = self;
             NSString *imageName = [NSString stringWithFormat:@"c-%@-profile.jpg", self.candid8.cid];
             [[ASCCandidateProfileSubmissionService sharedInstance] pushImage:img named:imageName withResponseBlock:^(BOOL isSuccess, id resultData) {
                 if (isSuccess) {
                     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                         __typeof__(self) strongSelf = weakSelf;if(!strongSelf)return;
                         [MBProgressHUD hideAllHUDsForView:strongSelf.view animated:NO];
                         [self updateCandidat8Details];
                         
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"kASCProfilePhotoUpdatedNotification" object:img];
                     });
                 }
             }];
         }];
    });
    [picker dismissViewControllerAnimated:YES completion:^{
        self.isViewDisAppearsForPhotoPicking = NO;
    }];
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(ALAsset *)asset{
    return picker.selectedAssets.count<1;
}


- (UIImage *)rotateImage:(UIImage *)image {
    
    if (image.imageOrientation == UIImageOrientationUp ) {
        return image;
    }
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0, 0, image.size.width, image.size.height)];
    UIImage *copy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return copy;
}

@end
