//
//  ASCResumeTableViewCell.m
//  candid8
//
//  Created by islet on 09/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "ASCResumeTableViewCell.h"


static NSDateFormatter * dateFormatter;
@implementation ASCResumeTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setResume:(ASCResume *)aResum andOnAction:(BtnActionBlockResume)ahandle{
    self.resume = aResum;
    self.lblFileName.text = aResum.fileName;
    NSString * size;
    if (aResum.fileSize.integerValue>1024) {
         size = [NSString stringWithFormat:@"%.2fMB",(aResum.fileSize.floatValue/1024.0)];
    }else{
        size = [NSString stringWithFormat:@"%dKB",aResum.fileSize.intValue];
    }
    self.lblFileTypeSize.text = [NSString stringWithFormat:@"%@ - %@",aResum.fileType,size];
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:kC8_DATE_FORMAT];
    }
    NSDate *updateDate = [dateFormatter dateFromString:aResum.updatedDateStr];
    if (updateDate) {
        NSTimeInterval timeInervale = updateDate.timeIntervalSinceNow;
        self.lblResumeUploaded.text = [NSString stringWithFormat:@"Uploaded %d days ago",-(int)timeInervale/(24*60*60)];
    }else{
        self.lblResumeUploaded.text = @"";

    }

    
        btnClickHandle = ahandle;
}
- (IBAction)btnEditTouch:(UIButton *)sender {
    if (btnClickHandle) {
        btnClickHandle(self);
    }
}
@end
