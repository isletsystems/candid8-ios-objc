//
//  ASCProfileDetailEditCell.swift
//  candid8
//
//  Created by islet on 19/08/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

import UIKit

class ASCProfileDetailEditCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    typealias BtnActionBlock = (cell:ASCProfileDetailEditCell) -> Void
    var btnClickHandleBlock: BtnActionBlock?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setblock(handle:BtnActionBlock){
        self.btnClickHandleBlock = handle
    }
    @IBAction func btnEditTouch(sender: AnyObject) {
        btnClickHandleBlock?(cell: self)
    }
}
