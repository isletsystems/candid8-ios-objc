//
//  ASCMainHeadingCell.swift
//  candid8
//
//  Created by islet on 19/08/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

import UIKit

class ASCMainHeadingCell: UITableViewCell {

    @IBOutlet weak var lblHeading: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
