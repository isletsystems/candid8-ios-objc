//
//  ASCMainViewController.m
//  candid8
//
//  Created by Aldrin Lenny on 20/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCMainViewController.h"

#import "ACSGenericWebViewController.h"


NSString *kC8HelpUrl = @"http://www.thecandid8.com/site/app_faq.html";
NSString *kC8TCsUrl = @"http://www.thecandid8.com/site/app_terms.html";
NSString *kC8PrivacyUrl = @"http://www.thecandid8.com/site/app_privacy.html";

/*
 http://www.thecandid8.com/site/app_terms.html
 http://www.thecandid8.com/site/app_privacy.html
 http://www.thecandid8.com/site/app_faq.html
 */

@interface ASCMainViewController ()

@property (nonatomic,strong) UIViewController *menuScene;
@property (nonatomic,strong) UIViewController *loginScene;
@property (nonatomic,strong) UIViewController *mainScene;
@property (nonatomic,strong) ACSGenericWebViewController *genericWebViewScene;
@property (nonatomic,strong) UIViewController *profileScene;
@property (nonatomic,strong) UIViewController *tcsScene;
@property (nonatomic,strong) UIViewController *privacyScene;

@end

@implementation ASCMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}

- (void)awakeFromNib
{
    self.scaleMenuView = false;
    self.scaleBackgroundImageView = false;
    self.scaleContentView = false;
    self.parallaxEnabled = false;
    
    self.menuScene = [self.storyboard instantiateViewControllerWithIdentifier:@"menuScene"];
    [self setLeftMenuViewController:self.menuScene];
    self.loginScene = [self.storyboard instantiateViewControllerWithIdentifier:@"loginScene"];
    [self setContentViewController:self.loginScene];
    [self setRightMenuViewController:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showSideMenu) name:@"kASCShowSideMenuNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout) name:@"kASCLogoutNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin) name:@"kASCLoginSuccessNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showProfileDetails) name:@"kASCShowProfile" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showHelpSupport) name:@"kASCShowHelpSupport" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showTCs) name:@"kASCShowTCsNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPrivacyPolicy) name:@"kASCShowPrivacyNotification" object:nil];

}

- (void)showSideMenu
{
    [self presentLeftMenuViewController];
}

- (void)didLogout
{
    [self setContentViewController:self.loginScene];
    [self hideMenuViewController];
}

- (void)didLogin
{
    if (!self.mainScene)
    {
        self.mainScene = [self.storyboard instantiateViewControllerWithIdentifier:@"mainScene"];
    }
    [self setContentViewController:self.mainScene];
    [self hideMenuViewController];
}

- (void)showHelpSupport
{
    [self showWebViewWithUrl:kC8HelpUrl];
}

- (void)showProfileDetails
{
    if (!self.profileScene)
    {
        self.profileScene = [self.storyboard instantiateViewControllerWithIdentifier:@"profileScene"];
    }
    [self setContentViewController:self.profileScene];
    [self hideMenuViewController];
}

- (void)showTCs
{
    [self showWebViewWithUrl:kC8TCsUrl];
}

- (void)showPrivacyPolicy
{
    [self showWebViewWithUrl:kC8PrivacyUrl];
}

- (void)showWebViewWithUrl:(NSString *)aUrlString
{
    if (!self.genericWebViewScene)
    {
        self.genericWebViewScene = [self.storyboard instantiateViewControllerWithIdentifier:@"genericWebScene"];
    }
    self.genericWebViewScene.urlString = aUrlString;
    [self setContentViewController:self.genericWebViewScene];
    [self hideMenuViewController];
}

@end
