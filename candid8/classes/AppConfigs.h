//
//  AppConfigs.h
//  candid8
//
//  Created by Aldrin Lenny on 22/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "SystemDefines.h"


// Theme Color


static NSString * const kC8ThemeColor = @"#148CBA";
static NSString * const kC8ThemeButtonColor = @"#1F6F98";
static NSString * const kC8ThemeButtonTextColor = @"#f5f5f5";

static NSString * const kC8DarkGrayColor = @"#1C1C1C";
static NSString * const kC8GrayColor = @"#9E9E9E";
static NSString * const kC8DisableSubmitColor = @"#A7C5C9";
static NSString * const kC8LightGrayColor = @"#E0E0E0";

static NSString * const kC8SeperatorLineColor = @"#1C90BC";

static NSString * const  kC8LightBlueColor = @"#27A7CD";

// Font



static NSString * const kC8ButtonColor = @"#6ACDEA";
static int kC8ButtonColorInt = 0x6ACDEA;
static int kC8ButtonBgColorInt = 0x1F6F98;
static NSString * const kC8SelectedSegmentColor = @"#148CBA"; // @"#528AB9";
static NSString * const kC8TabColor = @"#00aeef";
static NSString * const kC8JobQuestionUnasweredColor = @"#a0a0a0";
static NSString * const kC8JobQuestionAsweredColor = @"#1679ab";
static NSString * const kC8JobQuestionSeparatorColor = @"#b0b0b0";
static NSString * const kC8JobQuestionSeparatorColorOld = @"#bdbebf";
static NSString * const kC8LineColor = @"#54bfe3";

static NSString * const kC8_VIDEO_TYPE = @"video/mp4";
static NSString * const kC8_IMAGE_TYPE = @"image/jpeg";
static NSString * const kC8_VIDEO_TYPE_EXTENSION = @"mp4";
static NSString * const kC8_IMAGE_TYPE_EXTENSION = @"jpg";

static NSString * const kC8_RESPMODE_KEY = @"RESPMODE";
static NSString * const kC8_RESPDATA_KEY = @"RESPDATA";

static NSString * const kC8_DATE_FORMAT = @"yyyy-MM-dd'T'HH:mm:ss.sss'Z'";

#ifdef DEBUG_CONFIG

static NSString * const kTestUserName = @"captalvins@gmail.com";
static NSString * const kTestUserPass = @"islet1!";
static NSString * const kSiteURL = @"http://thecandid8.com/beta";

#endif

#ifdef ADHOC_CONFIG

static NSString * const kTestUserName = @"captalvins@gmail.com";
static NSString * const kTestUserPass = @"appsails1!";
static NSString * const kSiteURL = @"http://thecandid8.com/beta";

#endif

#ifdef DISTRIBUTE_CONFIG

static NSString * const kSiteURL = @"http://thecandid8.com";

#endif