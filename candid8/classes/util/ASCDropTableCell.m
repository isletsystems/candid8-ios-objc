//
//  ASCDropTableCell.m
//  Totelier
//
//  Created by Eldhose on 03-06-2014.
//  Copyright (c) 2014 Phenomtec. All rights reserved.
//

#import "ASCDropTableCell.h"

@implementation ASCDropTableCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
