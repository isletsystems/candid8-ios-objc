//
//  ASCDropDownListViewController.m
//  Totelier
//
//  Created by Eldhose on 11-06-2014.
//  Copyright (c) 2014 Phenomtec. All rights reserved.
//

#import "ASCDropDownListViewController.h"
#import "ASCDropTableCell.h"
@interface ASCDropDownListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *theTableView;
@end

@implementation ASCDropDownListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.theTableView registerNib:[UINib nibWithNibName:@"ASCDropTableCell" bundle:nil] forCellReuseIdentifier:@"ASC_DROP_TABLE_CELL"];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    choicelist = nil;
    selectionHandle = NULL;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)loadWithItemsInttheArray:(NSArray *)itmList withSettingDic:(NSDictionary *)settingD onSelectingAnItem:(SelectedItem)handle{
    choicelist = itmList;
    selectionHandle = handle;
    self.indexSettingDic = settingD;
}
- (CGFloat)heighOfARow{
    return 44.0;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return choicelist.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ASCDropTableCell * cell =[tableView dequeueReusableCellWithIdentifier:@"ASC_DROP_TABLE_CELL"];
    cell.backgroundColor = [UIColor clearColor];
    cell.lblItem.text = choicelist[indexPath.row];
    if (self.indexSettingDic) {
        NSDictionary * settings = self.indexSettingDic[@(indexPath.row)];
        if (settings) {
            if (settings[@(ASCDropTableRowFont)]) {
                cell.lblItem.font =settings[@(ASCDropTableRowFont)];
            }
            if (settings[@(ASCDropTableRowTextColor)]) {
                cell.lblItem.textColor =settings[@(ASCDropTableRowTextColor)];
            }
        }else{
            cell.lblItem.font = [UIFont fontWithName:@"Helvetica Neue" size:18];
            cell.lblItem.textColor = [UIColor whiteColor];
        }

    }
        return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (selectionHandle) {
        selectionHandle(choicelist[indexPath.row],indexPath);
    }
}

@end
