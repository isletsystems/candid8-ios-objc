//
//  TOTDropTableCell.h
//  Totelier
//
//  Created by Eldhose on 03-06-2014.
//  Copyright (c) 2014 Phenomtec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASCDropTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblItem;

@end
