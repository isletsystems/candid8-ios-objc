//
//  ASCTSPopoverController.h
//  Totelier
//
//  Created by cereb1 on 01/10/14.
//  Copyright (c) 2014 Phenomtec. All rights reserved.
//

#import "TSPopoverController.h"
#import "ASCDropDownListViewController.h"
#import "ASCPopUpEnums.h"

@protocol TSPopoverDismissDelegate;

@interface ASCTSPopoverController : TSPopoverController

+(instancetype)showArrayPopoverWithArray:(NSArray *)array fromView:(UIView *)aView inView:(UIView *)aSuperView withinFrame:(CGRect)frame andOnSelectingAnItem:(SelectedItem)handle;
+(instancetype)showArrayPopoverWithArray:(NSArray *)array fromView:(UIView *)aView inView:(UIView *)aSuperView withinFrame:(CGRect)frame withWidh:(float)width andOnSelectingAnItem:(SelectedItem)handle;
+(instancetype)showArrayPopoverWithArray:(NSArray *)array fromView:(UIView *)aView inView:(UIView *)aSuperView withinFrame:(CGRect)frame withSettingd:(NSDictionary *)setingDic andOnSelectingAnItem:(SelectedItem)handle;
@property(nonatomic,weak) id delegate;

@end


@protocol TSPopoverDismissDelegate <NSObject>


-(void)popOverControllerDidDismissed;

@end