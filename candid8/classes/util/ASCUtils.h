//
//  ASCUtils.h
//  candid8
//
//  Created by Aldrin Lenny on 03/08/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ASCUtils : NSObject

+ (NSString*)appVersionInfo;

@end
