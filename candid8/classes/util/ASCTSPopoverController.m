//
//  ASCTSPopoverController.m
//  Totelier
//
//  Created by cereb1 on 01/10/14.
//  Copyright (c) 2014 Phenomtec. All rights reserved.
//

#import "ASCTSPopoverController.h"

#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define CORNER_RADIUS 5
#define MARGIN 5
#define OUTER_MARGIN 5
#define TITLE_LABEL_HEIGHT 25
#define ARROW_SIZE 20
#define ARROW_MARGIN 2

@interface ASCTSPopoverController ()

@end

@implementation ASCTSPopoverController

static CGRect viewFrame;

- (id)init {
    if ((self = [super init])) {
        
        self.titleColor = [UIColor whiteColor];
        self.titleFont = [UIFont boldSystemFontOfSize:14];
        self.view.backgroundColor = [UIColor clearColor];
        self.arrowPosition = TSPopoverArrowPositionHorizontal;
        self.cornerRadius = 0;
        self.popoverBaseColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.20f];
        self.popoverGradient = NO;
        screenRect = [[UIScreen mainScreen] bounds];
        if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
            if(self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft || self.interfaceOrientation == UIInterfaceOrientationLandscapeRight){
                screenRect.size.width = [[UIScreen mainScreen] bounds].size.height;
                screenRect.size.height = [[UIScreen mainScreen] bounds].size.width;
            }
        }
        self.view.frame = screenRect;
        screenRect.origin.y = 0;
        screenRect.size.height = screenRect.size.height-20;
        
        titleLabelheight = 0;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.contentViewController = nil;
    self.contentView = nil;
    self.titleText = nil;
    self.titleColor = nil;
    self.titleFont = nil;
    self.popoverBaseColor = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dismissPopoverAnimatd:(BOOL)animated
{
    [super dismissPopoverAnimatd:animated];
    if (_delegate) {
        if ([_delegate respondsToSelector:@selector(popOverControllerDidDismissed)]) {
            [_delegate popOverControllerDidDismissed];
            _delegate = nil;
        }
    }
    NSArray *subViews = self.view.subviews;
    if([subViews[0] respondsToSelector:@selector(setDelegate:)]){
        [subViews[0] setDelegate:nil];
    }
}


//#pragma mark - Class Function
//+(instancetype)showArrayPopoverWithArray:(NSArray *)array fromView:(UIView *)aView inView:(UIView *)aSuperView andOnSelectingAnItem:(SelectedItem)handle{
//    ASCDropDownListViewController * dropView = [[ASCDropDownListViewController alloc] initWithNibName:NSStringFromClass([ASCDropDownListViewController class]) bundle:nil];
//    [dropView loadWithItemsInttheArray:array onSelectingAnItem:handle];
//    
//    ASCTSPopoverController* popoverView = [[ASCTSPopoverController alloc] initWithContentViewController:dropView];
//    CGFloat height = MAX(dropView.heighOfARow*array.count+30, 200);
//    if (height>700)
//    {
//        height = 700;
//    }
//    dropView.view.frame = CGRectMake(0,0, 250, height);
//    popoverView.arrowPosition = TSPopoverArrowPositionVertical;
//   // popoverView.popoverBaseColor = [TOTThemeHelper colorForPopUp];
//    [popoverView showPopoverWithRect:[aView convertRect:aView.bounds toView:aSuperView]];
//
//
//    return popoverView;
//}
+(instancetype)showArrayPopoverWithArray:(NSArray *)array fromView:(UIView *)aView inView:(UIView *)aSuperView withinFrame:(CGRect)frame withWidh:(float)width andOnSelectingAnItem:(SelectedItem)handle{
    
    return [ASCTSPopoverController showArrayPopoverWithArray:array fromView:aView inView:aSuperView withinFrame:frame withWidh:width withSettingDic:nil andOnSelectingAnItem:handle];
}
+(instancetype)showArrayPopoverWithArray:(NSArray *)array fromView:(UIView *)aView inView:(UIView *)aSuperView withinFrame:(CGRect)frame withWidh:(float)width withSettingDic:(NSDictionary *)asettingDic andOnSelectingAnItem:(SelectedItem)handle{
    @autoreleasepool {
        viewFrame = frame;
        ASCDropDownListViewController * dropView = [[ASCDropDownListViewController alloc] initWithNibName:NSStringFromClass([ASCDropDownListViewController class]) bundle:nil];
        
        [dropView loadWithItemsInttheArray:array withSettingDic:asettingDic onSelectingAnItem:handle];
        dropView.indexSettingDic = asettingDic;
        ASCTSPopoverController* popoverView = [[ASCTSPopoverController alloc] initWithContentViewController:dropView];
        CGFloat height = MAX(dropView.heighOfARow*array.count+10, 200);
        if (height>700)
        {
            height = 700;
        }
        dropView.view.frame = CGRectMake(0,0, width, height);
        popoverView.arrowPosition = TSPopoverArrowPositionVertical;
        [popoverView showPopoverWithRect:[aView convertRect:aView.bounds toView:aSuperView]];
        NSArray *subViews = popoverView.view.subviews;
        ((UIView *)subViews[1]).alpha = 0.95;
        return popoverView;
    }

    
}
+(instancetype)showArrayPopoverWithArray:(NSArray *)array fromView:(UIView *)aView inView:(UIView *)aSuperView withinFrame:(CGRect)frame andOnSelectingAnItem:(SelectedItem)handle
{
    return [ASCTSPopoverController showArrayPopoverWithArray:array fromView:aView inView:aSuperView withinFrame:frame withWidh:250 andOnSelectingAnItem:handle];
}
+(instancetype)showArrayPopoverWithArray:(NSArray *)array fromView:(UIView *)aView inView:(UIView *)aSuperView withinFrame:(CGRect)frame withSettingd:(NSDictionary *)setingDic andOnSelectingAnItem:(SelectedItem)handle{
     return [ASCTSPopoverController showArrayPopoverWithArray:array fromView:aView inView:aSuperView withinFrame:frame withWidh:250 withSettingDic:setingDic andOnSelectingAnItem:handle];
}


- (CGRect)popoverFrameRect:(CGRect)contentFrame senderPoint:(CGPoint)senderPoint
{
    CGRect popoverRect;
    float popoverWidth;
    float popoverHeight;
    float popoverX;
    float popoverY;
    
    if(self.arrowPosition == TSPopoverArrowPositionVertical){
        
        popoverWidth = contentFrame.size.width+MARGIN*2;
        popoverHeight = contentFrame.size.height+titleLabelheight+(ARROW_SIZE+MARGIN*2);
        
        popoverX = senderPoint.x - (popoverWidth/2);
        if(popoverX < viewFrame.origin.x) {
            popoverX = viewFrame.origin.x;
        } else if((popoverX + popoverWidth)>(viewFrame.size.width+viewFrame.origin.x)) {
            popoverX = viewFrame.size.width+viewFrame.origin.x - (popoverWidth+OUTER_MARGIN);
        }
        
        if(arrowDirection == TSPopoverArrowDirectionBottom){
            popoverY = senderPoint.y - popoverHeight - ARROW_MARGIN;
        }else{
            popoverY = senderPoint.y + ARROW_MARGIN;
        }
        
        popoverRect = CGRectMake(popoverX, popoverY, popoverWidth, popoverHeight);
        
    }
    else if(self.arrowPosition == TSPopoverArrowPositionHorizontal){
        
        popoverWidth = contentFrame.size.width+ARROW_SIZE+MARGIN*2;
        popoverHeight = contentFrame.size.height+titleLabelheight+MARGIN*2;
        
        if(arrowDirection == TSPopoverArrowDirectionRight){
            popoverX = senderPoint.x - popoverWidth - ARROW_MARGIN;
        }else{
            popoverX = senderPoint.x + ARROW_MARGIN;
        }
        
        popoverY = senderPoint.y - (popoverHeight/2);
        if(popoverY < OUTER_MARGIN){ 
            popoverY = OUTER_MARGIN;
        }else if((popoverY + popoverHeight)>self.view.frame.size.height){
            popoverY = self.view.frame.size.height - (popoverHeight+OUTER_MARGIN);
        }
        
        popoverRect = CGRectMake(popoverX, popoverY, popoverWidth, popoverHeight);
        
    }
    
    
    return popoverRect;
    
}
- (void)view:(UIView*)view touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    [self dismissPopoverAnimatd:NO];
}

@end
