//
//  ASCS3Manager.h
//  candid8
//
//  Created by Aldrin Lenny on 21/04/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

extern NSString *const C8S3_BUCKET;
extern NSString *const C8S3_BUCKET_PROD;
extern NSString *const C8S3_ENDPOINT;

#import <AWSS3/AWSS3.h>
#import "ASCServiceResponse.h"

typedef void (^ASCS3UploadResponseBlock)(BOOL status, double percentComplete);

@interface ASCS3Manager : NSObject

@property (nonatomic, strong) S3TransferManager *tm;

+ (ASCS3Manager *)sharedInstance;

- (S3TransferOperation *)uploadFromLocalUrl:(NSString *)aLocalUrl withBucketItemName:(NSString *)aBucketItemName andContentType:(NSString *)aContentType withResponseBlock:(ASCS3UploadResponseBlock)aResponseBlock;

//- (S3TransferOperation *)downloadToLocalUrl:(NSString *)aLocalUrl withBucketItemName:(NSString *)aBucketItemName andContentType:(NSString *)aContentType withResponseBlock:(ASCS3UploadResponseBlock)aResponseBlock;

- (NSURL *)getPreSignedURLWithBucketItemName:(NSString *)aBucketItemName;
- (NSURL *)getPreSignedURLWithItemName:(NSString *)aBucketItemName;

- (void)loadImagefromS3Key:(NSString *)anImageKey withCompletion:(ASCServiceResponseBlock)aResponseBlock;

@end
