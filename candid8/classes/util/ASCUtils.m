//
//  ASCUtils.m
//  candid8
//
//  Created by Aldrin Lenny on 03/08/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCUtils.h"

@implementation ASCUtils

+ (NSString*)appVersionInfo
{
    //http://stackoverflow.com/questions/6851660/version-vs-build-in-xcode-4
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    return [NSString stringWithFormat:@"%@ b%@", version, build];
}

@end
