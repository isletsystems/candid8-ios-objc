//
//  ASCS3Manager.m
//  candid8
//
//  Created by Aldrin Lenny on 21/04/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCS3Manager.h"

NSString *const C8S3_BUCKET = @"c8webapp";
NSString *const C8S3_BUCKET_PROD = @"c8webapp";
NSString *const C8S3_ENDPOINT = @"s3-ap-southeast-2.amazonaws.com";

#import <Objection/Objection.h>
#import <AWSRuntime/AWSRuntime.h>
#import "SDWebImageManager.h"

// Constants used to represent your AWS Credentials.
#warning TODO: base64 encode it before taking to AppSore
NSString *const C8S3_ACCESS_KEY_ID = @"AKIAJ5GWKP7P7SCNGD3A";
NSString *const C8S3_SECRET_KEY = @"lEs5SrFs8Xnd1Pip1dgV3+2U1ExrcWqups2G0CyK";

NSString *const CREDENTIALS_ERROR_TITLE  = @"Missing Credentials";
NSString *const CREDENTIALS_ERROR_MESSAGE  = @"AWS Credentials not configured correctly.  Please review the README file.";

@interface ASCS3Manager()<AmazonServiceRequestDelegate>

@property (nonatomic, strong) AmazonS3Client *s3;
@property (nonatomic, strong) NSMutableDictionary *s3FilesOperationDict;
@property (nonatomic, strong) NSMutableDictionary *s3FilesResponseHandlerDict;

@end

@implementation ASCS3Manager

objection_register_singleton(ASCS3Manager)

+ (ASCS3Manager *)sharedInstance {
   ASCS3Manager *sinst = [[JSObjection defaultInjector] getObject:[ASCS3Manager class]];
   [sinst initS3];
   return sinst;
}

- (void)initS3 {
    if(![C8S3_ACCESS_KEY_ID isEqualToString:@"CHANGE ME"]
       && self.s3 == nil)
    {
        // Initial the S3 Client.
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // This sample App is for demonstration purposes only.
        // It is not secure to embed your credentials into source code.
        // DO NOT EMBED YOUR CREDENTIALS IN PRODUCTION APPS.
        // We offer two solutions for getting credentials to your mobile App.
        // Please read the following article to learn about Token Vending Machine:
        // * http://aws.amazon.com/articles/Mobile/4611615499399490
        // Or consider using web identity federation:
        // * http://aws.amazon.com/articles/Mobile/4617974389850313
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        self.s3 = [[AmazonS3Client alloc] initWithAccessKey:C8S3_ACCESS_KEY_ID withSecretKey:C8S3_SECRET_KEY];
        self.s3.endpoint = [AmazonEndpoints s3Endpoint:AP_SOUTHEAST_2]; //sydney  //@"c8beta.s3-ap-southeast-2.amazonaws.com"
        
        // Initialize the S3TransferManager
        self.tm = [S3TransferManager new];
        self.tm.s3 = self.s3;
        self.tm.delegate = self;
        
        @try {
            S3CreateBucketRequest *createBucketRequest = [[S3CreateBucketRequest alloc] initWithName:C8S3_BUCKET andRegion:[S3Region APSydney]];
            S3CreateBucketResponse *createBucketResponse = [self.s3 createBucket:createBucketRequest];
            if(createBucketResponse.error != nil)
            {
                DLog(@"Error: %@", createBucketResponse.error);
            }
        }@catch(AmazonServiceException *exception){
            if(![@"BucketAlreadyOwnedByYou" isEqualToString: exception.errorCode]){
                DLog(@"Unable to create bucket: %@", exception.error);
            }
        }
        self.s3FilesOperationDict = [@{} mutableCopy];//init the file -> S3 operation dictionary
        self.s3FilesResponseHandlerDict = [@{} mutableCopy];//init the file -> Response Handler block dictionary
    }
}


- (NSURL *)getPreSignedURLWithBucketItemName:(NSString *)aBucketItemName {
    
    S3GetPreSignedURLRequest * request = [[S3GetPreSignedURLRequest alloc] init];
    request.key = aBucketItemName;
    request.bucket = C8S3_BUCKET;
    [request setExpires:[NSDate dateWithTimeIntervalSinceNow:3600]];
    
    return [self.s3 getPreSignedURL:request];
}

- (NSURL *)getPreSignedURLWithItemName:(NSString *)aBucketItemName {
    
    S3GetPreSignedURLRequest * request = [[S3GetPreSignedURLRequest alloc] init];
    request.key = aBucketItemName;
    [request setExpires:[NSDate dateWithTimeIntervalSinceNow:3600]];
    
    return [self.s3 getPreSignedURL:request];
}




//https://www.iphonelife.com/blog/31369/unleash-your-inner-app-developer-part-38-storing-ios-images-video-amazon-s3-cloud

- (S3TransferOperation *)uploadFromLocalUrl:(NSString *)aLocalUrl withBucketItemName:(NSString *)aBucketItemName andContentType:(NSString *)aContentType withResponseBlock:(ASCS3UploadResponseBlock)aResponseBlock {

    S3TransferOperation *uploadFileS3Operation = [self.s3FilesOperationDict objectForKey:aBucketItemName];
    
    if(uploadFileS3Operation == nil || (uploadFileS3Operation.isFinished && !uploadFileS3Operation.isPaused))
    {
        uploadFileS3Operation = [self.tm uploadFile:aLocalUrl bucket:C8S3_BUCKET key:aBucketItemName];
        [self.s3FilesOperationDict setObject:uploadFileS3Operation forKey:aBucketItemName];
        [self.s3FilesResponseHandlerDict setObject:aResponseBlock forKey:aBucketItemName];
    }
    return uploadFileS3Operation;
}

#pragma mark - AmazonServiceRequestDelegate

-(void)request:(AmazonServiceRequest *)request didReceiveResponse:(NSURLResponse *)response
{
    DLog(@"didReceiveResponse called: %@", response);
}

-(void)request:(AmazonServiceRequest *)request didSendData:(long long) bytesWritten totalBytesWritten:(long long)totalBytesWritten totalBytesExpectedToWrite:(long long)totalBytesExpectedToWrite
{
    NSString *key = ((S3PutObjectRequest *)request).key;
    ASCS3UploadResponseBlock aResponseBlock = [self.s3FilesResponseHandlerDict objectForKey:key];
    
    if(aResponseBlock){
        double percent = ((double)totalBytesWritten/(double)totalBytesExpectedToWrite)*100;
        aResponseBlock(TRUE, percent); //[NSString stringWithFormat:@"%.2f%%", percent]
    }
}

-(void)request:(AmazonServiceRequest *)request didCompleteWithResponse:(AmazonServiceResponse *)response
{
    NSString *key = ((S3PutObjectRequest *)request).key;
    ASCS3UploadResponseBlock aResponseBlock = [self.s3FilesResponseHandlerDict objectForKey:key];
    
    if(aResponseBlock){
        aResponseBlock(TRUE, 100); //done
    }
}

-(void)request:(AmazonServiceRequest *)request didFailWithError:(NSError *)error
{
    DLog(@"didFailWithError called: %@", error);
    NSString *key = ((S3PutObjectRequest *)request).key;
    ASCS3UploadResponseBlock aResponseBlock = [self.s3FilesResponseHandlerDict objectForKey:key];
    
    if(aResponseBlock){
        aResponseBlock(FALSE, -1); //failed
    }
}

-(void)request:(AmazonServiceRequest *)request didFailWithServiceException:(NSException *)exception
{
    DLog(@"didFailWithServiceException called: %@", exception);
    NSString *key = ((S3PutObjectRequest *)request).key;
    ASCS3UploadResponseBlock aResponseBlock = [self.s3FilesResponseHandlerDict objectForKey:key];
    
    if(aResponseBlock){
        aResponseBlock(FALSE, -1); //failed
    }
}

-(void)loadImagefromS3Key:(NSString *)anImageKey withCompletion:(ASCServiceResponseBlock)aResponseBlock
{
    // check if photo URL is not null crash fix
    if ([anImageKey isKindOfClass:[NSString class]]) {
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        NSURL *signedPhotoURL = [[ASCS3Manager sharedInstance] getPreSignedURLWithBucketItemName:anImageKey];
        [manager downloadImageWithURL:signedPhotoURL
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (aResponseBlock) {
                                    aResponseBlock(TRUE, image);
                                }
                            }];
        
    }
}

@end
