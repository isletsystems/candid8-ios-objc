//
//  ASCDropDownListViewController.h
//  Totelier
//
//  Created by Eldhose on 11-06-2014.
//  Copyright (c) 2014 Phenomtec. All rights reserved.
//

#import "IBaseViewController.h"
#import "ASCPopUpEnums.h"
typedef void (^SelectedItem)(NSString *seletedText, NSIndexPath *indexPath);



@interface ASCDropDownListViewController : IBaseViewController{
    NSArray * choicelist;
    SelectedItem selectionHandle;
}
@property (nonatomic,strong)NSDictionary * indexSettingDic;
- (void)loadWithItemsInttheArray:(NSArray *)itmList withSettingDic:(NSDictionary *)settingD onSelectingAnItem:(SelectedItem)handle;
- (CGFloat)heighOfARow;
@end
