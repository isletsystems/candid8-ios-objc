//
//  ASCCandindate.m
//  candid8
//
//  Created by Aldrin Lenny on 11/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCCandindate.h"

@implementation ASCCandindate

- (NSString *)fullName {
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"candidateId": @"cid",
                                                       @"last_login": @"lastLogin",
                                                       @"userId": @"user_id"
                                                       }];
}

@end
