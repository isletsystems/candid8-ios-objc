//
//  ASCTokenInfo.swift
//  candid8
//
//  Created by Aldrin ML on 26/08/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

import UIKit

@objc(ASCTokenInfo)
class ASCTokenInfo: NSObject {
   
    var accessToken:String?
    var refreshToken:String?
    var expiresInSeconds:Int = 1800
    var tokenType:String?
    var lastTockenUpdatedDateTime:NSDate?
    
    init(accessToken:String, refreshToken:String, tokenType:String) {
        self.accessToken = accessToken
        self.refreshToken = refreshToken
        self.tokenType = tokenType
    }
}
