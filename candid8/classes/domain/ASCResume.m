//
//  ASCResume.m
//  candid8
//
//  Created by Aldrin ML on 28/08/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import "ASCResume.h"

@implementation ASCResume

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"rid",
                                                       @"resume_file_name": @"fileName",
                                                       @"resume_content_type": @"fileType",
                                                       @"resume_file_size": @"fileSize",
                                                       @"resume_url": @"fileUrl",
                                                       @"updated_at":@"updatedDateStr"
                                                       }];
}

@end
