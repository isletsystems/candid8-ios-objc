//
//  ASCJobPostingResponse.h
//  candid8
//
//  Created by Aldrin Lenny on 11/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <JSONModel/JSONModel.h>

@protocol ASCJobPostingResponse
@end


typedef enum : NSInteger {
    ASCResponseModePending = -1,
    ASCResponseModeRecord = 0,
    ASCResponseModeWrite = 1,
    ASCResponseModeSkip = 2,
} ASCResponseMode;


@interface ASCJobPostingResponse : JSONModel

@property (nonatomic, strong) NSString *aid;
@property (nonatomic, strong) NSString<Optional> *qid; //questiod it belongs to
@property (nonatomic, strong) NSString<Optional> *score;
@property (nonatomic, strong) NSNumber<Optional> *status;
@property (nonatomic, assign) BOOL published;
@property (nonatomic, strong) NSNumber<Optional> *answerType;
@property (nonatomic, strong) NSString<Optional> *url;
@property (nonatomic, strong) NSString<Optional> *writtenText;
@property (nonatomic, strong) NSString<Optional> *notes;

@property (nonatomic, strong) NSString<Optional> *stillImageUrl;
@property (nonatomic, strong) NSString<Optional> *stillImageLocalUrl;
@property (nonatomic, strong) NSString<Optional> *videoLocalUrl;
@property (nonatomic, strong) NSString<Optional> *recordRightAwayFlag;
@property (nonatomic, strong) NSDate<Optional> *createdAt;
@property (nonatomic, strong) NSDate<Optional> *lastUpdatedAt;

- (BOOL)isCaptured;

- (ASCResponseMode)getResponseMode;

@end
