//
//  ASCCandindate.h
//  candid8
//
//  Created by Aldrin Lenny on 11/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSONModel/JSONModel.h>
#import "ASCJobPosting.h"
#import "ASCResume.h"

@interface ASCCandindate : JSONModel

@property (nonatomic, strong) NSString *cid;
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString<Optional>* photo;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *lastLogin;
@property (nonatomic, strong) NSString<Optional>* gender;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString <Optional>* phone;
@property (nonatomic, strong) NSString <Optional>* createdAtStr;
@property (nonatomic, strong) NSNumber <Optional>* userId;

@property (nonatomic, strong) NSArray<ASCJobPosting, Optional> *jobs;
@property (nonatomic, strong) NSArray<ASCResume, Optional> *resumes;

- (NSString *)fullName;

@end
