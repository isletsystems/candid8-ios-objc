//
//  ASCJobPostingQuestion.h
//  candid8
//
//  Created by Aldrin Lenny on 11/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <JSONModel/JSONModel.h>

#import "ASCJobPostingResponse.h"

@protocol ASCJobPostingQuestion
@end

@interface ASCJobPostingQuestion : JSONModel

@property (nonatomic, strong) NSString *qid;
@property (nonatomic, strong) NSString<Optional> *title;
@property (nonatomic, strong) NSString<Optional> *desc;
@property (nonatomic, readwrite) NSInteger maxTime;
@property (nonatomic, strong) ASCJobPostingResponse<Optional> *answer;

//either answered or skipped
- (BOOL)isAttempted;
- (BOOL)isVideoResponse;

@end
