//
//  ASCResume.h
//  candid8
//
//  Created by Aldrin ML on 28/08/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol ASCResume
@end

@interface ASCResume : JSONModel

@property (nonatomic, strong) NSString *rid;
@property (nonatomic, strong) NSString<Optional> *fileName;
@property (nonatomic, strong) NSString<Optional> *fileType;
@property (nonatomic, strong) NSNumber<Optional> *fileSize;
@property (nonatomic, strong) NSString<Optional> *status;
@property (nonatomic, strong) NSString<Optional> *fileUrl;
@property (nonatomic, strong) NSString<Optional> *updatedDateStr;


@end
