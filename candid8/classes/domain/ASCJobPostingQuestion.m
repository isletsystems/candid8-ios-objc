//
//  ASCJobPostingQuestion.m
//  candid8
//
//  Created by Aldrin Lenny on 11/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobPostingQuestion.h"

#import "ASCJobSubmissionService.h"

@implementation ASCJobPostingQuestion

- (BOOL)isAttempted {
    return self.answer && [self.answer isCaptured];
}

- (BOOL)isVideoResponse {
    if ([self isAttempted]) {
        return self.answer.answerType.intValue == ASCResponseModeRecord;
    }
    return YES;
}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"qid",
                                                       @"question": @"title",
                                                       @"max_time": @"maxTime",
                                                       @"description": @"desc"
                                                       }];
}

@end
