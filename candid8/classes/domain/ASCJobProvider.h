//
//  ASCJobProvider.h
//  candid8
//
//  Created by Aldrin Lenny on 13/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <JSONModel/JSONModel.h>

@interface ASCJobProvider : JSONModel

@property (nonatomic, strong) NSString<Optional> *eid;
@property (nonatomic, strong) NSString<Optional> *name;
@property (nonatomic, strong) NSString<Optional> *logo;

@end
