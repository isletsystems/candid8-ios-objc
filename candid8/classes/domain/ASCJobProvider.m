//
//  ASCJobProvider.m
//  candid8
//
//  Created by Aldrin Lenny on 13/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobProvider.h"

@implementation ASCJobProvider

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"eid",
                                                       @"email": @"name",
                                                       @"logo_img_file_name": @"logo"
                                                       }];
}

@end
