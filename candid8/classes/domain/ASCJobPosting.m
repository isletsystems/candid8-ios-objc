//
//  ASCJobPosting.m
//  candid8
//
//  Created by Aldrin Lenny on 11/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobPosting.h"

#define ASC_INDATE_FORMAT @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
#define ASC_JOBDATE_FORMAT @"dd MMM, yyyy"   //2015-08-20T00:00:00.000Z

@implementation JSONValueTransformer (CustomTransformer)

- (NSDate *)NSDateFromNSString:(NSString*)string
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:ASC_INDATE_FORMAT];
    return [formatter dateFromString:string];
}

- (NSString *)JSONObjectFromNSDate:(NSDate *)date
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:ASC_INDATE_FORMAT];
    return [formatter stringFromDate:date];
}

@end

@implementation ASCJobPosting

- (BOOL)isExpired
{
    //check if end date is earlier than today
    NSDate *today = [NSDate new];
    if ([today compare:self.endDate] == NSOrderedDescending)
    {
        return YES;
    }
    return NO;
}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"jid",
                                                       @"job_id": @"jobId",
                                                       @"ref_name": @"refName",
                                                       @"ref_no": @"refNo",
                                                       @"description": @"jobDescription",
                                                       @"start_date": @"startDate",
                                                       @"end_date": @"endDate",
                                                       @"employer_id": @"employerId",
                                                       @"logo_img_file_name": @"ikn",
                                                       @"companyName" : @"companyName",
                                                       @"job_applied" : @"submitted"
                                                       }];
}

+ (NSString *)formattedDate:(NSDate *)date
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:ASC_JOBDATE_FORMAT];
    return [formatter stringFromDate:date];
}

- (BOOL)isSubmitted
{
    if (self.submitted && [self.submitted isEqualToString:@"1"])
    {
        return YES;
    }
    return NO;
}

- (BOOL)isViewed
{
    if ([self isSubmitted])
    {
        return YES;
    }
    if (self.viewed && [self.viewed isEqualToString:@"yes"])
    {
        return YES;
    }
    return NO;
}

@end
