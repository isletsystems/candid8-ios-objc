//
//  ASCJobPosting.h
//  candid8
//
//  Created by Aldrin Lenny on 11/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <JSONModel/JSONModel.h>

#import "ASCJobProvider.h"
#import "ASCJobPostingQuestion.h"

@protocol ASCJobPosting
@end

@interface ASCJobPosting : JSONModel

//candidate job id
@property (nonatomic, strong) NSString *jid;
//original job id
@property (nonatomic, strong) NSString<Optional> *jobId;
@property (nonatomic, strong) NSString<Optional> *refName;
@property (nonatomic, strong) NSString<Optional> *refNo;
@property (nonatomic, strong) NSString<Optional> *ikn;
@property (nonatomic, strong) NSString<Optional> *jobDescription;
@property (nonatomic, strong) NSString<Optional> *jobUrl;
@property (nonatomic, strong) NSDate<Optional> *startDate;
@property (nonatomic, strong) NSDate<Optional> *endDate;
@property (nonatomic, strong) NSString<Optional> *submitted;
@property (nonatomic, strong) NSString<Optional> *viewed;
@property (nonatomic, strong) NSString<Optional> *companyName;


@property (nonatomic, strong) NSString<Optional> *employerId;
@property (nonatomic, strong) ASCJobProvider<Optional> *employer;
@property (nonatomic, strong) NSArray<ASCJobPostingQuestion, Optional> *questions;

- (BOOL)isExpired;
- (BOOL)isSubmitted;
- (BOOL)isViewed;

+ (NSString *)formattedDate:(NSDate *)date;

@end
