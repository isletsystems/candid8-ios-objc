//
//  ASCJobPostingResponse.m
//  candid8
//
//  Created by Aldrin Lenny on 11/01/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//

#import "ASCJobPostingResponse.h"

@implementation ASCJobPostingResponse


- (BOOL)isCaptured {
    if (self.aid && ![self.aid isEqualToString:@"n.a"]) {
        return YES;
    }
    return NO;
}

- (ASCResponseMode)getResponseMode {
    return self.published ? self.answerType.integerValue : ASCResponseModePending;
}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"aid",
                                                       @"question_id": @"qid",
                                                       @"question": @"title",
                                                       @"video_url": @"url",
                                                       @"publish": @"published",
                                                       @"answer_type": @"answerType",
                                                       @"notes" : @"writtenText",
                                                       @"updated_at" : @"lastUpdatedAt",
                                                       @"screen_shot_file_name" : @"stillImageUrl"
                                                       }];
}

@end
