//
//  SystemDefines.h
//  candid8
//
//  Created by Aldrin Lenny on 22/07/14.
//  Copyright (c) 2014 AppSails. All rights reserved.
//
#ifdef CONFIG_Debug

    #define DEBUG_CONFIG

#endif

#ifdef CONFIG_Release

    #define ADHOC_CONFIG

#endif

#ifdef CONFIG_Distribute

    #define DISTRIBUTE_CONFIG

#endif
