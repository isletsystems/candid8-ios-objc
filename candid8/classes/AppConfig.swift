//
//  AppConfig.swift
//  candid8
//
//  Created by Avin Leo on 09/09/15.
//  Copyright (c) 2015 AppSails. All rights reserved.
//

import Foundation

public class AppConfig {
    
    public struct Theme {
        
        // Blue
        public static let kC8BlueColor = "#148CBA"
        public static let kC8LightBlueColor = "27A7CD" //"1C90BC";
        
        // Gray
        public static let kC8DarkGrayColor = "#1C1C1C"
        public static let kC8GrayColor = "#9E9E9E"
        public static let kC8LightGrayColor = "#E0E0E0"
        
    }
    
}