PREBorderView
=============

A very simple Objective-C UIView category for specifying single-sided borders. 

<p align="center" >
  <img src="https://raw.githubusercontent.com/pres/PREBorderView/master/sample.png" alt="sample" title="sample">
</p>

## Installation

Using CocoaPods:

```ruby
pod 'PREBorderView', "~> 2.1"
```

Otherwise just include `UIView+PREBorderView.{h,m}` in your project.

