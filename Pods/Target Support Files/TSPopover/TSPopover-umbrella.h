#import <UIKit/UIKit.h>

#import "TSActionSheet.h"
#import "TSPopoverController.h"
#import "TSPopoverPopoverView.h"
#import "TSPopoverTouchesDelegate.h"
#import "TSPopoverTouchView.h"
#import "UIBarButtonItem+WEPopover.h"

FOUNDATION_EXPORT double TSPopoverVersionNumber;
FOUNDATION_EXPORT const unsigned char TSPopoverVersionString[];

